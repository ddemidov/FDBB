########################################################################
# CMakeLists.txt
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
########################################################################

include(Armadillo)

########################################################################
# Create executables from source files and add tests
########################################################################

file(GLOB UNITTESTS_SRC RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.cpp *.cxx)
add_executables("${UNITTESTS_SRC}" "")

if(CUDA_FOUND)
  file(GLOB UNITTESTS_SRC RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.cu)
  add_executables("${UNITTESTS_SRC}")
endif()

add_tests("${UNITTESTS_SRC}")
