/** @file test_blockexpr_col_vector.hpp
 *
 *  @brief UnitTests++ block col-vector test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
 *  Test creation of block column-vector
 */

TEST_FIXTURE(FDBB_BLOCKEXPRESSION_FIXTURE, blockexpr_col_vector)
{
  fill_vector_d(v0, type_real{ 0.0 });
  fill_vector_d(v1, type_real{ 1.0 });
  fill_vector_d(v2, type_real{ 2.0 });
  fill_vector_d(v3, type_real{ 3.0 });
  fill_vector_d(v4, type_real{ 4.0 });
  fill_vector_d(v5, type_real{ 5.0 });

  TEST_INIT((0),
            (FDBB_BLOCKEXPRESSION_FIXTURE::len),
            (6 * sizeof(type)),
            (FDBB_BLOCKEXPRESSIONFIXTURE::len));

  try {
    TEST_START();

    // Create block column-vector from sequence of scalar vectors
    fdbb::fdbbBlockColVector<0, type_vector_d, 6> A{ v0, v1, v2, v3, v4, v5 };

    // Change content of scalar source vectors
    fill_vector_d(v0, type_real{ 5.0 });
    fill_vector_d(v1, type_real{ 4.0 });
    fill_vector_d(v2, type_real{ 3.0 });
    fill_vector_d(v3, type_real{ 2.0 });
    fill_vector_d(v4, type_real{ 1.0 });
    fill_vector_d(v5, type_real{ 0.0 });

    // Copy block column-vector from block column-vector
    fdbb::fdbbBlockColVector<1, type_vector_d, 6> B{ A };

    // Copy block row-vector from block column-vector
    fdbb::fdbbBlockRowVector<2, type_vector_d, 6> C{ A };

    // Move from temporary block column-vector
    fdbb::fdbbBlockColVector<3, type_vector_d, 6> D{
      fdbb::fdbbBlockColVector<0, type_vector_d, 6>{ A }
    };

    // Move from temporary block column-vector
    fdbb::fdbbBlockColVector<4, type_vector_d, 6> E{
      fdbb::fdbbBlockRowVector<0, type_vector_d, 6>{ A }
    };

    TEST_STOP();
    TEST_REPORT();

    // Check block column-vector A
    copy_vector_d2h(fdbb::get<0>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<1>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<2>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<3>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<4>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<5>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block column-vector B
    copy_vector_d2h(fdbb::get<0>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<1>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<2>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<3>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<4>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<5>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block column-vector C
    copy_vector_d2h(fdbb::get<0>(C), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<1>(C), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<2>(C), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<3>(C), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<4>(C), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<5>(C), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block column-vector D
    copy_vector_d2h(fdbb::get<0>(D), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<1>(D), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<2>(D), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<3>(D), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<4>(D), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<5>(D), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block column-vector E
    copy_vector_d2h(fdbb::get<0>(E), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<1>(E), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<2>(E), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<3>(E), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<4>(E), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<5>(E), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
