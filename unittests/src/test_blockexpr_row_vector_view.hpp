/** @file test_blockexpr_row_vector_view.hpp
 *
 *  @brief UnitTests++ block row-vector view test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
 *  Test creation of block row-vector view
 */

TEST_FIXTURE(FDBB_BLOCKEXPRESSION_FIXTURE, blockexpr_row_vector_view)
{
  fill_vector_d(v0, type_real{ 0.0 });
  fill_vector_d(v1, type_real{ 1.0 });
  fill_vector_d(v2, type_real{ 2.0 });
  fill_vector_d(v3, type_real{ 3.0 });
  fill_vector_d(v4, type_real{ 4.0 });
  fill_vector_d(v5, type_real{ 5.0 });

  TEST_INIT((0),
            (FDBB_BLOCKEXPRESSION_FIXTURE::len),
            (6 * sizeof(type)),
            (FDBB_BLOCKEXPRESSIONFIXTURE::len));

  try {
    TEST_START();

    // Create block row-vector view from sequence of scalar vectors
    fdbb::fdbbBlockRowVectorView<0, type_vector_d, 6> U(v0, v1, v2, v3, v4, v5);

    // Change content of scalar source vectors
    fill_vector_d(v0, type_real{ 5.0 });
    fill_vector_d(v1, type_real{ 4.0 });
    fill_vector_d(v2, type_real{ 3.0 });
    fill_vector_d(v3, type_real{ 2.0 });
    fill_vector_d(v4, type_real{ 1.0 });
    fill_vector_d(v5, type_real{ 0.0 });

    // Create block row-vector view from block row-vector view
    fdbb::fdbbBlockRowVector<0, type_vector_d, 6> V(U);

    // Change content of scalar source vectors
    fill_vector_d(v0, type_real{ 5.0 });
    fill_vector_d(v1, type_real{ 4.0 });
    fill_vector_d(v2, type_real{ 3.0 });
    fill_vector_d(v3, type_real{ 2.0 });
    fill_vector_d(v4, type_real{ 1.0 });
    fill_vector_d(v5, type_real{ 0.0 });

    // Create block row-vector view from block row-vector view
    fdbb::fdbbBlockRowVectorView<0, type_vector_d, 6> W(U)

      TEST_STOP();
    TEST_REPORT();

    // Check block row-vector
    copy_vector_d2h(fdbb::get<0>(V), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<1>(V), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<2>(V), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<3>(V), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<4>(V), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<5>(V), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block row-vector view
    copy_vector_d2h(fdbb::get<0>(V), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<1>(V), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<2>(V), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<3>(V), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<4>(V), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<5>(V), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
