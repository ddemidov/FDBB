/** @file test_blockexpr_col_vector_view.hpp
 *
 *  @brief UnitTests++ block col-vector view test
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller, Andrzej Jaeschke
 */

/** @brief
 *  Test creation of block col-vector view
 */

TEST_FIXTURE(FDBB_BLOCKEXPRESSION_FIXTURE, blockexpr_col_vector_view)
{
  fill_vector_d(v0, type_real{ 0.0 });
  fill_vector_d(v1, type_real{ 1.0 });
  fill_vector_d(v2, type_real{ 2.0 });
  fill_vector_d(v3, type_real{ 3.0 });
  fill_vector_d(v4, type_real{ 4.0 });
  fill_vector_d(v5, type_real{ 5.0 });

  TEST_INIT((0),
            (FDBB_BLOCKEXPRESSION_FIXTURE::len),
            (6 * sizeof(type)),
            (FDBB_BLOCKEXPRESSIONFIXTURE::len));

  try {
    TEST_START();

    // Create block column-vector view from sequence of scalar vectors
    fdbb::fdbbBlockColVectorView<0, type_vector_d, 6> A{
      v0, v1, v2, v3, v4, v5
    };

    // Create block column-vector from block column-vector view
    fdbb::fdbbBlockColVector<1, type_vector_d, 6> B{ A };

    // Change content of scalar source vectors
    fill_vector_d(v0, type_real{ 5.0 });
    fill_vector_d(v1, type_real{ 4.0 });
    fill_vector_d(v2, type_real{ 3.0 });
    fill_vector_d(v3, type_real{ 2.0 });
    fill_vector_d(v4, type_real{ 1.0 });
    fill_vector_d(v5, type_real{ 0.0 });

    TEST_STOP();
    TEST_REPORT();

    // Check block column-vector view A
    copy_vector_d2h(fdbb::get<0>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<1>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<2>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<3>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<4>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<5>(A), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    // Check block column-vector B
    copy_vector_d2h(fdbb::get<0>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 0.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<1>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 1.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<2>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 2.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<3>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 3.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<4>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 4.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

    copy_vector_d2h(fdbb::get<5>(B), result_h);
    fill_vector_h(dummy_h, type_real{ 5.0 });
    CHECK_ARRAY_CLOSE(
      result_h, dummy_h, FDBB_BLOCKEXPRESSION_FIXTURE::len, type_real{ 1e-5 });

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
