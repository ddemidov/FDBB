SUITE(suite_@SUITE_SPEC@)
{
    typedef FixtureCMTL<@SIZE@,
                        @PRECISION@,
                        mtl::dense_vector<@PRECISION@>,
                        mtl::dense2D<@PRECISION@> > Fixture;

#define FDBB_FIXTURE Fixture

        typedef BlockExpressionFixtureCMTL<@SIZE@,
                        @PRECISION@,
                        mtl::dense_vector<@PRECISION@>,
                        mtl::dense2D<@PRECISION@> > BlockExpressionFixture;

#define FDBB_BLOCKEXPRESSION_FIXTURE BlockExpressionFixture

    typedef ConservativeFixture1dCMTL<@SIZE@,
                                      @PRECISION@,
                                      mtl::dense_vector<@PRECISION@>,
                                      mtl::dense2D<@PRECISION@> > ConservativeFixture1d;

#define FDBB_CONSERVATIVE_FIXTURE_1D ConservativeFixture1d

    typedef ConservativeFixture2dCMTL<@SIZE@,
                                      @PRECISION@,
                                      mtl::dense_vector<@PRECISION@>,
                                      mtl::dense2D<@PRECISION@> > ConservativeFixture2d;

#define FDBB_CONSERVATIVE_FIXTURE_2D ConservativeFixture2d

    typedef ConservativeFixture3dCMTL<@SIZE@,
                                      @PRECISION@,
                                      mtl::dense_vector<@PRECISION@>,
                                      mtl::dense2D<@PRECISION@> > ConservativeFixture3d;

#define FDBB_CONSERVATIVE_FIXTURE_3D ConservativeFixture3d

    typedef PrimitiveFixture1dCMTL<@SIZE@,
                                      @PRECISION@,
                                      mtl::dense_vector<@PRECISION@>,
                                      mtl::dense2D<@PRECISION@> > PrimitiveFixture1d;

#define FDBB_PRIMITIVE_FIXTURE_1D PrimitiveFixture1d

    typedef PrimitiveFixture2dCMTL<@SIZE@,
                                      @PRECISION@,
                                      mtl::dense_vector<@PRECISION@>,
                                      mtl::dense2D<@PRECISION@> > PrimitiveFixture2d;

#define FDBB_PRIMITIVE_FIXTURE_2D PrimitiveFixture2d

    typedef PrimitiveFixture3dCMTL<@SIZE@,
                                      @PRECISION@,
                                      mtl::dense_vector<@PRECISION@>,
                                      mtl::dense2D<@PRECISION@> > PrimitiveFixture3d;

#define FDBB_PRIMITIVE_FIXTURE_3D PrimitiveFixture3d

// Define macro to count the number of arguments
#define NARGS(...) std::tuple_size<decltype(std::make_tuple(__VA_ARGS__))>::value

// Define timer macros
#ifdef ENABLE_PERFMODE

#define TEST_INIT(flops,flops_size,memops,memops_size) \
                         PerfCounter<NARGS(flops),NARGS(memops)> \
                         perfCounter({flops},{flops_size},{memops},{memops_size},@RUNS@,100)
#define TEST_START(...)  perfCounter.start()
#define TEST_STOP(...)   perfCounter.stop()
#define TEST_TIMER(...)  perfCounter
#define TEST_RUNS(...)   perfCounter.getRuns()
#define TEST_REPORT(...) perfCounter.print(std::clog, \
                         "[" + std::string(m_details.suiteName) + "." + std::string(m_details.testName) + "] ", "\n")

#else

#define TEST_INIT(...)   Timer timer(@RUNS@,100)
#define TEST_START(...)  timer.start()
#define TEST_STOP(...)   timer.stop()
#define TEST_TIMER(...)  timer
#define TEST_RUNS(...)   timer.getRuns()
#define TEST_REPORT(...) timer.print(std::clog, \
                         "[" + std::string(m_details.suiteName) + "." + std::string(m_details.testName) + "] ", "\n")

#endif

// Include unittest header files
@INCLUDE_UNITTESTS_HEADERS@

// Undefine timer macros
#undef TEST_INIT
#undef TEST_START
#undef TEST_STOP
#undef TEST_TIMER
#undef TEST_RUNS
#undef TEST_REPORT

} // suite_@SUITE_SPEC@

