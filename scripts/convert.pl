#!/usr/bin/env perl

use strict;
use warnings;

my $leadingBlanks = "";
my $inComment = 0;
while (<>) {
    if (m/^(\s*)\/\*\*/) { 
	$inComment=1;
	$leadingBlanks=$1;
    } 
    if (m/[^\\]*\*\//) { 
	$inComment=0;
	$leadingBlanks="";
    }
    if ($inComment) {
	# Place an asterisk in column 2 (counting from '/' that starts the
	# comment block) of a non-empty (continued) comment that is properly
	# indented (e.g. has 3 blanks, counting from '/' that starts the
	# comment block)
	s/^$leadingBlanks   /$leadingBlanks * /;

	# Place an asterisk in column 2 plus trailing blank of a non-empty
	# (continued) comment that is improperly indented (e.g. only has 3
	# blanks)
	s/^$leadingBlanks  /$leadingBlanks * /;

	# Place an asterisk in column 2 for an empty line within a comment
	s/^(\s*)$/$leadingBlanks *$1/;
    }
    print;
}
