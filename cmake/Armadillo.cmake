########################################################################
# Armadillo.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# Armadillo
########################################################################

if (FDBB_BUILTIN_ARMADILLO)
  
  include(DownloadProject)
  download_project(
    PROJ              Armadillo
    URL               http://downloads.sourceforge.net/project/arma/armadillo-7.800.2.tar.xz
    URL_MD5           c601f3a5ec6d50666aa3a539fa20e6ca
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/Armadillo
    PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Armadillo/Armadillo-src < ${CMAKE_SOURCE_DIR}/patches/Armadillo.patch
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
  
  # Process Armadillo project
  if (NOT TARGET armadillo)
    add_subdirectory(${Armadillo_SOURCE_DIR} ${Armadillo_BINARY_DIR})
  endif()
  
  # Add include directory
  include_directories("${Armadillo_SOURCE_DIR}/include")
  
  # Add library
  list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   armadillo)
  list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES armadillo)

else()
  
  # Add include directory
  if(ARMADILLO_INCLUDE_PATH)
    include_directories(${ARMADILLO_INCLUDE_PATH})
  else()
    message(WARNING "Variable ARMADILLO_INCLUDE_PATH is not defined. FDBB might be unable to find Armadillo include files.")
  endif()

  # Add libraries
  if(ARMADILLO_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${ARMADILLO_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${ARMADILLO_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable ARMADILLO_LIBRARIES must point to the Armadillo libraries.")
  endif()
  
endif()

# Enable Armadillo support
add_definitions(-DSUPPORT_ARMADILLO)
