########################################################################
# VexCL.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# VexCL
########################################################################

if (FDBB_BUILTIN_VEXCL)

  include(DownloadProject)
  download_project(
    PROJ              VexCL
    GIT_REPOSITORY    https://github.com/ddemidov/vexcl.git
    GIT_TAG           master
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/VexCL
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )

  # VexCL depends on the Boost library
  find_package(Boost COMPONENTS
    date_time
    filesystem
    system
    thread
    unit_test_framework
    program_options QUIET REQUIRED)
  include_directories(${Boost_INCLUDE_DIRS})
  set(LIBS ${LIBS} ${Boost_LIBRARIES})
  
  list(APPEND FDBB_C_TARGET_LINK_LIBRARIES ${Boost_LIBRARIES})
  list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${Boost_LIBRARIES})
  
  # Set VexCL backend
  if(FDBB_WITH_CUDA)
    add_definitions(-DVEXCL_BACKEND_CUDA)
    include_directories(${CUDA_INCLUDE_DIRS})
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES cuda)
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES cuda)
  elseif(FDBB_WITH_OCL)
    add_definitions(-DVEXCL_BACKEND_OPENCL)
    include_directories(${OpenCL_INCLUDE_DIRS})
  else()
    if (UNIX AND NOT APPLE)
      list(APPEND FDBB_C_TARGET_LINK_LIBRARIES dl)
      list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES dl)
    endif()
    add_definitions(-DVEXCL_BACKEND_JIT -DVEXCL_JIT_COMPILER="${CMAKE_CXX_COMPILER}" -DVEXCL_JIT_COMPILER_OPTIONS="$<$<CXX_COMPILER_ID:GNU>:-fPIC> -shared ${CMAKE_CXX_FLAGS} $<$<CONFIG:Debug>:${CMAKE_CXX_FLAGS_DEBUG}>$<$<CONFIG:Release>:${CMAKE_CXX_FLAGS_RELEASE}>$<$<CONFIG:RelWithDebInfo>:${CMAKE_CXX_FLAGS_RELWITHDEBINFO}> -I${Boost_INCLUDE_DIRS}")
  endif()
  
  # Add include directory
  include_directories("${VexCL_SOURCE_DIR}")

else()

  # Add include directory
  if(VEXCL_INCLUDE_PATH)
    include_directories(${VEXCL_INCLUDE_PATH})
  else()
    message(WARNING "Variable VEXCL_INCLUDE_PATH is not defined. FDBB might be unable to find VexCL include files.")
  endif()
  
  # Add libraries
  if(VEXCL_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${VEXCL_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${VEXCL_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable VEXCL_LIBRARIES must point to the VexCL libraries.")
  endif()
  
endif()

# Enable VexCL support
add_definitions(-DSUPPORT_VEXCL)
