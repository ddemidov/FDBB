########################################################################
# Blaze.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

# Blaze requires C++14-support enabled
if(NOT ${FDBB_CXX_STANDARD} MATCHES "14")
  message(FATAL_ERROR "Blaze must be compiled with C++14-support enabled")
endif()
  
########################################################################
# Blaze
########################################################################

if (FDBB_BUILTIN_BLAZE)

  # Download Blaze
  include(DownloadProject)
  download_project(
    PROJ              Blaze
    GIT_REPOSITORY    https://bitbucket.org/blaze-lib/blaze.git
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/Blaze
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
  
  # Blaze depends on the Boost library
  find_package(Boost COMPONENTS
    system
    thread
    QUIET REQUIRED)
  include_directories(${Boost_INCLUDE_DIRS})
  set(LIBS ${LIBS} ${Boost_LIBRARIES})
  
  # Blaze depends on Threads
  set(THREADS_PREFER_PTHREAD_FLAG ON)
  find_package(Threads QUIET REQUIRED)
  list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${CMAKE_THREAD_LIBS_INIT})
  list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${CMAKE_THREAD_LIBS_INIT})
    
  # Add include directory
  include_directories("${Blaze_SOURCE_DIR}")

else()

  # Add include directory
  if(BLAZE_INCLUDE_PATH)
    include_directories(${BLAZE_INCLUDE_PATH})
  else()
    message(WARNING "Variable BLAZE_INCLUDE_PATH is not defined. FDBB might be unable to find Blaze include files.")
  endif()
  
endif()

# Enable Blaze support
add_definitions(-DSUPPORT_BLAZE)

# Enable OpenMP support
if(NOT OPENMP_FOUND)
  add_definitions(-DBLAZE_USE_CPP_THREADS)
endif()
