########################################################################
# Eigen.cmake
#
# Author: Matthias Moller
# Copyright (C) 2015 - 2017 by the FDBB authors
#
# This file is part of the FDBB library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# Eigen
########################################################################

if (FDBB_BUILTIN_EIGEN MATCHES "3.2")
  
  include(DownloadProject)
  download_project(
    PROJ              Eigen
    URL               http://bitbucket.org/eigen/eigen/get/3.2.10.tar.bz2
    URL_MD5           cad3e2079d9d97e9a95f854a298e9c0e
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
    PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.2.patch
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
    
  # Add include directory
  include_directories("${Eigen_SOURCE_DIR}")

elseif (FDBB_BUILTIN_EIGEN MATCHES "3.3")
  
  include(DownloadProject)
  download_project(
    PROJ              Eigen
    URL               http://bitbucket.org/eigen/eigen/get/3.3.3.tar.bz2
    URL_MD5           b2ddade41040d9cf73b39b4b51e8775b
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/Eigen
    PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/Eigen/Eigen-src < ${CMAKE_SOURCE_DIR}/patches/Eigen3.3.patch
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
    
  # Add include directory
  include_directories("${Eigen_SOURCE_DIR}")
  
else()

  # Add include directory
  if(EIGEN_INCLUDE_PATH)
    include_directories(${EIGEN_INCLUDE_PATH})
  else()
    message(WARNING "Variable EIGEN_INCLUDE_PATH is not defined. FDBB might be unable to find Eigen include files.")
  endif()
  
  # Add libraries
  if(EIGEN_LIBRARIES)
    list(APPEND FDBB_C_TARGET_LINK_LIBRARIES   ${EIGEN_LIBRARIES})
    list(APPEND FDBB_CXX_TARGET_LINK_LIBRARIES ${EIGEN_LIBRARIES})
  else()
    message(FATAL_ERROR "Variable EIGEN_LIBRARIES must point to the Eigen libraries.")
  endif()
  
endif()

# Enable Eigen support
add_definitions(-DSUPPORT_EIGEN)
