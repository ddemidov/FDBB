/** @file fdbbBlockExpressions.hpp
 *
 *  @brief Block expression class
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_BLOCK_EXPRESSIONS_HPP
#define FDBB_BLOCK_EXPRESSIONS_HPP

#include <array>
#include <initializer_list>
#include <memory>
#include <stdexcept>
#include <tuple>

#include "fdbbBlockExpressionsForward.hpp"
#include "fdbbCompatibility.hpp"
#include "fdbbConfig.hpp"
#include "fdbbFunctors.hpp"
#include "fdbbUtils.hpp"

namespace fdbb {

/** @brief
 *  A fixed-size block row-vector
 *
 *  This is a block row-vector container for a fixed number of
 *  scalar vectors of type T. It supports all arithmetic
 *  operations like addition, subtraction, element-wise
 *  multiplication and division. Moreover, it can be
 *  transposed by returning a view of a block column-vector.
 */
template<std::size_t Tag, typename T, std::size_t _cols>
struct fdbbBlockRowVector : public fdbbBlockRowVectorBase
{
private:
  /// @brief Self type
  using self_type = fdbbBlockRowVector<Tag, T, _cols>;

  /// @brief Expression type
  using expr_type = std::array<T, _cols>;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

  /// @brief Number of rows
  static constexpr std::size_t rows = 1;

  /// @brief Number of columns
  static constexpr std::size_t cols = _cols;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  fdbbBlockRowVector() = delete;

  /// @brief Constructor (copy from parameter pack)
  template<
    typename... Ts,
    typename = typename std::enable_if<
      !std::is_base_of<fdbbBlockBase,
                       typename fdbb::remove_all<typename std::tuple_element<
                         0,
                         std::template tuple<Ts...>>::type>::type>::value &&
      _cols == sizeof...(Ts)>::type>
  explicit fdbbBlockRowVector(Ts&&... args)
    : expr{ args... }
  {
  }

  /// @brief Constructor (copy from block expression)
  template<std::size_t TagOther, typename... Ts>
  fdbbBlockRowVector(
    const fdbbBlockExpr<TagOther, _cols, 1, false, Ts...>& other)
  {
    utils::for_each(other.get(), expr, functor::assign());
    std::cout << "COPY1\n";
  }

  /// @brief Constructor (move from to block expression)
  template<std::size_t TagOther, typename... Ts>
  fdbbBlockRowVector(fdbbBlockExpr<TagOther, _cols, 1, false, Ts...>&& other)
  {
    utils::for_each(other.get(), expr, functor::move_assign());
    std::cout << "COPY2\n";
  }

  /// @brief Constructor (copy from to block row-vector)
  template<std::size_t TagOther>
  explicit fdbbBlockRowVector(
    const fdbbBlockRowVector<TagOther, T, _cols>& other)
    : expr{ other.get() }
  {
  }

  /// @brief Constructor (copy and transpose from block column-vector)
  template<std::size_t TagOther>
  explicit fdbbBlockRowVector(
    const fdbbBlockColVector<TagOther, T, _cols>& other)
    : expr{ other.get() }
  {
  }

  /// @brief Constructor (copy from block row-vector view)
  template<std::size_t TagOther>
  explicit fdbbBlockRowVector(
    const fdbbBlockRowVectorView<TagOther, T, _cols>& other)
    : expr(utils::apply(functor::make_array(),
                        utils::for_each_return(other.get(), functor::deref())))
  {
  }

  /// @brief Constructor (copy and transpose from block column-vector view)
  template<std::size_t TagOther>
  explicit fdbbBlockRowVector(
    const fdbbBlockColVectorView<TagOther, T, _cols>& other)
    : expr(utils::apply(functor::make_array(),
                        utils::for_each_return(other.get(), functor::deref())))
  {
  }

  /// @brief Constructor (move from block row-vector)
  template<std::size_t TagOther>
  explicit fdbbBlockRowVector(fdbbBlockRowVector<TagOther, T, _cols>&& other)
    : expr{ std::move(other.get()) }
  {
  }

  /// @brief Constructor (move and transpose from block column-vector)
  template<std::size_t TagOther>
  explicit fdbbBlockRowVector(fdbbBlockColVector<TagOther, T, _cols>&& other)
    : expr{ std::move(other.get()) }
  {
  }

  // /// @brief Returns the transposed of the row-vector
  // fdbbBlockColVectorView<utils::hash<std::size_t>(tag, 'T'),
  //                        T,
  //                        _cols> constexpr const transpose() const
  // {
  //   return fdbbBlockColVectorView<utils::hash<std::size_t>(tag, 'T'), T,
  //   _cols>(
  //     *this);
  // }

  /// @brief Returns the transposed of the row-vector
  fdbbBlockColVectorView<utils::hash<std::size_t>(tag, 'T'), T, _cols>
  transpose()
  {
    return fdbbBlockColVectorView<utils::hash<std::size_t>(tag, 'T'), T, _cols>(
      *this);
  }

  /// @brief Returns constant reference to scalar vector from column number \a
  /// col
  constexpr const T& operator()(const std::size_t& col) const
  {
    return expr[col];
  }

  /// @brief Returns reference to scalar vector from column number \a col
  T& operator()(const std::size_t& col) { return expr[col]; }

  /// @brief Returns constant reference to scalar vector from column number \a
  /// col
  template<std::size_t col>
  constexpr const T& get() const
  {
    static_assert(col < _cols, "Index exceeds bounds");
    return expr[col];
  }

  /// @brief Returns reference to scalar vector from columne number \a col
  template<std::size_t col>
  T& get()
  {
    static_assert(col < _cols, "Index exceeds bounds");
    return expr[col];
  }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& get() const { return expr; }

  /// @brief Returns reference to raw expressions
  expr_type& get() { return expr; }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& operator->() const { return expr; }

  /// @brief Returns reference to raw expressions
  expr_type& operator->() { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const
  {
    os << "Block row-vector[" << _cols << "]<" << Tag << ">: (";
    for (std::size_t col = 0; col < _cols; col++)
      os << expr[col].size() << (col + 1 < _cols ? "," : ")");
  }
}; // namespace fdbb

/// @brief Prints a short description of the block row-vector
template<std::size_t Tag, typename T, std::size_t _cols>
std::ostream&
operator<<(std::ostream& os, const fdbbBlockRowVector<Tag, T, _cols>& obj)
{
  obj.print(os);
  return os;
}

/// @brief Returns reference to the Ith element of the block row-vector
template<std::size_t I, std::size_t Tag, class T, std::size_t _cols>
constexpr T&
get(fdbbBlockRowVector<Tag, T, _cols>& obj) noexcept
{
  static_assert(I < _cols, "Index exceeds bounds");
  return obj.template get<I>();
}

/// @brief Returns the Ith element of the block row-vector
template<std::size_t I, std::size_t Tag, class T, std::size_t _cols>
constexpr T&&
get(fdbbBlockRowVector<Tag, T, _cols>&& obj) noexcept
{
  static_assert(I < _cols, "Index exceeds bounds");
  return std::move(obj.template get<I>());
}

/// @brief Returns constant reference to the Ith element of the block row-vector
template<std::size_t I, std::size_t Tag, class T, std::size_t _cols>
constexpr const T&
get(const fdbbBlockRowVector<Tag, T, _cols>& obj) noexcept
{
  static_assert(I < _cols, "Index exceeds bounds");
  return obj.template get<I>();
}

/** @brief
 *  A fixed-size block column-vector
 *
 *  This is a block column-vector container for a fixed number of
 *  scalar vectors of type T. It supports all arithmetic
 *  operations like addition, subtraction, element-wise
 *  multiplication and division. Moreover, it can be
 *  transposed by returning a view of a block row-vector.
 */
template<std::size_t Tag, typename T, std::size_t _rows>
struct fdbbBlockColVector : public fdbbBlockColVectorBase
{
private:
  /// @brief Self type
  using self_type = fdbbBlockColVector<Tag, T, _rows>;

  /// @brief Expression type
  using expr_type = std::array<T, _rows>;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

  /// @brief Number of rows
  static constexpr std::size_t rows = _rows;

  /// @brief Number of columns
  static constexpr std::size_t cols = 1;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  fdbbBlockColVector() = delete;

  /// @brief Constructor (copy from parameter pack)
  template<
    typename... Ts,
    typename = typename std::enable_if<
      !std::is_base_of<fdbbBlockBase,
                       typename fdbb::remove_all<typename std::tuple_element<
                         0,
                         std::template tuple<Ts...>>::type>::type>::value &&
      _rows == sizeof...(Ts)>::type>
  fdbbBlockColVector(Ts&&... args)
    : expr{ args... }
  {
  }

  /// @brief Constructor (copy from block expression)
  template<std::size_t TagOther, typename... Ts>
  fdbbBlockColVector(
    const fdbbBlockExpr<TagOther, 1, _rows, false, Ts...>& other)
  {
    utils::for_each(other.get(), expr, functor::assign());
  }

  /// @brief Constructor (move from block expression)
  template<std::size_t TagOther, typename... Ts>
  fdbbBlockColVector(fdbbBlockExpr<TagOther, 1, _rows, false, Ts...>&& other)
  {
    utils::for_each(other.get(), expr, functor::move_assign());
  }

  /// @brief Constructor (copy from block column-vector)
  template<std::size_t TagOther>
  explicit fdbbBlockColVector(
    const fdbbBlockColVector<TagOther, T, _rows>& other)
    : expr{ other.get() }
  {
  }

  /// @brief Constructor (copy and transpose from block row-vector)
  template<std::size_t TagOther>
  explicit fdbbBlockColVector(
    const fdbbBlockRowVector<TagOther, T, _rows>& other)
    : expr{ other.get() }
  {
  }

  /// @brief Constructor (copy from block column-vector view)
  template<std::size_t TagOther>
  explicit fdbbBlockColVector(
    const fdbbBlockColVectorView<TagOther, T, _rows>& other)
    : expr(utils::apply(functor::make_array(),
                        utils::for_each_return(other.get(), functor::deref())))
  {
  }

  /// @brief Constructor (copy and transpose from block row-vector view)
  template<std::size_t TagOther>
  explicit fdbbBlockColVector(
    const fdbbBlockRowVectorView<TagOther, T, _rows>& other)
    : expr(utils::apply(functor::make_array(),
                        utils::for_each_return(other.get(), functor::deref())))
  {
  }

  /// @brief Constructor (move from block column-vector)
  template<std::size_t TagOther>
  explicit fdbbBlockColVector(fdbbBlockColVector<TagOther, T, _rows>&& other)
    : expr{ std::move(other.get()) }
  {
  }

  /// @brief Constructor (move and transpose from block row-vector)
  template<std::size_t TagOther>
  explicit fdbbBlockColVector(fdbbBlockRowVector<TagOther, T, _rows>&& other)
    : expr{ std::move(other.get()) }
  {
  }

  // /// @brief Returns the transposed of the column-vector
  // fdbbBlockRowVectorView<utils::hash<std::size_t>(tag, 'T'),
  //                        T,
  //                        _rows> constexpr const transpose() const
  // {
  //   return fdbbBlockRowVectorView<utils::hash<std::size_t>(tag, 'T'), T,
  //   _rows>(
  //     *this);
  // }

  /// @brief Returns the transposed of the column-vector
  fdbbBlockRowVectorView<utils::hash<std::size_t>(tag, 'T'), T, _rows>
  transpose()
  {
    return fdbbBlockRowVectorView<utils::hash<std::size_t>(tag, 'T'), T, _rows>(
      *this);
  }

  /// @brief Returns constant reference to scalar vector from row number \a row
  constexpr const T& operator()(const std::size_t& row) const
  {
    return expr[row];
  }

  /// @brief Returns reference to scalar vector from row number \a row
  T& operator()(const std::size_t& row) { return expr[row]; }

  /// @brief Returns constant reference to scalar vector from row number \a row
  template<std::size_t row>
  constexpr const T& get() const
  {
    static_assert(row < _rows, "Index exceeds bounds");
    return expr[row];
  }

  /// @brief Returns reference to scalar vector from row number \a row
  template<std::size_t row>
  T& get()
  {
    static_assert(row < _rows, "Index exceeds bounds");
    return expr[row];
  }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& get() const { return expr; }

  /// @brief Returns reference to raw expressions
  expr_type& get() { return expr; }

  /// @brief Returns constant reference to raw expression
  constexpr const expr_type& operator->() const { return expr; }

  /// @brief Returns reference to raw expression
  expr_type& operator->() { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const
  {
    os << "Block column-vector[" << _rows << "]<" << Tag << ">: (";
    for (std::size_t row = 0; row < _rows; row++)
      os << expr[row].size() << (row + 1 < _rows ? "," : ")");
  }
};

/// @brief Prints a short description of the block column-vector
template<std::size_t Tag, typename T, std::size_t _rows>
std::ostream&
operator<<(std::ostream& os, const fdbbBlockColVector<Tag, T, _rows>& obj)
{
  obj.print(os);
  return os;
}

/// @brief Returns reference to the Ith element of the block column-vector
template<std::size_t I, std::size_t Tag, class T, std::size_t _rows>
constexpr T&
get(fdbbBlockColVector<Tag, T, _rows>& obj) noexcept
{
  static_assert(I < _rows, "Index exceeds bounds");
  return obj.template get<I>();
}

/// @brief Returns the Ith element of the block column-vector
template<std::size_t I, std::size_t Tag, class T, std::size_t _rows>
constexpr T&&
get(fdbbBlockColVector<Tag, T, _rows>&& obj) noexcept
{
  static_assert(I < _rows, "Index exceeds bounds");
  return std::move(obj.template get<I>());
}

/// @brief Returns constant reference to the Ith element of the block
/// column-vector
template<std::size_t I, std::size_t Tag, class T, std::size_t _rows>
constexpr const T&
get(const fdbbBlockColVector<Tag, T, _rows>& obj) noexcept
{
  static_assert(I < _rows, "Index exceeds bounds");
  return obj.template get<I>();
}

/** @brief
 *  A view on a fixed-size block row-vector
 *
 *  This is a view on a fixed-size block row-vector. It
 *  supports exactly the same functionality but keeps
 *  only references to external data arrays.
 */
template<std::size_t Tag, typename T, std::size_t _cols>
struct fdbbBlockRowVectorView : public fdbbBlockRowVectorBase
{
private:
  /// @brief Self type
  using self_type = fdbbBlockRowVectorView<Tag, T, _cols>;

  /// @brief Expression type
  using expr_type = std::array<T*, _cols>;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

  /// @brief Number of rows
  static constexpr std::size_t rows = 1;

  /// @brief Number of columns
  static constexpr std::size_t cols = _cols;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  fdbbBlockRowVectorView() = delete;

  /// @brief Constructor (copy from parameter pack)
  template<
    typename... Ts,
    typename = typename std::enable_if<
      !std::is_base_of<fdbbBlockBase,
                       typename fdbb::remove_all<typename std::tuple_element<
                         0,
                         std::template tuple<Ts...>>::type>::type>::value &&
      _cols == sizeof...(Ts)>::type>
  explicit fdbbBlockRowVectorView(Ts&&... args)
    : expr{ &args... }
  {
  }

  /// @brief Constructor (copy from parameter pack)
  template<
    typename... Ts,
    typename = typename std::enable_if<
      !std::is_base_of<fdbbBlockBase,
                       typename fdbb::remove_all<typename std::tuple_element<
                         0,
                         std::template tuple<Ts...>>::type>::type>::value &&
      _cols == sizeof...(Ts)>::type>
  explicit fdbbBlockRowVectorView(Ts*... args)
    : expr{ args... }
  {
  }

  /// @brief Constructor (copy from block row-vector)
  template<std::size_t TagOther>
  explicit fdbbBlockRowVectorView(fdbbBlockRowVector<TagOther, T, _cols>& other)
  {
    for (std::size_t col = 0; col < _cols; col++)
      expr[col] = &other(col);
  }

  /// @brief Constructor (copy and transpose from block column-vector)
  template<std::size_t TagOther>
  explicit fdbbBlockRowVectorView(fdbbBlockColVector<TagOther, T, _cols>& other)
  {
    for (std::size_t col = 0; col < _cols; col++)
      expr[col] = &other(col);
  }

  /// @brief Constructor (copy from block row-vector view)
  template<std::size_t TagOther>
  explicit fdbbBlockRowVectorView(
    const fdbbBlockRowVectorView<TagOther, T, _cols>& other)
    : expr{ other.get() }
  {
  }

  /// @brief Constructor (copy and transpose from block column-vector view)
  template<std::size_t TagOther>
  explicit fdbbBlockRowVectorView(
    const fdbbBlockColVectorView<TagOther, T, _cols>& other)
    : expr{ other.get() }
  {
  }

  /// @brief Constructor (move from block row-vector view)
  template<std::size_t TagOther>
  explicit fdbbBlockRowVectorView(
    fdbbBlockRowVectorView<TagOther, T, _cols>&& other)
    : expr{ std::move(other.get()) }
  {
  }

  /// @brief Constructor (move and transpose from block column-vector view)
  template<std::size_t TagOther>
  explicit fdbbBlockRowVectorView(
    fdbbBlockColVectorView<TagOther, T, _cols>&& other)
    : expr{ std::move(other.get()) }
  {
  }

  /// @brief Returns the transposed of the row-vector view
  fdbbBlockColVectorView<utils::hash<std::size_t>(tag, 'T'),
                         T,
                         _cols> constexpr const transpose() const
  {
    return fdbbBlockColVectorView<utils::hash<std::size_t>(tag, 'T'), T, _cols>(
      *this);
  }

  /// @brief Returns the transposed of the row-vector view
  fdbbBlockColVectorView<utils::hash<std::size_t>(tag, 'T'), T, _cols>
  transpose()
  {
    return fdbbBlockColVectorView<utils::hash<std::size_t>(tag, 'T'), T, _cols>(
      *this);
  }

  /// @brief Returns constant reference to scalar vector from column number \a
  /// col
  constexpr const T& operator()(const std::size_t& col) const
  {
    return *expr[col];
  }

  /// @brief Returns reference to scalar vector from column number \a col
  T& operator()(const std::size_t& col) { return *expr[col]; }

  /// @brief Returns constant reference to scalar vector from column number \a
  /// col
  template<std::size_t col>
  constexpr const T& get() const
  {
    static_assert(col < _cols, "Index exceeds bounds");
    return *expr[col];
  }

  /// @brief Returns reference to scalar vector from column number \a col
  template<std::size_t col>
  T& get()
  {
    static_assert(col < _cols, "Index exceeds bounds");
    return *expr[col];
  }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& get() const { return expr; }

  /// @brief Returns reference to raw expressions
  expr_type& get() { return expr; }

  /// @brief Returns constant reference to raw expression
  constexpr const expr_type& operator->() const { return expr; }

  /// @brief Returns reference to raw expression
  expr_type& operator->() { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const
  {
    os << "Block row-vector view[" << _cols << "]<" << Tag << ">: (";
    for (std::size_t col = 0; col < _cols; col++)
      os << expr[col]->size() << (col + 1 < _cols ? "," : ")");
  }
};

/// @brief Prints a short description of the block row-vector view
template<std::size_t Tag, typename T, std::size_t _cols>
std::ostream&
operator<<(std::ostream& os, const fdbbBlockRowVectorView<Tag, T, _cols>& obj)
{
  obj.print(os);
  return os;
}

/// @brief Returns reference to the Ith element of the block row-vector view
template<std::size_t I, std::size_t Tag, class T, std::size_t _cols>
constexpr T&
get(fdbbBlockRowVectorView<Tag, T, _cols>& obj) noexcept
{
  static_assert(I < _cols, "Index exceeds bounds");
  return obj.template get<I>();
}

/// @brief Returns the Ith element of the block row-vector view
template<std::size_t I, std::size_t Tag, class T, std::size_t _cols>
constexpr T&&
get(fdbbBlockRowVectorView<Tag, T, _cols>&& obj) noexcept
{
  static_assert(I < _cols, "Index exceeds bounds");
  return std::move(obj.template get<I>());
}

/// @brief Returns constant reference to the Ith element of the block row-vector
/// view
template<std::size_t I, std::size_t Tag, class T, std::size_t _cols>
constexpr const T&
get(const fdbbBlockRowVectorView<Tag, T, _cols>& obj) noexcept
{
  static_assert(I < _cols, "Index exceeds bounds");
  return obj.template get<I>();
}

/** @brief
 *  A view on a fixed-size block column-vector
 *
 *  This is a view on a fixed-size block column-vector. It
 *  supports exactly the same functionality but keeps
 *  only references to external data arrays.
 */
template<std::size_t Tag, typename T, std::size_t _rows>
struct fdbbBlockColVectorView : public fdbbBlockColVectorBase
{
private:
  /// @brief Self type
  using self_type = fdbbBlockColVectorView<Tag, T, _rows>;

  /// @brief Expression type
  using expr_type = std::array<T*, _rows>;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

  /// @brief Number of rows
  static constexpr std::size_t rows = _rows;

  /// @brief Number of columns
  static constexpr std::size_t cols = 1;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  fdbbBlockColVectorView() = delete;

  /// @brief Constructor (copy from parameter pack)
  template<
    typename... Ts,
    typename = typename std::enable_if<
      !std::is_base_of<fdbbBlockBase,
                       typename fdbb::remove_all<typename std::tuple_element<
                         0,
                         std::template tuple<Ts...>>::type>::type>::value &&
      _rows == sizeof...(Ts)>::type>
  explicit fdbbBlockColVectorView(Ts&&... args)
    : expr{ &args... }
  {
  }

  /// @brief Constructor (copy from parameter pack)
  template<
    typename... Ts,
    typename = typename std::enable_if<
      !std::is_base_of<fdbbBlockBase,
                       typename fdbb::remove_all<typename std::tuple_element<
                         0,
                         std::template tuple<Ts...>>::type>::type>::value &&
      _rows == sizeof...(Ts)>::type>
  fdbbBlockColVectorView(Ts*... args)
    : expr{ args... }
  {
  }

  /// @brief Constructore (copy from block column-vector)
  template<std::size_t TagOther>
  explicit fdbbBlockColVectorView(fdbbBlockColVector<TagOther, T, _rows>& other)
  {
    for (std::size_t row = 0; row < _rows; row++)
      expr[row] = &other(row);
  }

  /// @brief Constructor (copy and transpose from block row-vector)
  template<std::size_t TagOther>
  explicit fdbbBlockColVectorView(fdbbBlockRowVector<TagOther, T, _rows>& other)
  {
    for (std::size_t row = 0; row < _rows; row++)
      expr[row] = &other(row);
  }

  /// @brief Constructor (copy from block column-vector view)
  template<std::size_t TagOther>
  explicit fdbbBlockColVectorView(
    const fdbbBlockColVectorView<TagOther, T, _rows>& other)
    : expr{ other.get() }
  {
  }

  /// @brief Constructor (copy and transpose from block row-vector view)
  template<std::size_t TagOther>
  explicit fdbbBlockColVectorView(
    const fdbbBlockRowVectorView<TagOther, T, _rows>& other)
    : expr{ other.get() }
  {
  }

  /// @brief Constructor (move from block column-vector view)
  template<std::size_t TagOther>
  explicit fdbbBlockColVectorView(
    fdbbBlockColVectorView<TagOther, T, _rows>&& other)
    : expr{ std::move(other.get()) }
  {
  }

  /// @brief Constructor (move and transpose from block row-vector view)
  template<std::size_t TagOther>
  explicit fdbbBlockColVectorView(
    fdbbBlockRowVectorView<TagOther, T, _rows>&& other)
    : expr{ std::move(other.get()) }
  {
  }

  /// @brief Returns the transposed of the column-vector view
  fdbbBlockRowVectorView<utils::hash<std::size_t>(tag, 'T'),
                         T,
                         _rows> constexpr const transpose() const
  {
    return fdbbBlockRowVectorView<utils::hash<std::size_t>(tag, 'T'), T, _rows>(
      *this);
  }

  /// @brief Returns the transposed of the column-vector view
  fdbbBlockRowVectorView<utils::hash<std::size_t>(tag, 'T'), T, _rows>
  transpose()
  {
    return fdbbBlockRowVectorView<utils::hash<std::size_t>(tag, 'T'), T, _rows>(
      *this);
  }

  /// @brief Returns constant reference to scalar vector from row number \a row
  constexpr const T& operator()(const std::size_t& row) const
  {
    return *expr[row];
  }

  /// @brief Returns reference to scalar vector from row number \a row
  T& operator()(const std::size_t& row) { return *expr[row]; }

  /// @brief Returns constant reference to scalar vector from row number \a row
  template<std::size_t row>
  constexpr const T& get() const
  {
    static_assert(row < _rows, "Index exceeds bounds");
    return *expr[row];
  }

  /// @brief Returns reference to scalar vector from row number \a row
  template<std::size_t row>
  T& get()
  {
    static_assert(row < _rows, "Index exceeds bounds");
    return *expr[row];
  }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& get() const { return expr; }

  /// @brief Returns reference to raw expressions
  expr_type& get() { return expr; }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& operator->() const { return expr; }

  /// @brief Returns reference to raw expressions
  expr_type& operator->() { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const
  {
    os << "Block column-vector view[" << _rows << "]<" << Tag << ">: (";
    for (std::size_t row = 0; row < _rows; row++)
      os << expr[row]->size() << (row + 1 < _rows ? "," : ")");
  }
};

/// @brief Prints a short description of the block column-vector view
template<std::size_t Tag, typename T, std::size_t _rows>
std::ostream&
operator<<(std::ostream& os, const fdbbBlockColVectorView<Tag, T, _rows>& obj)
{
  obj.print(os);
  return os;
}

/// @brief Returns reference to the Ith element of the block column-vector view
template<std::size_t I, std::size_t Tag, class T, std::size_t _rows>
constexpr T&
get(fdbbBlockColVectorView<Tag, T, _rows>& obj) noexcept
{
  static_assert(I < _rows, "Index exceeds bounds");
  return obj.template get<I>();
}

/// @brief Returns the Ith element of the block column-vector view
template<std::size_t I, std::size_t Tag, class T, std::size_t _rows>
constexpr T&&
get(fdbbBlockColVectorView<Tag, T, _rows>&& obj) noexcept
{
  static_assert(I < _rows, "Index exceeds bounds");
  return std::move(obj.template get<I>());
}

/// @brief Returns constant reference to the Ith element of the block
/// column-vector view
template<std::size_t I, std::size_t Tag, class T, std::size_t _rows>
constexpr const T&
get(const fdbbBlockColVectorView<Tag, T, _rows>& obj) noexcept
{
  static_assert(I < _rows, "Index exceeds bounds");
  return obj.template get<I>();
}

/** @brief
 *  A fixed-size block expression
 *
 *  This is a block expression container for a fixed number of scalar
 *  expressions. It supports all arithmetic operations like addition,
 *  subtraction, element-wise multiplication and division. Moreover,
 *  it can be transposed by returning a view of a block matrix.
 *
 *  @note
 *  Block expressions are stored in row-wise order in a \ref
 *  std::tuple. The compile-time flag \a transposed only changes the
 *  behavior of the \ref get member functions.
 */
template<std::size_t Tag,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed,
         typename... Ts>
struct fdbbBlockExpr : public fdbbBlockBase
{
private:
  /// @brief Self type
  using self_type = fdbbBlockExpr<Tag, _rows, _cols, transposed, Ts...>;

  /// @brief Expression type
  using expr_type = std::tuple<Ts...>;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

  /// @brief Number of rows
  static constexpr std::size_t rows = _rows;

  /// @brief Number of columns
  static constexpr std::size_t cols = _cols;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  fdbbBlockExpr() = delete;

  /// @brief Constructor (copy from tuple)
  explicit fdbbBlockExpr(const expr_type& tuple)
    : expr{ tuple }
  {
  }

  /// @brief Constructor (move from tuple)
  explicit fdbbBlockExpr(expr_type&& tuple)
    : expr{ tuple }
  {
  }

  /// @brief Returns the transposed of the block expression
  fdbbBlockExpr<utils::hash<std::size_t>(tag, 'T'),
                _cols,
                _rows,
                !transposed,
                Ts...> constexpr const transpose() const
  {
    return fdbbBlockExpr<utils::hash<std::size_t>(tag, 'T'),
                         _cols,
                         _rows,
                         !transposed,
                         Ts...>(expr);
  }

  /// @brief Returns the transposed of the block expression
  fdbbBlockExpr<utils::hash<std::size_t>(tag, 'T'),
                _cols,
                _rows,
                !transposed,
                Ts...>
  transpose()
  {
    return fdbbBlockExpr<utils::hash<std::size_t>(tag, 'T'),
                         _cols,
                         _rows,
                         !transposed,
                         Ts...>(expr);
  }

  /// @brief Returns constant reference to Ith scalar expression
  /// (specialization for non-transposed block expression)
  template<std::size_t I, bool _transposed = transposed>
  constexpr auto get() const noexcept -> const
    typename std::enable_if<!_transposed, decltype(std::get<I>(expr))>::type
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return (std::get<I>(expr));
  }

  /// @brief Returns constant reference to Ith scalar expression
  /// (specialization for transposed block expression)
  template<std::size_t I, bool _transposed = transposed>
  constexpr auto get() const noexcept -> const typename std::enable_if<
    _transposed,
    decltype(std::get<(I < _rows * _cols - 1 ? (I * _cols) % (_rows * _cols - 1)
                                             : I)>(expr))>::type
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return std::get<(I < _rows * _cols - 1 ? (I * _cols) % (_rows * _cols - 1)
                                           : I)>(expr);
  }

  /// @brief Returns reference to Ith scalar expression
  /// (specialization for non-transposed block expression)
  template<std::size_t I, bool _transposed = transposed>
  auto get() noexcept ->
    typename std::enable_if<!_transposed, decltype(std::get<I>(expr))>::type
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return (std::get<I>(expr));
  }

  /// @brief Returns reference to Ith scalar expression
  /// (specialization for transposed block expression)
  template<std::size_t I, bool _transposed = transposed>
  auto get() noexcept -> typename std::enable_if<
    _transposed,
    decltype(std::get<(I < _rows * _cols - 1 ? (I * _cols) % (_rows * _cols - 1)
                                             : I)>(expr))>::type
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return std::get<(I < _rows * _cols - 1 ? (I * _cols) % (_rows * _cols - 1)
                                           : I)>(expr);
  }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& get() const { return expr; }

  /// @brief Returns reference to raw expressions
  expr_type& get() { return expr; }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& operator->() const { return expr; }

  /// @brief Returns reference to raw expression
  expr_type& operator->() { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const
  {
    os << "Block expression[" << _rows << "x" << _cols << "]<" << Tag << ">: ";
    utils::for_each(expr, functor::print_size());
    // for (std::size_t row = 0; row < _rows; row++)
    //  for (std::size_t col = 0; col < _cols; col++)
    //      os
    //          << expr[transposed ? (col * _rows + row) : (row * _cols +
    //          col)].size()
    //          << (col + 1 < _cols ? "," : row + 1 < _rows ? "),(" : "))");
  }
};

/// @brief Prints a short description of the block expression
template<std::size_t Tag,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed,
         typename... Ts>
std::ostream&
operator<<(std::ostream& os,
           const fdbbBlockExpr<Tag, _rows, _cols, transposed, Ts...>& obj)
{
  obj.print(os);
  return os;
}

/// @brief Returns reference to the Ith element of the block expression
template<std::size_t I,
         std::size_t Tag,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed,
         typename... Ts>
constexpr auto
get(fdbbBlockExpr<Tag, _rows, _cols, transposed, Ts...>& obj) noexcept
  -> decltype(obj.template get<I>())
{
  static_assert(I < _rows * _cols, "Index exceeds bounds");
  return obj.template get<I>();
}

/// @brief Returns the Ith element of the block expression
template<std::size_t I,
         std::size_t Tag,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed,
         typename... Ts>
constexpr auto
get(fdbbBlockExpr<Tag, _rows, _cols, transposed, Ts...>&& obj) noexcept
  -> decltype(std::move(obj.template get<I>()))
{
  static_assert(I < _rows, "Index exceeds bounds");
  return std::move(obj.template get<I>());
}

/// @brief Returns constant reference to the Ith element of the block expression
template<std::size_t I,
         std::size_t Tag,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed,
         typename... Ts>
constexpr auto
get(const fdbbBlockExpr<Tag, _rows, _cols, transposed, Ts...>& obj) noexcept
  -> const decltype(obj.template get<I>())
{
  static_assert(I < _rows, "Index exceeds bounds");
  return obj.template get<I>();
}

/// @brief Create \ref fdbb::fdbbBlockExpr object from tuple
template<std::size_t Tag, std::size_t _rows, std::size_t _cols, typename... Ts>
constexpr fdbbBlockExpr<Tag, _rows, _cols, false, Ts...>
make_fdbbBlockExpr(std::tuple<Ts...>&& tuple)
{
  static_assert(_rows * _cols == sizeof...(Ts),
                "Requested col/row combination mismatches size of arguments");
  return fdbbBlockExpr<Tag, _rows, _cols, false, Ts...>(
    std::forward<std::tuple<Ts...>>(tuple));
}

/// @brief Create \ref fdbb::fdbbBlockExpr object from parameter pack
template<std::size_t Tag, std::size_t _rows, std::size_t _cols, typename... Ts>
constexpr fdbbBlockExpr<Tag, _rows, _cols, false, Ts...>
make_fdbbBlockExpr(Ts&&... args)
{
  static_assert(_rows * _cols == sizeof...(args),
                "Requested col/row combination mismatches size of arguments");
  return fdbbBlockExpr<Tag, _rows, _cols, false, Ts...>(
    std::make_tuple(args...));
}

/** @brief
 *  A fixed-size block matrix
 *
 *  This is a block matrix container for a fixed number of scalar
 *  matrices of type T. It supports all arithmetic operations
 *  like addition, subtraction, element-wise multiplication and
 *  division, and matrix-vector multiplication. Moreover, it can be
 *  transposed by returning a view of a block matrix.
 *
 *  @note
 *  Block matrices are stored in row-wise order in a \ref
 *  std::array. The compile-time flag \a transposed only changes the
 *  behavior of the \ref get member functions.
 */
template<std::size_t Tag,
         typename T,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed>
struct fdbbBlockMatrix : public fdbbBlockMatrixBase
{
private:
  /// @brief Self type
  using self_type = fdbbBlockMatrix<Tag, T, _rows, _cols, transposed>;

  /// @brief Expression type
  using expr_type = std::array<T, _rows * _cols>;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

  /// @brief Number of rows
  static constexpr std::size_t rows = _rows;

  /// @brief Number of columns
  static constexpr std::size_t cols = _cols;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  fdbbBlockMatrix() = delete;

  /// @brief Constructor (copy from parameter pack)
  template<
    typename... Ts,
    typename = typename std::enable_if<
      !std::is_base_of<fdbbBlockBase,
                       typename fdbb::remove_all<typename std::tuple_element<
                         0,
                         std::template tuple<Ts...>>::type>::type>::value &&
      _rows * _cols == sizeof...(Ts)>::type>
  explicit fdbbBlockMatrix(Ts&&... args)
    : expr{ args... }
  {
  }

  /// @brief Constructor (copy from block expression)
  template<std::size_t TagOther, typename... Ts>
  fdbbBlockMatrix(
    const fdbbBlockExpr<TagOther, _rows, _cols, transposed, Ts...>& other)
  {
    utils::for_each(other.get(), expr, functor::assign());
  }

  /// @brief Constructor (copy and transpose from block expression)
  template<std::size_t TagOther, typename... Ts>
  fdbbBlockMatrix(
    const fdbbBlockExpr<TagOther, _cols, _rows, !transposed, Ts...>& other)
  {
    utils::for_each(other.get(), expr, functor::assign());
  }

  /// @brief Constructor (move from block expression)
  template<std::size_t TagOther, typename... Ts>
  fdbbBlockMatrix(
    fdbbBlockExpr<TagOther, _rows, _cols, transposed, Ts...>&& other)
  {
    utils::for_each(other.get(), expr, functor::move_assign());
  }

  /// @brief Constructor (move and transpose from block expression)
  template<std::size_t TagOther, typename... Ts>
  fdbbBlockMatrix(
    fdbbBlockExpr<TagOther, _cols, _rows, !transposed, Ts...>&& other)
  {
    utils::for_each(other.get(), expr, functor::move_assign());
  }

  /// @brief Constructor (copy from block matrix)
  template<std::size_t TagOther>
  explicit fdbbBlockMatrix(
    const fdbbBlockMatrix<TagOther, T, _rows, _cols, transposed>& other)
    : expr{ other.get() }
  {
  }

  /// @brief Constructor (copy and transpose from block matrix)
  template<std::size_t TagOther>
  explicit fdbbBlockMatrix(
    const fdbbBlockMatrix<TagOther, T, _cols, _rows, !transposed>& other)
    : expr{ other.get() }
  {
  }

  /// @brief Constructor (copy from block matrix view)
  template<std::size_t TagOther>
  explicit fdbbBlockMatrix(
    const fdbbBlockMatrixView<TagOther, T, _rows, _cols, transposed>& other)
    : expr(utils::apply(functor::make_array(),
                        utils::for_each_return(other.get(), functor::deref())))
  {
  }

  /// @brief Constructor (copy and transpose from block matrix view)
  template<std::size_t TagOther>
  explicit fdbbBlockMatrix(
    const fdbbBlockMatrixView<TagOther, T, _cols, _rows, !transposed>& other)
    : expr(utils::apply(functor::make_array(),
                        utils::for_each_return(other.get(), functor::deref())))
  {
  }

  /// @brief Constructor (move from block matrix)
  template<std::size_t TagOther>
  explicit fdbbBlockMatrix(
    fdbbBlockMatrix<TagOther, T, _rows, _cols, transposed>&& other)
    : expr{ std::move(other.get()) }
  {
  }

  /// @brief Constructor (move and transpose from block matrix)
  template<std::size_t TagOther>
  explicit fdbbBlockMatrix(
    fdbbBlockMatrix<TagOther, T, _cols, _rows, !transposed>&& other)
    : expr{ std::move(other.get()) }
  {
  }

  // /// @brief Returns the transposed of the block matrix
  // fdbbBlockMatrixView<utils::hash<std::size_t>(tag, 'T'),
  //                     T,
  //                     _cols,
  //                     _rows,
  //                     !transposed> constexpr const transpose() const
  // {
  //   return fdbbBlockMatrixView<utils::hash<std::size_t>(tag, 'T'),
  //                              T,
  //                              _cols,
  //                              _rows,
  //                              !transposed>(*this);
  // }

  /// @brief Returns the transposed of the block matrix
  fdbbBlockMatrixView<utils::hash<std::size_t>(tag, 'T'),
                      T,
                      _cols,
                      _rows,
                      !transposed>
  transpose()
  {
    return fdbbBlockMatrixView<utils::hash<std::size_t>(tag, 'T'),
                               T,
                               _cols,
                               _rows,
                               !transposed>(*this);
  }

  /// @brief Returns constant reference to scalar matrix from position
  /// \a idx (row-wise ordering)
  constexpr const T& operator()(const std::size_t& idx) const
  {
    return expr[idx];
  }

  /// @brief Returns reference to scalar matrix from position \a idx
  /// (row-wise ordering)
  T& operator()(const std::size_t& idx) { return expr[idx]; }

  /// @brief Returns constant reference to Ith scalar matrix
  /// (specialization for non-transposed block matrix)
  template<std::size_t I, bool _transposed = transposed>
  constexpr auto get() const noexcept -> const
    typename std::enable_if<!_transposed, T>::type&
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return expr[I];
  }

  /// @brief Returns constant reference to Ith scalar matrix
  /// (specialization for transposed block matrix)
  template<std::size_t I, bool _transposed = transposed>
  constexpr auto get() const noexcept -> const
    typename std::enable_if<_transposed, T>::type&
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return expr[I < _rows * _cols - 1 ? (I * _cols) % (_rows * _cols - 1) : I];
  }

  /// @brief Returns reference to Ith scalar matrix
  /// (specialization for non-transposed block matrix)
  template<std::size_t I, bool _transposed = transposed>
  auto get() noexcept -> typename std::enable_if<!_transposed, T>::type&
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return expr[I];
  }

  /// @brief Returns reference to Ith scalar expression
  /// (specialization for transposed block expression)
  template<std::size_t I, bool _transposed = transposed>
  auto get() noexcept -> typename std::enable_if<_transposed, T>::type&
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return expr[I < _rows * _cols - 1 ? (I * _cols) % (_rows * _cols - 1) : I];
  }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& get() const { return expr; }

  /// @brief Returns reference to raw expressions
  expr_type& get() { return expr; }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& operator->() const { return expr; }

  /// @brief Returns reference to raw expression
  expr_type& operator->() { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const
  {
    os << "Block matrix[" << _rows << "x" << _cols << "]<" << Tag << ">: ((";
    for (std::size_t row = 0; row < _rows; row++)
      for (std::size_t col = 0; col < _cols; col++)
        os
          << expr[transposed ? (col * _rows + row) : (row * _cols + col)].size()
          << (col + 1 < _cols ? "," : row + 1 < _rows ? "),(" : "))");
  }
};

/// @brief Prints a short description of the block matrix
template<std::size_t Tag,
         typename T,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed>
std::ostream&
operator<<(std::ostream& os,
           const fdbbBlockMatrix<Tag, T, _rows, _cols, transposed>& obj)
{
  obj.print(os);
  return os;
}

/// @brief Returns reference to the Ith element of the block matrix
template<std::size_t I,
         std::size_t Tag,
         class T,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed>
constexpr T&
get(fdbbBlockMatrix<Tag, T, _rows, _cols, transposed>& obj) noexcept
{
  static_assert(I < _rows * _cols, "Index exceeds bounds");
  return obj.template get<I>();
}

/// @brief Returns the Ith element of the block matrix
template<std::size_t I,
         std::size_t Tag,
         class T,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed>
constexpr T&&
get(fdbbBlockMatrix<Tag, T, _rows, _cols, transposed>&& obj) noexcept
{
  static_assert(I < _rows * _cols, "Index exceeds bounds");
  return std::move(obj.template get<I>());
}

/// @brief Returns constant reference to the Ith element of the block matrix
template<std::size_t I,
         std::size_t Tag,
         class T,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed>
constexpr const T&
get(const fdbbBlockMatrix<Tag, T, _rows, _cols, transposed>& obj) noexcept
{
  static_assert(I < _rows * _cols, "Index exceeds bounds");
  return obj.template get<I>();
}

/** @brief
 *  A view on a fixed-size block matrix
 *
 *  This is a view on a fixed-size block matrix. It supports exactly
 *  the same functionality but keeps only references to external data
 *  arrays.
 *
 *  @note
 *  Block matrix views are stored in row-wise order in a \ref
 *  std::array. The compile-time flag \a transposed only changes the
 *  behavior of the \ref get member functions.
 */
template<std::size_t Tag,
         typename T,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed>
struct fdbbBlockMatrixView : public fdbbBlockMatrixBase
{
private:
  /// @brief Self type
  using self_type = fdbbBlockMatrixView<Tag, T, _rows, _cols, transposed>;

  /// @brief Expression type
  using expr_type = std::array<T*, _rows * _cols>;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

  /// @brief Number of rows
  static constexpr std::size_t rows = _rows;

  /// @brief Number of columns
  static constexpr std::size_t cols = _cols;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  fdbbBlockMatrixView() = delete;

  /// @brief Constructor (copy from parameter pack)
  template<
    typename... Ts,
    typename = typename std::enable_if<
      !std::is_base_of<fdbbBlockBase,
                       typename fdbb::remove_all<typename std::tuple_element<
                         0,
                         std::template tuple<Ts...>>::type>::type>::value &&
      _cols * _rows == sizeof...(Ts)>::type>
  explicit fdbbBlockMatrixView(Ts&&... args)
    : expr{ &args... }
  {
  }

  /// @brief Constructor (copy from parameter pack)
  template<
    typename... Ts,
    typename = typename std::enable_if<
      !std::is_base_of<fdbbBlockBase,
                       typename fdbb::remove_all<typename std::tuple_element<
                         0,
                         std::template tuple<Ts...>>::type>::type>::value &&
      _cols * _rows == sizeof...(Ts)>::type>
  explicit fdbbBlockMatrixView(Ts*... args)
    : expr{ args... }
  {
  }

  /// @brief Constructor (copy from block matrix)
  template<std::size_t TagOther>
  explicit fdbbBlockMatrixView(
    fdbbBlockMatrix<TagOther, T, _rows, _cols, transposed>& other)
  {
    for (std::size_t idx = 0; idx < _rows * _cols; idx++)
      expr[idx] = &other(idx);
  }

  /// @brief Constructor (copy and transpose from block matrix)
  template<std::size_t TagOther>
  explicit fdbbBlockMatrixView(
    fdbbBlockMatrix<TagOther, T, _cols, _rows, !transposed>& other)
  {
    for (std::size_t idx = 0; idx < _rows * _cols; idx++)
      expr[idx] = &other(idx);
  }

  /// @brief Constructor (copy from block matrix view)
  template<std::size_t TagOther>
  explicit fdbbBlockMatrixView(
    const fdbbBlockMatrixView<TagOther, T, _rows, _cols, transposed>& other)
    : expr{ other.get() }
  {
  }

  /// @brief Constructor (copy and transpose from block matrix view)
  template<std::size_t TagOther>
  explicit fdbbBlockMatrixView(
    const fdbbBlockMatrixView<TagOther, T, _cols, _rows, !transposed>& other)
    : expr{ other.get() }
  {
  }

  /// @brief Constructor (move from block matrix view)
  template<std::size_t TagOther>
  explicit fdbbBlockMatrixView(
    const fdbbBlockMatrixView<TagOther, T, _rows, _cols, transposed>&& other)
    : expr{ std::move(other.get()) }
  {
  }

  /// @brief Constructor (move and transpose from block matrix view)
  template<std::size_t TagOther>
  explicit fdbbBlockMatrixView(
    const fdbbBlockMatrixView<TagOther, T, _cols, _rows, !transposed>&& other)
    : expr{ std::move(other.get()) }
  {
  }

  /// @brief Returns the transposed of the block matrix
  fdbbBlockMatrixView<utils::hash<std::size_t>(tag, 'T'),
                      T,
                      _cols,
                      _rows,
                      !transposed> constexpr const transpose() const
  {
    return fdbbBlockMatrixView<utils::hash<std::size_t>(tag, 'T'),
                               T,
                               _cols,
                               _rows,
                               !transposed>(*this);
  }

  /// @brief Returns the transposed of the block matrix
  fdbbBlockMatrixView<utils::hash<std::size_t>(tag, 'T'),
                      T,
                      _cols,
                      _rows,
                      !transposed>
  transpose()
  {
    return fdbbBlockMatrixView<utils::hash<std::size_t>(tag, 'T'),
                               T,
                               _cols,
                               _rows,
                               !transposed>(*this);
  }

  /// @brief Returns constant reference to scalar matrix from position
  /// \a idx (row-wise ordering)
  constexpr const T& operator()(const std::size_t& idx) const
  {
    return *expr[idx];
  }

  /// @brief Returns reference to scalar matrix from position \a idx
  /// (row-wise ordering)
  T& operator()(const std::size_t& idx) { return *expr[idx]; }

  /// @brief Returns constant reference to Ith scalar matrix
  /// (specialization for non-transposed block matrix)
  template<std::size_t I, bool _transposed = transposed>
  constexpr auto get() const noexcept -> const
    typename std::enable_if<!_transposed, T>::type&
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return *expr[I];
  }

  /// @brief Returns constant reference to Ith scalar matrix
  /// (specialization for transposed block matrix)
  template<std::size_t I, bool _transposed = transposed>
  constexpr auto get() const noexcept -> const
    typename std::enable_if<_transposed, T>::type&
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return *expr[I < _rows * _cols - 1 ? (I * _cols) % (_rows * _cols - 1) : I];
  }

  /// @brief Returns reference to Ith scalar matrix
  /// (specialization for non-transposed block matrix)
  template<std::size_t I, bool _transposed = transposed>
  auto get() noexcept -> typename std::enable_if<!_transposed, T>::type&
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return *expr[I];
  }

  /// @brief Returns reference to Ith scalar expression
  /// (specialization for transposed block expression)
  template<std::size_t I, bool _transposed = transposed>
  auto get() noexcept -> typename std::enable_if<_transposed, T>::type&
  {
    static_assert(I < _rows * _cols, "Index exceeds bounds");
    return *expr[I < _rows * _cols - 1 ? (I * _cols) % (_rows * _cols - 1) : I];
  }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& get() const { return expr; }

  /// @brief Returns reference to raw expressions
  expr_type& get() { return expr; }

  /// @brief Returns constant reference to raw expressions
  constexpr const expr_type& operator->() const { return expr; }

  /// @brief Returns reference to raw expression
  expr_type& operator->() { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const
  {
    os << "Block matrix view[" << _rows << "x" << _cols << "]<" << Tag
       << ">: ((";
    for (std::size_t row = 0; row < _rows; row++)
      for (std::size_t col = 0; col < _cols; col++)
        os << expr[transposed ? (col * _rows + row) : (row * _cols + col)]
                ->size()
           << (col + 1 < _cols ? "," : row + 1 < _rows ? "),(" : "))");
  }
};

/// @brief Prints a short description of the block matrix
template<std::size_t Tag,
         typename T,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed>
std::ostream&
operator<<(std::ostream& os,
           const fdbbBlockMatrixView<Tag, T, _rows, _cols, transposed>& obj)
{
  obj.print(os);
  return os;
}

/// @brief Returns reference to the Ith element of the block matrix view
template<std::size_t I,
         std::size_t Tag,
         class T,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed>
constexpr T&
get(fdbbBlockMatrixView<Tag, T, _rows, _cols, transposed>& obj) noexcept
{
  static_assert(I < _rows * _cols, "Index exceeds bounds");
  return obj.template get<I>();
}

/// @brief Returns the Ith element of the block matrix view
template<std::size_t I,
         std::size_t Tag,
         class T,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed>
constexpr T&&
get(fdbbBlockMatrixView<Tag, T, _rows, _cols, transposed>&& obj) noexcept
{
  static_assert(I < _rows * _cols, "Index exceeds bounds");
  return std::move(obj.template get<I>());
}

/// @brief Returns constant reference to the Ith element of the block matrix
/// view
template<std::size_t I,
         std::size_t Tag,
         class T,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed>
constexpr const T&
get(const fdbbBlockMatrixView<Tag, T, _rows, _cols, transposed>& obj) noexcept
{
  static_assert(I < _rows * _cols, "Index exceeds bounds");
  return obj.template get<I>();
}

/// @brief Binary operator+ between two block expressions
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr2>::type>::value &&
           Expr1::rows == Expr2::rows &&
           Expr1::cols == Expr2::cols>::type>
auto
operator+(Expr1& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '+',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::add())))
{
  return make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '+',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::add()));
}

/// @brief Binary operator+ between two block expressions
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr2>::type>::value &&
           Expr1::rows == Expr2::rows &&
           Expr1::cols == Expr2::cols>::type>
auto
operator+(Expr1&& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '+',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::add())))
{
  return make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '+',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::add()));
}

/// @brief Binary operator+ between two block expressions
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr2>::type>::value &&
           Expr1::rows == Expr2::rows &&
           Expr1::cols == Expr2::cols>::type>
auto
operator+(Expr1& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '+',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::add())))
{
  return make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '+',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::add()));
}

/// @brief Binary operator+ between two block expressions
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr2>::type>::value &&
           Expr1::rows == Expr2::rows &&
           Expr1::cols == Expr2::cols>::type>
auto
operator+(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '+',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::add())))
{
  return make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '+',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::add()));
}

/// @brief Binary operator+ between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto
operator+(Expr1& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6903560, '+', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::add())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6903560, '+', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::add()));
}

/// @brief Binary operator+ between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto
operator+(Expr1&& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6903560, '+', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::add())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6903560, '+', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::add()));
}

/// @brief Binary operator+ between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto
operator+(Expr1& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6903560, '+', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::add())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6903560, '+', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::add()));
}

/// @brief Binary operator+ between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto
operator+(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6903560, '+', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::add())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6903560, '+', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::add()));
}

/// @brief Binary operator+ between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto
operator+(Expr1& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '+', 7381259),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::add())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '+', 7381259),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::add()));
}

/// @brief Binary operator+ between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto
operator+(Expr1&& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '+', 7381259),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::add())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '+', 7381259),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::add()));
}

/// @brief Binary operator+ between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto
operator+(Expr1& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '+', 7381259),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::add())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '+', 7381259),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::add()));
}

/// @brief Binary operator+ between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto
operator+(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '+', 7381259),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::add())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '+', 7381259),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::add()));
}

/// @brief Binary operator- between two block expressions
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr2>::type>::value &&
           Expr1::rows == Expr2::rows &&
           Expr1::cols == Expr2::cols>::type>
auto
operator-(Expr1& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '-',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::sub())))
{
  return make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '-',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::sub()));
}

/// @brief Binary operator- between two block expressions
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr2>::type>::value &&
           Expr1::rows == Expr2::rows &&
           Expr1::cols == Expr2::cols>::type>
auto
operator-(Expr1&& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '-',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::sub())))
{
  return make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '-',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::sub()));
}

/// @brief Binary operator- between two block expressions
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr2>::type>::value &&
           Expr1::rows == Expr2::rows &&
           Expr1::cols == Expr2::cols>::type>
auto
operator-(Expr1& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '-',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::sub())))
{
  return make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '-',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::sub()));
}

/// @brief Binary operator- between two block expressions
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr2>::type>::value &&
           Expr1::rows == Expr2::rows &&
           Expr1::cols == Expr2::cols>::type>
auto
operator-(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '-',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::sub())))
{
  return make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '-',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return(expr1, expr2, functor::sub()));
}

/// @brief Binary operator- between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto
operator-(Expr1& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6913510, '-', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::sub())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6913510, '-', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::sub()));
}

/// @brief Binary operator- between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto
operator-(Expr1&& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6913510, '-', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::sub())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6913510, '-', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::sub()));
}

/// @brief Binary operator- between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto
operator-(Expr1& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6913510, '-', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::sub())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6913510, '-', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::sub()));
}

/// @brief Binary operator- between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto
operator-(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6913510, '-', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::sub())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6913510, '-', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::sub()));
}

/// @brief Binary operator- between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto
operator-(Expr1& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '-', 7321253),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::sub())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '-', 7321253),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::sub()));
}

/// @brief Binary operator- between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto
operator-(Expr1&& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '-', 7321253),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::sub())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '-', 7321253),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::sub()));
}

/// @brief Binary operator- between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto
operator-(Expr1& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '-', 7321253),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::sub())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '-', 7321253),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::sub()));
}

/// @brief Binary operator- between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto
operator-(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '-', 7321253),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::sub())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '-', 7321253),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::sub()));
}

/// @brief Binary operator* between two block expressions
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr2>::type>::value &&
           Expr1::cols == Expr2::rows>::type>
auto operator*(Expr1& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '*',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr2::cols>(utils::for_each_return<0, 1, Expr1::rows, 0, 0, 0, 0, 0>(
    utils::make_tuple(utils::make_index_sequence<Expr1::rows * Expr2::cols>{}),
    utils::make_tuple(utils::make_index_sequence<Expr1::rows + 1>{}),
    std::make_tuple(expr1),
    std::make_tuple(expr2),
    functor::matmul())))
{
  return make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '*',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr2::cols>(utils::for_each_return<0, 1, Expr1::rows, 0, 0, 0, 0, 0>(
    utils::make_tuple(utils::make_index_sequence<Expr1::rows * Expr2::cols>{}),
    utils::make_tuple(utils::make_index_sequence<Expr1::rows + 1>{}),
    std::make_tuple(expr1),
    std::make_tuple(expr2),
    functor::matmul()));
}

/// @brief Binary operator* between two block expressions
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr2>::type>::value &&
           Expr1::cols == Expr2::rows>::type>
auto operator*(Expr1&& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '*',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr2::cols>(utils::for_each_return<0, 1, Expr1::rows, 0, 0, 0, 0, 0>(
    utils::make_tuple(utils::make_index_sequence<Expr1::rows * Expr2::cols>{}),
    utils::make_tuple(utils::make_index_sequence<Expr1::rows + 1>{}),
    std::make_tuple(expr1),
    std::make_tuple(expr2),
    functor::matmul())))
{
  return make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '*',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr2::cols>(utils::for_each_return<0, 1, Expr1::rows, 0, 0, 0, 0, 0>(
    utils::make_tuple(utils::make_index_sequence<Expr1::rows * Expr2::cols>{}),
    utils::make_tuple(utils::make_index_sequence<Expr1::rows + 1>{}),
    std::make_tuple(expr1),
    std::make_tuple(expr2),
    functor::matmul()));
}

/// @brief Binary operator* between two block expressions
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr2>::type>::value &&
           Expr1::cols == Expr2::rows>::type>
auto operator*(Expr1& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '*',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr2::cols>(utils::for_each_return<0, 1, Expr1::rows, 0, 0, 0, 0, 0>(
    utils::make_tuple(utils::make_index_sequence<Expr1::rows * Expr2::cols>{}),
    utils::make_tuple(utils::make_index_sequence<Expr1::rows + 1>{}),
    std::make_tuple(expr1),
    std::make_tuple(expr2),
    functor::matmul())))
{
  return make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '*',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr2::cols>(utils::for_each_return<0, 1, Expr1::rows, 0, 0, 0, 0, 0>(
    utils::make_tuple(utils::make_index_sequence<Expr1::rows * Expr2::cols>{}),
    utils::make_tuple(utils::make_index_sequence<Expr1::rows + 1>{}),
    std::make_tuple(expr1),
    std::make_tuple(expr2),
    functor::matmul()));
}

/// @brief Binary operator* between two block expressions
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr2>::type>::value &&
           Expr1::cols == Expr2::rows>::type>
auto operator*(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '*',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr2::cols>(utils::for_each_return<0, 1, Expr1::rows, 0, 0, 0, 0, 0>(
    utils::make_tuple(utils::make_index_sequence<Expr1::rows * Expr2::cols>{}),
    utils::make_tuple(utils::make_index_sequence<Expr1::rows + 1>{}),
    std::make_tuple(expr1),
    std::make_tuple(expr2),
    functor::matmul())))
{
  return make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                             '*',
                             fdbb::remove_all<Expr2>::type::tag),
    Expr1::rows,
    Expr2::cols>(utils::for_each_return<0, 1, Expr1::rows, 0, 0, 0, 0, 0>(
    utils::make_tuple(utils::make_index_sequence<Expr1::rows * Expr2::cols>{}),
    utils::make_tuple(utils::make_index_sequence<Expr1::rows + 1>{}),
    std::make_tuple(expr1),
    std::make_tuple(expr2),
    functor::matmul()));
}

/// @brief Binary operator* between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto operator*(Expr1& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6553510, '*', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::mul())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6553510, '*', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::mul()));
}

/// @brief Binary operator* between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto operator*(Expr1&& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6553510, '*', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::mul())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6553510, '*', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::mul()));
}

/// @brief Binary operator* between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto operator*(Expr1& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6553510, '*', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::mul())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6553510, '*', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::mul()));
}

/// @brief Binary operator* between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto operator*(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6553510, '*', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::mul())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6553510, '*', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::mul()));
}

/// @brief Binary operator* between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto operator*(Expr1& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '*', 7328423),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::mul())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '*', 7328423),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::mul()));
}

/// @brief Binary operator* between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto operator*(Expr1&& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '*', 7328423),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::mul())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '*', 7328423),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::mul()));
}

/// @brief Binary operator* between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto operator*(Expr1& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '*', 7328423),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::mul())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '*', 7328423),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::mul()));
}

/// @brief Binary operator* between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto operator*(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '*', 7328423),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::mul())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '*', 7328423),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::mul()));
}

/// @brief Binary operator/ between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto
operator/(Expr1& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6498507, '/', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::div())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6498507, '/', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::div()));
}

/// @brief Binary operator/ between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto
operator/(Expr1&& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6498507, '/', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::div())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6498507, '/', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::div()));
}

/// @brief Binary operator/ between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto
operator/(Expr1& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6498507, '/', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::div())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6498507, '/', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::div()));
}

/// @brief Binary operator/ between an arithmetic type and a block expression
template<
  typename Expr1,
  typename Expr2,
  typename = typename std::enable_if<
    std::is_arithmetic<Expr1>::value &&
    std::is_base_of<fdbbBlockBase,
                    typename fdbb::remove_all<Expr2>::type>::value>::type>
auto
operator/(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(6498507, '/', fdbb::remove_all<Expr2>::type::tag),
    Expr2::rows,
    Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(expr2,
                                                    std::make_tuple(expr1),
                                                    functor::div())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              6498507, '/', fdbb::remove_all<Expr2>::type::tag),
                            Expr2::rows,
                            Expr2::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr2, std::make_tuple(expr1), functor::div()));
}

/// @brief Binary operator/ between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto
operator/(Expr1& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '/', 5116317),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::div())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '/', 5116317),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::div()));
}

/// @brief Binary operator/ between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto
operator/(Expr1&& expr1, Expr2& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '/', 5116317),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::div())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '/', 5116317),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::div()));
}

/// @brief Binary operator/ between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto
operator/(Expr1& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '/', 5116317),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::div())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '/', 5116317),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::div()));
}

/// @brief Binary operator/ between a block expression and an arithmetic type
template<typename Expr1,
         typename Expr2,
         typename = typename std::enable_if<
           std::is_base_of<fdbbBlockBase,
                           typename fdbb::remove_all<Expr1>::type>::value &&
           std::is_arithmetic<Expr2>::value>::type>
auto
operator/(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  make_fdbbBlockExpr<
    utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '/', 5116317),
    Expr1::rows,
    Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(expr1,
                                                    std::make_tuple(expr2),
                                                    functor::div())))
{
  return make_fdbbBlockExpr<utils::hash<std::size_t>(
                              fdbb::remove_all<Expr1>::type::tag, '/', 5116317),
                            Expr1::rows,
                            Expr1::cols>(utils::for_each_return<0, 1, 0, 0>(
    expr1, std::make_tuple(expr2), functor::div()));
}

} // namespace fdbb

#endif // FDBB_BLOCK_EXPRESSIONS_HPP
