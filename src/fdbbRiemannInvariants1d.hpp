/** @file fdbbRiemannInvariants1d.hpp
 *
 *  @brief 1D implementation for Riemann invariants
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_RIEMANN_INVARIANTS_1D_H
#define FDBB_RIEMANN_INVARIANTS_1D_H

#include <type_traits>

#include "fdbbConfig.hpp"
#include "fdbbEnums.hpp"
#include "fdbbTraits.hpp"
#include "fdbbTypes.hpp"

namespace fdbb {

/** @brief
 *
 *  Specialization of the fdbbVariables structure for Riemann
 *  invariants in 1d space dimensions.
 *  This implementation is valid only for ideal gas!
 *
 *  \f[
 *      W = \begin{bmatrix}
 *            w_1\\
 *            w_2\\
 *            w_3
 *          \end{bmatrix}
 *        = \begin{bmatrix}
 *            v_n-2c/(\gamma-1)\\
 *            c_v log(p/\rho^\gamma)\\
 *            v_n+2c/(\gamma-1)
 *          \end{bmatrix}
 *  \f]
 *  where
 *  \f$ v_n       \f$ is the velocity component normal to the boundary,
 *  \f$ c         \f$ is the speed of sound,
 *  \f$ \gamma    \f$ is the adiabatic index,
 *  \f$ c_v       \f$ is the specific heat at constant volume,
 *  \f$ p         \f$ is the absolute pressure, and
 *  \f$ \rho      \f$ is the volumetric mass density.
 */
template<typename EOS, typename Traits>
struct fdbbVariables<EOS, 1, EnumForm::Riemann_invariants, Traits>
{
  /// @brief Equation of state
  using eos = EOS;

  /// @brief Type traits
  using traits = Traits;

  /// @brief Dimension
  static constexpr index_t dim = 1;

  /// @brief Return output stream
  static std::ostream& print(std::ostream& os)
  {
    os << "Riemann invariants in 1d, ";
    eos::print(os);
    return os;
  }

  /** @brief
   *  First Riemann invariant \f$ w_1 \f$ in 1d
   *
   *  @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_1(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      traits::template getVariable<EnumVar::w_1>(std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::w_1>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Second Riemann invariant \f$ w_2 \f$ in 1d
   *
   *  @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_2(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      traits::template getVariable<EnumVar::w_2>(std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::w_2>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Third Riemann invariant \f$ w_3 \f$ in 1d
   *
   *  @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_3(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      traits::template getVariable<EnumVar::w_3>(std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::w_3>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Speed of sound variable \f$ c \f$ for Riemann
   *  invariants in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr c(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(
      fdbb::elem_div(eos::gamma -
                       CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)),
                     CONSTANT(4.0, w_1(std::forward<Vars>(vars)...))),
      w_3(std::forward<Vars>(vars)...) - w_1(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(
      fdbb::elem_div(eos::gamma -
                       CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)),
                     CONSTANT(4.0, w_1(std::forward<Vars>(vars)...))),
      w_3(std::forward<Vars>(vars)...) - w_1(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Volumetric mass density variable \f$ rho \f$ for Riemann
   *  invariants in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rho(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_pow(
      fdbb::elem_mul(
        fdbb::elem_div(fdbb::elem_mul(c(std::forward<Vars>(vars)...),
                                      c(std::forward<Vars>(vars)...)),
                       eos::gamma),
        fdbb::elem_exp(fdbb::elem_div(-w_2(std::forward<Vars>(vars)...),
                                      eos::cv))),
      fdbb::elem_div(CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)),
                     eos::gamma -
                       CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)))))
#endif
  {
    return fdbb::elem_pow(
      fdbb::elem_mul(
        fdbb::elem_div(fdbb::elem_mul(c(std::forward<Vars>(vars)...),
                                      c(std::forward<Vars>(vars)...)),
                       eos::gamma),
        fdbb::elem_exp(
          fdbb::elem_div(-w_2(std::forward<Vars>(vars)...), eos::cv))),
      fdbb::elem_div(CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)),
                     eos::gamma -
                       CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))));
  }

  /** @brief
   *  Absolute pressure variable \f$ p \f$ for Riemann
   *  invariants in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr p(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(
      fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                     fdbb::elem_mul(c(std::forward<Vars>(vars)...),
                                    c(std::forward<Vars>(vars)...))),
      eos::gamma))
#endif
  {
    return fdbb::elem_div(
      fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                     fdbb::elem_mul(c(std::forward<Vars>(vars)...),
                                    c(std::forward<Vars>(vars)...))),
      eos::gamma);
  }

  /** @brief
   *  Velocity variable \f$ v_0 \f$ for Riemann
   *  invariants in 1d
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *
   *  @param[in] n_x  x component of vector n
   *
   *  @ingroup VariablesPrimary
   */
  template<index_t idim, typename T, typename... Vars>
  static FDBB_INLINE auto constexpr v(T&& n_x, Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      idim == 0,
      decltype(fdbb::elem_mul(
        fdbb::elem_div(w_1(std::forward<Vars>(vars)...) +
                         w_3(std::forward<Vars>(vars)...),
                       CONSTANT(2.0, w_1(std::forward<Vars>(vars)...))),
        fdbb::elem_div(
          std::forward<T>(n_x),
          fdbb::elem_sqrt(fdbb::elem_mul(std::forward<T>(n_x),
                                         std::forward<T>(n_x))))))>::type
#endif
  {
    return fdbb::elem_mul(
      fdbb::elem_div(w_1(std::forward<Vars>(vars)...) +
                       w_3(std::forward<Vars>(vars)...),
                     CONSTANT(2.0, w_1(std::forward<Vars>(vars)...))),
      fdbb::elem_div(std::forward<T>(n_x),
                     fdbb::elem_sqrt(fdbb::elem_mul(std::forward<T>(n_x),
                                                    std::forward<T>(n_x)))));
  }

  /** @brief
   *  Momentum variable \f$ \rho v_0 \f$ for Riemann
   *  invariants in 1d
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *
   *  @param[in] n_x  x component of vector n
   *
   *  @ingroup VariablesSecondary
   */
  template<index_t idim, typename T, typename... Vars>
  static FDBB_INLINE auto constexpr rhov(T&& n_x, Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(v<idim>(std::forward<T>(n_x),
                                       std::forward<Vars>(vars)...),
                               rho(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(
      v<idim>(std::forward<T>(n_x), std::forward<Vars>(vars)...),
      rho(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Total energy per unit volume variable \f$ \rho E \f$
   *  for Riemann invariants in 1d
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *
   *  @param[in] n_x  x component of vector n
   *
   *  @ingroup VariablesSecondary
   */
  template<typename T, typename... Vars>
  static FDBB_INLINE auto constexpr rhoE(T&& n_x, Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      fdbb::elem_div(p(std::forward<Vars>(vars)...),
                     eos::gamma -
                       CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))) +
      fdbb::elem_mul(
        fdbb::elem_div(rho(std::forward<Vars>(vars)...),
                       CONSTANT(2.0, w_1(std::forward<Vars>(vars)...))),
        fdbb::elem_mul(v<0>(std::forward<T>(n_x), std::forward<Vars>(vars)...),
                       v<0>(std::forward<T>(n_x),
                            std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::elem_div(p(std::forward<Vars>(vars)...),
                          eos::gamma -
                            CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))) +
           fdbb::elem_mul(
             fdbb::elem_div(rho(std::forward<Vars>(vars)...),
                            CONSTANT(2.0, w_1(std::forward<Vars>(vars)...))),
             fdbb::elem_mul(
               v<0>(std::forward<T>(n_x), std::forward<Vars>(vars)...),
               v<0>(std::forward<T>(n_x), std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  State vector of conservative variables in 1d
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *
   *  @param[in] n_x  x component of vector n
   *
   *  @ingroup VariablesSecondary
   */
  template<std::size_t Tag, typename T, typename... Vars>
  static FDBB_INLINE auto constexpr conservative(T&& n_x,
                                                 Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(rhov<0>(std::forward<T>(n_x), std::forward<Vars>(vars)...)),
      std::move(rhoE(std::forward<T>(n_x), std::forward<Vars>(vars)...))))
#endif
  {
    return make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(rhov<0>(std::forward<T>(n_x), std::forward<Vars>(vars)...)),
      std::move(rhoE(std::forward<T>(n_x), std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  State vector of primitive variables in 1d
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *
   *  @param[in] n_x  x component of vector n
   *
   *  @ingroup VariablesSecondary
   */
  template<std::size_t Tag, typename T, typename... Vars>
  static FDBB_INLINE auto constexpr primitive(T&& n_x, Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(v<0>(std::forward<T>(n_x), std::forward<Vars>(vars)...)),
      std::move(p(std::forward<Vars>(vars)...))))
#endif
  {
    return make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(v<0>(std::forward<T>(n_x), std::forward<Vars>(vars)...)),
      std::move(p(std::forward<Vars>(vars)...)));
  }
};

} // namespace fdbb

#endif // FDBB_RIEMANN_INVARIANTS_1D_H
