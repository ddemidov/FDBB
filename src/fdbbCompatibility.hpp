/** @file fdbbCompatibility.hpp
 *
 *  @brief Compatibility layer for Fluid Dynamics Building Blocks
 *
 *  This file implements a compatibility layer between the
 *  non-standardized expression template libraries and the naming
 *  convention using in FDBB. It also implements extra functionality
 *  not available in (some of) the expression template libraries.
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_COMPATIBILITY_HPP
#define FDBB_COMPATIBILITY_HPP

#include <cmath>
#include <iomanip>
#include <type_traits>
#include <utility>

#include "fdbbBlockExpressionsForward.hpp"
#include "fdbbCacheForward.hpp"

#include "fdbbConfig.hpp"
#include "fdbbUtils.hpp"

namespace fdbb {

/** @brief
 *  Extracts the unqualified base type by removing any const, volatile and
 *  reference
 *
 *  @tparam The type from which to extract the unqualified base type
 */
template<typename T>
using remove_all =
  std::remove_cv<typename std::remove_pointer<typename std::remove_reference<
    typename std::remove_all_extents<T>::type>::type>::type>;

/** @brief
 *  If T is type of the expression template library Expr, provides the
 *  member constant value equal to true. Otherwise value is false.
 *
 *  @tparam T    The type to check against the given expression type
 *  @tparam Expr The expression type
 */
template<typename T, EnumETL Expr, typename = void>
struct is_type_of : std::false_type
{
};

/** @brief
 *  If T is type of the expression template library Expr, then
 *  fdbb::enable_if_type_of has a public member typedef type, equal to
 *  T; otherwise is no member typedef.
 *
 *  @tparam T    The type to check against the given expression type
 *  @tparam Expr The expression type
 */
template<typename T, EnumETL Expr, class C = void>
using enable_if_type_of = std::enable_if<is_type_of<T, Expr>::value, C>;

/** @brief
 *  If T is type of the expression template library Expr and condition
 *  cond is true, then fdbb::enable_if_type_of has a public member
 *  typedef type, equal to T; otherwise is no member typedef.
 *
 *  @tparam T    The type to check against the given expression type
 *  @tparam Expr The expression type
 *  @tparam Cond The condition consider additionally
 */
template<typename T, EnumETL Expr, bool Cond, class C = void>
using enable_if_type_of_and_cond =
  std::enable_if<is_type_of<T, Expr>::value && Cond, C>;

/** @brief
 *  If T is type of the expression template library Expr or condition
 *  cond is true, then fdbb::enable_if_type_of has a public member
 *  typedef type, equal to T; otherwise is no member typedef.
 *
 *  @tparam T    The type to check against the given expression type
 *  @tparam Expr The expression type
 *  @tparam Cond The condition consider additionally
 */
template<typename T, EnumETL Expr, bool Cond, class C = void>
using enable_if_type_of_or_cond =
  std::enable_if<is_type_of<T, Expr>::value || Cond, C>;

/** @brief
 *  If T1 and T2 are the same types of the expression template library
 *  Expr, then fdbb::enable_if_type_of has a public member typedef type,
 *  equal to T1; otherwise is no member typedef.
 *
 *  @tparam T1 The first type to check against the given expression type
 *  @tparam T2 The second type to check against the given expression type
 *  @tparam Expr The expression type
 */
template<typename T1, typename T2, EnumETL Expr, class C = void>
using enable_if_all_type_of =
  std::enable_if<is_type_of<T1, Expr>::value && is_type_of<T2, Expr>::value, C>;

/** @brief
 *  If T1 and T2 are the same types of the expression template library
 *  Expr and condition cond is true, then fdbb::enable_if_type_of has a
 *  public member typedef type, equal to T1; otherwise is no member
 *  typedef.
 *
 *  @tparam T1 The first type to check against the given expression type
 *  @tparam T2 The second type to check against the given expression type
 *  @tparam Expr The expression type
 *  @tparam Cond The condition consider additionally
 */
template<typename T1, typename T2, EnumETL Expr, bool Cond, class C = void>
using enable_if_all_type_of_and_cond =
  std::enable_if<(is_type_of<T1, Expr>::value && is_type_of<T2, Expr>::value) &&
                   Cond,
                 C>;

/** @brief
 *  If T1 and T2 are the same types of the expression template library
 *  Expr or condition cond is true, then fdbb::enable_if_type_of has a
 *  public member typedef type, equal to T1; otherwise is no member
 *  typedef.
 *
 *  @tparam T1 The first type to check against the given expression type
 *  @tparam T2 The second type to check against the given expression type
 *  @tparam Expr The expression type
 *  @tparam Cond The condition consider additionally
 */
template<typename T1, typename T2, EnumETL Expr, bool Cond, class C = void>
using enable_if_all_type_of_or_cond =
  std::enable_if<(is_type_of<T1, Expr>::value && is_type_of<T2, Expr>::value) ||
                   Cond,
                 C>;

/** @brief
 *  If T1 or T2 are the same types of the expression template library
 *  Expr, then fdbb::enable_if_type_of has a public member typedef type,
 *  equal to T1 or T2; otherwise is no member typedef.
 *
 *  @tparam T1 The first type to check against the given expression type
 *  @tparam T2 The second type to check against the given expression type
 *  @tparam Expr The expression type
 */
template<typename T1, typename T2, EnumETL Expr, class C = void>
using enable_if_any_type_of =
  std::enable_if<(is_type_of<T1, Expr>::value || is_type_of<T2, Expr>::value),
                 C>;

/** @brief
 *  If T1 or T2 are the same types of the expression template library
 *  Expr and condition cond is true, then fdbb::enable_if_type_of has a
 *  public member typedef type, equal to T1 or T2; otherwise is no
 *  member typedef.
 *
 *  @tparam T1 The first type to check against the given expression type
 *  @tparam T2 The second type to check against the given expression type
 *  @tparam Expr The expression type
 *  @tparam Cond The condition consider additionally
 */
template<typename T1, typename T2, EnumETL Expr, bool Cond, class C = void>
using enable_if_any_type_of_and_cond =
  std::enable_if<(is_type_of<T1, Expr>::value || is_type_of<T2, Expr>::value) &&
                   Cond,
                 C>;

/** @brief
 *  If T1 or T2 are the same types of the expression template library
 *  Expr or condition cond is true, then fdbb::enable_if_type_of has a
 *  public member typedef type, equal to T1 or T2; otherwise is no
 *  member typedef.
 *
 *  @tparam T1 The first type to check against the given expression type
 *  @tparam T2 The second type to check against the given expression type
 *  @tparam Expr The expression type
 *  @tparam Cond The condition consider additionally
 */
template<typename T1, typename T2, EnumETL Expr, bool Cond, class C = void>
using enable_if_any_type_of_or_cond =
  std::enable_if<(is_type_of<T1, Expr>::value || is_type_of<T2, Expr>::value) ||
                   Cond,
                 C>;

/** @brief
 *  Result type of the expression Expr
 *
 *  @tparam Expr The expression type
 */
template<typename Expr, typename = void>
struct result_type
{
  using type = void;
};

/** @brief
 *  Result type of the expression Expr (arithmetic type)
 *
 *  @tparam Expr The expression type
 */
template<typename Expr>
struct result_type<
  Expr,
  typename std::enable_if<std::is_arithmetic<Expr>::value>::type>
{
  using type = Expr;
};

/** @brief
 *  Scalar value type of the expression Expr
 *
 *  @tparam Expr The expression type
 */
template<typename Expr, typename = void>
struct value_type
{
  using type = void;
};

/** @brief
 *  Scalar value type of the expression Expr (arithmetic type)
 *
 *  @tparam Expr The expression type
 */
template<typename Expr>
struct value_type<
  Expr,
  typename std::enable_if<std::is_arithmetic<Expr>::value>::type>
{
  using type = Expr;
};

/** @namespace fdbb::internal
 *
 *  @brief
 *  The fdbb::internal namespace, containing all internal details of
 *  functions of the FDBB library
 */
namespace detail {

/** @brief
 *  Indicator for (specialized) implementation of
 *  fdbb::detail::make_constant<T,Expr>(const T c, Expr&& expr) function
 *
 *  @tparam T    The constant type
 *  @tparam Expr The expression type
 */
template<typename Expr, typename = void>
struct has_make_constant_impl : public std::false_type
{
};

/** @brief
 *  Forward declaration of generic implementation of
 *  fdbb::detail::make_constant_impl<T,Expr>(const T c, Expr&& expr) function
 *
 *  @param  TC   The typeconstant (stores constant encoded in template
 *               parameter)
 *  @tparam T    The constant type
 *  @tparam Expr The expression type
 */
template<typename TC, typename T, typename Expr>
static FDBB_INLINE auto constexpr make_constant_impl(const T value,
                                                     Expr&& expr) noexcept ->
  typename std::enable_if<!has_make_constant_impl<Expr>::value,
                          typename fdbb::value_type<Expr>::type>::type;

/** @brief
 *  Indicator for (specialized) implementation of
 *  fdbb::detail::make_temp_impl<Tag,Expr>(Expr&& expr) function
 *
 *  @tparam Expr The expression type
 */
template<typename Expr, typename = void>
struct has_make_temp_impl : public std::false_type
{
};

/** @brief
 *  Forward declaration of generic implementation of
 *  fdbb::detail::make_temp_impl<Tag,Expr>(Expr&& expr) function
 *
 *  @tparam Tag  The tag
 *  @tparam Expr The expression type
 */
template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr make_temp_impl(Expr&& expr) noexcept ->
  typename std::enable_if<!has_make_temp_impl<Expr>::value,
                          decltype(std::forward<Expr>(expr))>::type;

/** @brief
 *  Indicator for (specialized) implementation of
 *  fdbb::detail::tag_impl<Tag,Expr>(Expr&& expr) function
 *
 *  @tparam Expr The expression type
 */
template<typename Expr, typename = void>
struct has_tag_impl : public std::false_type
{
};

/** @brief
 *  Forward declaration of generic implementation of
 *  fdbb::detail::tag_impl<Tag,Expr>(Expr&& expr) function
 *
 *  @tparam Tag  The tag
 *  @tparam Expr The expression type
 */
template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr tag_impl(Expr&& expr) noexcept ->
  typename std::enable_if<!has_tag_impl<Expr>::value,
                          decltype(std::forward<Expr>(expr))>::type;

/// Helper macro for generating selectors for specialialized
/// implementations of element-wise binary operations; generic
/// implementation follows below
#define FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  /** @brief                                                                   \
   *  Selector for (specialized) implementation of                             \
   *  fdbb::detail::elem_##OPNAME##<A,B>(A&& a, B&& b) function                \
   *                                                                           \
   *  @tparam A The first expression type                                      \
   *  @tparam B The second expression type                                     \
   */                                                                          \
  template<typename A, typename B, typename = void>                            \
  struct get_elem_##OPNAME##_impl                                              \
    : public std::integral_constant<EnumETL, EnumETL::GENERIC>                 \
  {                                                                            \
  };

FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow)

#undef FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/** @brief
 *  Element-wise multiplication (perfect forwarding to *-operator)
 *
 *  @tparam A       The first expression type
 *  @tparam B       The second expression type
 *  @tparam EnumETL The expression type enumerator
 */
template<typename A, typename B, EnumETL = EnumETL::GENERIC>
struct elem_mul_impl
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
    -> decltype(std::forward<A>(a) * std::forward<B>(b))
  {
    return std::forward<A>(a) * std::forward<B>(b);
  }
};

/** @brief
 *  Element-wise division (perfect forwarding to /-operator)
 *
 *  @tparam A       The first expression type
 *  @tparam B       The second expression type
 *  @tparam EnumETL The expression type enumerator
 */
template<typename A, typename B, EnumETL = EnumETL::GENERIC>
struct elem_div_impl
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
    -> decltype(std::forward<A>(a) / std::forward<B>(b))
  {
    return std::forward<A>(a) / std::forward<B>(b);
  }
};

/** @brief
 *  Element-wise raise to power (perfect forwarding to pow-function)
 *
 *  @tparam A       The first expression type
 *  @tparam B       The second expression type
 *  @tparam EnumETL The expression type enumerator
 */
template<typename A, typename B, EnumETL = EnumETL::GENERIC>
struct elem_pow_impl
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
    -> decltype(pow(std::forward<A>(a), std::forward<B>(b)))
  {
    return pow(std::forward<A>(a), std::forward<B>(b));
  }
};

/// Helper macro for generating selectors for specialized
/// implementations of element-wise unary operations and generic implementation
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief                                                                   \
   *  Selector for (specialized) implementation of                             \
   *  detail::elem_##OPNAME##<A>(A&& a) function                               \
   *                                                                           \
   *  @tparam A       The expression type                                      \
   *  @tparam EnumETL The expression type enumerator                           \
   */                                                                          \
  template<typename A, typename = void>                                        \
  struct get_elem_##OPNAME##_impl                                              \
    : public std::integral_constant<EnumETL, EnumETL::GENERIC>                 \
  {                                                                            \
  };                                                                           \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function (perfect forwarding to OPNAME)              \
   *                                                                           \
   *  @tparam A       The expression type                                      \
   *  @tparam EnumETL The expression type enumerator                           \
   */                                                                          \
  template<typename A, EnumETL = EnumETL::GENERIC>                             \
  struct elem_##OPNAME##_impl                                                  \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept                     \
      -> decltype(OPNAME(std::forward<A>(a)))                                  \
    {                                                                          \
      return OPNAME(std::forward<A>(a));                                       \
    }                                                                          \
  };

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

} // namespace detail
} // namespace fdbb

/// Include backend implementations
#include "backend/armadillo.hpp"
#include "backend/arrayfire.hpp"
#include "backend/blaze.hpp"
#include "backend/eigen.hpp"
#include "backend/itpp.hpp"
#include "backend/mtl4.hpp"
#include "backend/ublas.hpp"
#include "backend/vexcl.hpp"
#include "backend/viennacl.hpp"

/// Include cache-backend implementation:
///
/// Make sure that the cache implementation is included after all
/// other implementations have been included so that calls to methods
/// from the underlying backends get dispatched correctly.
#include "backend/cache.hpp"

/// Include block expression implementation:
///
/// Make sure that the block expression implementation is included
/// after all other implementations have been included so that calls
/// to methods from the underlying backends get dispatched correctly.
#include "backend/blockexpression.hpp"

namespace fdbb {

/** @brief
 *  Creates constant that complies with the value-type of the expression.
 *
 *  @note
 *  If no specialized implementation is available then this
 *  implementation casts the given value to the scalar value-type of
 *  the expression and returns it as const.
 *
 *  @param  TC   The typeconstant (stores constant encoded in template
 parameter)
 *
 *  @tparam T    The constant type
 *  @tparam Expr The expression type
 *  @{
 */
template<typename TC, typename T, typename Expr>
static FDBB_INLINE auto constexpr make_constant(const T value,
                                                Expr&& expr) noexcept ->
  typename std::enable_if<detail::has_make_constant_impl<Expr>::value,
                          decltype(detail::make_constant_impl<TC>(value,
                                                                  expr))>::type
{
  return detail::make_constant_impl<TC>(value, expr);
}

template<typename TC, typename T, typename Expr>
static FDBB_INLINE auto constexpr make_constant(const T value,
                                                Expr&& expr) noexcept ->
  typename std::enable_if<!detail::has_make_constant_impl<Expr>::value,
                          typename fdbb::value_type<Expr>::type>::type
{
  return static_cast<typename fdbb::value_type<Expr>::type>(value);
}
/** @} */

/** @brief
 *  Creates temporary expression that may be reused in a vector
 *  expression. The type of the resulting temporary variable must
 *  be explicitly specified as a template parameter.
 *
 *  @note
 *  This functions requires T!=Expr. If T==Expr the
 *  make_temp<Tag,Expr>(Expr&& expr) function gets activated
 *
 *  @tparam Tag  The tag
 *  @tparam T    The temporal type
 *  @tparam Expr The expression type
 */
template<std::size_t Tag, typename Temp, typename Expr>
static FDBB_INLINE auto constexpr make_temp(Expr&& expr) noexcept ->
  typename std::enable_if<!std::is_same<Temp, Expr>::value,
                          decltype(Temp(std::forward<Expr>(expr)))>::type
{
  return Temp(std::forward<Expr>(expr));
}

/** @brief
 *  Creates temporary expression that may be reused in a vector
 *  expression. The type of the resulting temporary variable is
 *  automatically deduced from the expression.
 *
 *  @note
 *  If no specialized implementation is available then this
 *  implementation performs perfect forwarding of the argument to the
 *  caller and no temporary is created at all.
 *
 *  @tparam Tag  The tag
 *  @tparam Expr The expression type
 *  @{
 */
template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr make_temp(Expr&& expr) noexcept ->
  typename std::enable_if<
    detail::has_make_temp_impl<Expr>::value,
    decltype(detail::make_temp_impl<Tag, Expr>(std::forward<Expr>(expr)))>::type
{
  return detail::make_temp_impl<Tag, Expr>(std::forward<Expr>(expr));
}

template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr make_temp(Expr&& expr) noexcept ->
  typename std::enable_if<!detail::has_make_temp_impl<Expr>::value,
                          decltype(std::forward<Expr>(expr))>::type
{
  return std::forward<Expr>(expr);
}
/** @} */

/** @brief
 *  Tags terminal with a unique (in a single expression) tag.
 *
 *  @note
 *  If no specialized implementation is available then this
 *  implementation performs perfect forarding of the argument to the
 *  caller and no tag is created at all.
 *
 *  @tparam Tag  The tag
 *  @tparam Expr The expression type
 *  @{
 */
template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr tag(Expr&& expr) noexcept ->
  typename std::enable_if<
    detail::has_tag_impl<Expr>::value,
    decltype(detail::tag_impl<Tag, Expr>(std::forward<Expr>(expr)))>::type
{
  return detail::tag_impl<Tag, Expr>(std::forward<Expr>(expr));
}

template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr tag(Expr&& expr) noexcept ->
  typename std::enable_if<!detail::has_tag_impl<Expr>::value,
                          decltype(std::forward<Expr>(expr))>::type
{
  return std::forward<Expr>(expr);
}
/** @} */

/// Helper macro for generating element-wise binary operations
#define FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  /** @brief                                                                   \
   *  Element-wise OPNAME function                                             \
   *                                                                           \
   *  @tparam A The first expression type                                      \
   *  @tparam B The second expression type                                     \
   */                                                                          \
  template<typename A, typename B>                                             \
  auto constexpr elem_##OPNAME(A&& a, B&& b) noexcept->decltype(               \
    detail::elem_##OPNAME##_impl<                                              \
      A,                                                                       \
      B,                                                                       \
      detail::get_elem_##OPNAME##_impl<A, B>::value>::eval(std::forward<A>(a), \
                                                           std::forward<B>(    \
                                                             b)))              \
  {                                                                            \
    return detail::elem_##OPNAME##_impl<                                       \
      A,                                                                       \
      B,                                                                       \
      detail::get_elem_##OPNAME##_impl<A, B>::value>::eval(std::forward<A>(a), \
                                                           std::forward<B>(    \
                                                             b));              \
  }

FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow)

#undef FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/// Helper macro for generating element-wise unary operations
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief                                                                   \
   *  Element-wise OPNAME function                                             \
   *                                                                           \
   *  @tparam A The expression type                                            \
   */                                                                          \
  template<typename A>                                                         \
  auto constexpr elem_##OPNAME(A&& a) noexcept->decltype(                      \
    detail::elem_##OPNAME##_impl<                                              \
      A,                                                                       \
      detail::get_elem_##OPNAME##_impl<A>::value>::eval(std::forward<A>(a)))   \
  {                                                                            \
    return detail::elem_##OPNAME##_impl<                                       \
      A,                                                                       \
      detail::get_elem_##OPNAME##_impl<A>::value>::eval(std::forward<A>(a));   \
  }

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

} // namespace fdbb

#endif // FDBB_COMPATIBILITY_HPP
