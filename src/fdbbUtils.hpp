/** @file fdbbUtils.hpp
 *
 *  @brief Utility class of the Fluid Dynamics Building Blocks
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_UTILS_HPP
#define FDBB_UTILS_HPP

#include <complex>
#include <functional>
#include <tuple>
#include <utility>

namespace fdbb {

using std::get;

/** @namespace fdbb::utils
 *
 *  @brief
 *  The \ref fdbb::utils namespace, containing extra utilities of the
 *  FDBB library
 *
 *  The \ref fdbb::utils namespace contains some extra utilities that
 *  are used by the FDBB library. Like the functionality in the \ref
 *  fdbb namespace, the extra utilities have a stable API that does
 *  not change over future releases of the FDBB library.
 *
 *  @note This Source Code Form is subject to the terms of the Mozilla
 *  Public License, v. 2.0. If a copy of the MPL was not distributed
 *  with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace utils {

/** @brief
 *  Compile-time djb2 hashing by Dan Bernstein
 *
 * @{
 */
template<typename hash_t, typename T>
hash_t constexpr djb2(hash_t hash, T t)
{
  return static_cast<hash_t>(((hash << 5) + hash) + static_cast<hash_t>(t));
}

template<typename hash_t, typename T, typename... Ts>
hash_t constexpr djb2(hash_t hash, T t, Ts... ts)
{
  return djb2(djb2(hash, t), ts...);
}

template<typename hash_t, typename... Ts>
hash_t constexpr hash(Ts... ts)
{
  return djb2<hash_t>(5381, ts...);
}
/** @} */

/**
 * @brief
 * Checks if a type is a complex type
 *
 * @tparam T The type that is checked for being of complex type
 *
 * @{
 */
template<typename T>
struct is_complex : std::false_type
{
};
template<typename T>
struct is_complex<std::complex<T>> : std::true_type
{
};
/** @} */

/**
 * @brief
 * Converts argument to given type
 *
 * @tparam D The destination type
 * @tparam S The source type
 *
 * @{
 */
template<typename D, typename S>
D
to_type(S s)
{
  return std::forward<D>(s);
}
template<typename D, typename S>
D
to_type(std::complex<S> s)
{
  return (D)s.real();
}
/** @} */

/** @brief
 *  Compile-time for_each loop (one parameter version)
 *
 *  @tparam Inc0 The increment of the 0th argument (must be nonzero)
 *  @tparam T0   The type of the 0th argument
 *  @tparam F    The functor to be applied
 *
 * @{
 */

namespace detail {

// Evaluate with return value
template<std::size_t I0,
         std::size_t Inc0,
         typename T0,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_return_1
{
  static auto constexpr eval(T0&& t0,
                             F f,
                             std::integral_constant<std::size_t, I0>,
                             std::integral_constant<std::size_t, Inc0>) noexcept
    -> decltype(std::tuple_cat(
      std::make_tuple(f(get<I0>(t0),
                        std::integral_constant<std::size_t, I0>())),
      for_each_return_1<I0 + Inc0, Inc0, T0, F>::eval(
        std::forward<T0>(t0),
        f,
        std::integral_constant<std::size_t, I0 + Inc0>(),
        std::integral_constant<std::size_t, Inc0>())))
  {
    return std::tuple_cat(
      std::make_tuple(
        f(get<I0>(t0), std::integral_constant<std::size_t, I0>())),
      for_each_return_1<I0 + Inc0, Inc0, T0, F>::eval(
        std::forward<T0>(t0),
        f,
        std::integral_constant<std::size_t, I0 + Inc0>(),
        std::integral_constant<std::size_t, Inc0>()));
  }
};

// Evaluate without return value
template<std::size_t I0,
         std::size_t Inc0,
         typename T0,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_1
{
  static void eval(T0&& t0,
                   F f,
                   std::integral_constant<std::size_t, I0>,
                   std::integral_constant<std::size_t, Inc0>)
  {
    f(get<I0>(t0), std::integral_constant<std::size_t, I0>());
    for_each_1<I0 + Inc0, Inc0, T0, F>::eval(
      std::forward<T0>(t0),
      f,
      std::integral_constant<std::size_t, I0 + Inc0>(),
      std::integral_constant<std::size_t, Inc0>());
  }
};

// Evaluate with return value
template<std::size_t I0, std::size_t Inc0, typename T0, typename F>
struct for_each_return_1<I0, Inc0, T0, F, true>
{
  static auto constexpr eval(T0&& t0,
                             F f,
                             std::integral_constant<std::size_t, I0>,
                             std::integral_constant<std::size_t, Inc0>) noexcept
    -> decltype(std::make_tuple(f(get<I0>(t0),
                                  std::integral_constant<std::size_t, I0>())))
  {
    return std::make_tuple(
      f(get<I0>(t0), std::integral_constant<std::size_t, I0>()));
  }
};

// Evaluate without return value
template<std::size_t I0, std::size_t Inc0, typename T0, typename F>
struct for_each_1<I0, Inc0, T0, F, true>
{
  static void eval(T0&& t0,
                   F f,
                   std::integral_constant<std::size_t, I0>,
                   std::integral_constant<std::size_t, Inc0>)
  {
    f(get<I0>(t0), std::integral_constant<std::size_t, I0>());
  }
};

} // namespace detail

// For each loop without return value
template<std::size_t I0 = 0, std::size_t Inc0 = 1, typename T0, typename F>
void
for_each(T0&& t0, F f)
{
  detail::for_each_1<I0, Inc0, T0, F>::eval(
    std::forward<T0>(t0),
    f,
    std::integral_constant<std::size_t, I0>(),
    std::integral_constant<std::size_t, Inc0>());
}

// For each loop with return value
template<std::size_t I0 = 0, std::size_t Inc0 = 1, typename T0, typename F>
auto
for_each_return(T0&& t0, F f) noexcept
  -> decltype(detail::for_each_return_1<I0, Inc0, T0, F>::eval(
    std::forward<T0>(t0),
    f,
    std::integral_constant<std::size_t, I0>(),
    std::integral_constant<std::size_t, Inc0>()))
{
  return detail::for_each_return_1<I0, Inc0, T0, F>::eval(
    std::forward<T0>(t0),
    f,
    std::integral_constant<std::size_t, I0>(),
    std::integral_constant<std::size_t, Inc0>());
}

/** @} */

/** @brief
 *  Compile-time for_each loop (two parameter version)
 *
 *  @tparam Inc0 The increment of the 0th argument (must be nonzero)
 *  @tparam Inc1 The increment of the 1th argument
 *  @tparam T0   The type of the 0th argument
 *  @tparam T1   The type of the 1th argument
 *  @tparam F    The functor to be applied
 *
 * @{
 */

namespace detail {

// Evaluate with return value
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         typename T0,
         typename T1,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_return_2
{
  static auto constexpr eval(T0&& t0,
                             T1&& t1,
                             F f,
                             std::integral_constant<std::size_t, I0>,
                             std::integral_constant<std::size_t, Inc0>,
                             std::integral_constant<std::size_t, I1>,
                             std::integral_constant<std::size_t, Inc1>) noexcept
    -> decltype(std::tuple_cat(
      std::make_tuple(f(get<I0>(t0),
                        get<I1>(t1),
                        std::integral_constant<std::size_t, I0>(),
                        std::integral_constant<std::size_t, I1>())),
      for_each_return_2<I0 + Inc0, Inc0, I1 + Inc1, Inc1, T0, T1, F>::eval(
        std::forward<T0>(t0),
        std::forward<T1>(t1),
        f,
        std::integral_constant<std::size_t, I0 + Inc0>(),
        std::integral_constant<std::size_t, Inc0>(),
        std::integral_constant<std::size_t, I1 + Inc1>(),
        std::integral_constant<std::size_t, Inc1>())))
  {
    return std::tuple_cat(
      std::make_tuple(f(get<I0>(t0),
                        get<I1>(t1),
                        std::integral_constant<std::size_t, I0>(),
                        std::integral_constant<std::size_t, I1>())),
      for_each_return_2<I0 + Inc0, Inc0, I1 + Inc1, Inc1, T0, T1, F>::eval(
        std::forward<T0>(t0),
        std::forward<T1>(t1),
        f,
        std::integral_constant<std::size_t, I0 + Inc0>(),
        std::integral_constant<std::size_t, Inc0>(),
        std::integral_constant<std::size_t, I1 + Inc1>(),
        std::integral_constant<std::size_t, Inc1>()));
  }
};

// Evaluate without return value
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         typename T0,
         typename T1,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_2
{
  static void eval(T0&& t0,
                   T1&& t1,
                   F f,
                   std::integral_constant<std::size_t, I0>,
                   std::integral_constant<std::size_t, Inc0>,
                   std::integral_constant<std::size_t, I1>,
                   std::integral_constant<std::size_t, Inc1>)
  {
    f(get<I0>(t0),
      get<I1>(t1),
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, I1>());
    for_each_2<I0 + Inc0, Inc0, I1 + Inc1, Inc1, T0, T1, F>::eval(
      std::forward<T0>(t0),
      std::forward<T1>(t1),
      f,
      std::integral_constant<std::size_t, I0 + Inc0>(),
      std::integral_constant<std::size_t, Inc0>(),
      std::integral_constant<std::size_t, I1 + Inc1>(),
      std::integral_constant<std::size_t, Inc1>());
  }
};

// Evaluate with return value
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         typename T0,
         typename T1,
         typename F>
struct for_each_return_2<I0, Inc0, I1, Inc1, T0, T1, F, true>
{
  static auto constexpr eval(T0&& t0,
                             T1&& t1,
                             F f,
                             std::integral_constant<std::size_t, I0>,
                             std::integral_constant<std::size_t, Inc0>,
                             std::integral_constant<std::size_t, I1>,
                             std::integral_constant<std::size_t, Inc1>) noexcept
    -> decltype(std::make_tuple(f(get<I0>(t0),
                                  get<I1>(t1),
                                  std::integral_constant<std::size_t, I0>(),
                                  std::integral_constant<std::size_t, I1>())))
  {
    return std::make_tuple(f(get<I0>(t0),
                             get<I1>(t1),
                             std::integral_constant<std::size_t, I0>(),
                             std::integral_constant<std::size_t, I1>()));
  }
};

// Evaluate without return value
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         typename T0,
         typename T1,
         typename F>
struct for_each_2<I0, Inc0, I1, Inc1, T0, T1, F, true>
{
  static void eval(T0&& t0,
                   T1&& t1,
                   F f,
                   std::integral_constant<std::size_t, I0>,
                   std::integral_constant<std::size_t, Inc0>,
                   std::integral_constant<std::size_t, I1>,
                   std::integral_constant<std::size_t, Inc1>)
  {
    f(get<I0>(t0),
      get<I1>(t1),
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, I1>());
  }
};

} // namespace detail

// Evaluate with return value
template<std::size_t I0 = 0,
         std::size_t Inc0 = 1,
         std::size_t I1 = I0,
         std::size_t Inc1 = Inc0,
         typename T0,
         typename T1,
         typename F>
auto
for_each_return(T0&& t0, T1&& t1, F f) noexcept
  -> decltype(detail::for_each_return_2<I0, Inc0, I1, Inc1, T0, T1, F>::eval(
    std::forward<T0>(t0),
    std::forward<T1>(t1),
    f,
    std::integral_constant<std::size_t, I0>(),
    std::integral_constant<std::size_t, Inc0>(),
    std::integral_constant<std::size_t, I1>(),
    std::integral_constant<std::size_t, Inc1>()))
{
  return detail::for_each_return_2<I0, Inc0, I1, Inc1, T0, T1, F>::eval(
    std::forward<T0>(t0),
    std::forward<T1>(t1),
    f,
    std::integral_constant<std::size_t, I0>(),
    std::integral_constant<std::size_t, Inc0>(),
    std::integral_constant<std::size_t, I1>(),
    std::integral_constant<std::size_t, Inc1>());
}

// Evaluate without return value
template<std::size_t I0 = 0,
         std::size_t Inc0 = 1,
         std::size_t I1 = I0,
         std::size_t Inc1 = Inc0,
         typename T0,
         typename T1,
         typename F>
void
for_each(T0&& t0, T1&& t1, F f)
{
  detail::for_each_2<I0, Inc0, I1, Inc1, T0, T1, F>::eval(
    std::forward<T0>(t0),
    std::forward<T1>(t1),
    f,
    std::integral_constant<std::size_t, I0>(),
    std::integral_constant<std::size_t, Inc0>(),
    std::integral_constant<std::size_t, I1>(),
    std::integral_constant<std::size_t, Inc1>());
}
/** @} */

/** @brief
 *  Compile-time for_each loop (three parameter version)
 *
 *  @tparam Inc0 The increment of the 0th argument (must be nonzero)
 *  @tparam Inc1 The increment of the 1th argument
 *  @tparam Inc2 The increment of the 2th argument
 *  @tparam T0   The type of the 0th argument
 *  @tparam T1   The type of the 1th argument
 *  @tparam T2   The type of the 2th argument
 *  @tparam F    The functor to be applied
 *
 * @{
 */

namespace detail {

// Evaluate with return value
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         typename T0,
         typename T1,
         typename T2,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_return_3
{
  static auto constexpr eval(T0&& t0,
                             T1&& t1,
                             T2&& t2,
                             F f,
                             std::integral_constant<std::size_t, I0>,
                             std::integral_constant<std::size_t, Inc0>,
                             std::integral_constant<std::size_t, I1>,
                             std::integral_constant<std::size_t, Inc1>,
                             std::integral_constant<std::size_t, I2>,
                             std::integral_constant<std::size_t, Inc2>) noexcept
    -> decltype(std::tuple_cat(
      std::make_tuple(f(get<I0>(t0),
                        get<I1>(t1),
                        get<I2>(t2),
                        std::integral_constant<std::size_t, I0>(),
                        std::integral_constant<std::size_t, I1>(),
                        std::integral_constant<std::size_t, I2>())),
      for_each_return_3<
        I0 + Inc0,
        Inc0,
        I1 + Inc1,
        Inc1,
        I2 + Inc2,
        Inc2,
        T0,
        T1,
        T2,
        F>::eval(std::forward<T0>(t0),
                 std::forward<T1>(t1),
                 std::forward<T2>(t2),
                 f,
                 std::integral_constant<std::size_t, I0 + Inc0>(),
                 std::integral_constant<std::size_t, Inc0>(),
                 std::integral_constant<std::size_t, I1 + Inc1>(),
                 std::integral_constant<std::size_t, Inc1>(),
                 std::integral_constant<std::size_t, I2 + Inc2>(),
                 std::integral_constant<std::size_t, Inc2>())))
  {
    return std::tuple_cat(
      std::make_tuple(f(get<I0>(t0),
                        get<I1>(t1),
                        get<I2>(t2),
                        std::integral_constant<std::size_t, I0>(),
                        std::integral_constant<std::size_t, I1>(),
                        std::integral_constant<std::size_t, I2>())),
      for_each_return_3<
        I0 + Inc0,
        Inc0,
        I1 + Inc1,
        Inc1,
        I2 + Inc2,
        Inc2,
        T0,
        T1,
        T2,
        F>::eval(std::forward<T0>(t0),
                 std::forward<T1>(t1),
                 std::forward<T2>(t2),
                 f,
                 std::integral_constant<std::size_t, I0 + Inc0>(),
                 std::integral_constant<std::size_t, Inc0>(),
                 std::integral_constant<std::size_t, I1 + Inc1>(),
                 std::integral_constant<std::size_t, Inc1>(),
                 std::integral_constant<std::size_t, I2 + Inc2>(),
                 std::integral_constant<std::size_t, Inc2>()));
  }
};

// Evaluate without return value
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         typename T0,
         typename T1,
         typename T2,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_3
{
  static void eval(T0&& t0,
                   T1&& t1,
                   T2&& t2,
                   F f,
                   std::integral_constant<std::size_t, I0>,
                   std::integral_constant<std::size_t, Inc0>,
                   std::integral_constant<std::size_t, I1>,
                   std::integral_constant<std::size_t, Inc1>,
                   std::integral_constant<std::size_t, I2>,
                   std::integral_constant<std::size_t, Inc2>)
  {
    f(get<I0>(t0),
      get<I1>(t1),
      get<I2>(t2),
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, I1>(),
      std::integral_constant<std::size_t, I2>());
    for_each_3<I0 + Inc0,
               Inc0,
               I1 + Inc1,
               Inc1,
               I2 + Inc2,
               Inc2,
               T0,
               T1,
               T2,
               F>::eval(std::forward<T0>(t0),
                        std::forward<T1>(t1),
                        std::forward<T2>(t2),
                        f,
                        std::integral_constant<std::size_t, I0 + Inc0>(),
                        std::integral_constant<std::size_t, Inc0>(),
                        std::integral_constant<std::size_t, I1 + Inc1>(),
                        std::integral_constant<std::size_t, Inc1>(),
                        std::integral_constant<std::size_t, I2 + Inc2>(),
                        std::integral_constant<std::size_t, Inc2>());
  }
};

// Evaluate with return value
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         typename T0,
         typename T1,
         typename T2,
         typename F>
struct for_each_return_3<I0, Inc0, I1, Inc1, I2, Inc2, T0, T1, T2, F, true>
{
  static auto constexpr eval(T0&& t0,
                             T1&& t1,
                             T2&& t2,
                             F f,
                             std::integral_constant<std::size_t, I0>,
                             std::integral_constant<std::size_t, Inc0>,
                             std::integral_constant<std::size_t, I1>,
                             std::integral_constant<std::size_t, Inc1>,
                             std::integral_constant<std::size_t, I2>,
                             std::integral_constant<std::size_t, Inc2>) noexcept
    -> decltype(std::make_tuple(f(get<I0>(t0),
                                  get<I1>(t1),
                                  get<I2>(t2),
                                  std::integral_constant<std::size_t, I0>(),
                                  std::integral_constant<std::size_t, I1>(),
                                  std::integral_constant<std::size_t, I2>())))
  {
    return std::make_tuple(f(get<I0>(t0),
                             get<I1>(t1),
                             get<I2>(t2),
                             std::integral_constant<std::size_t, I0>(),
                             std::integral_constant<std::size_t, I1>(),
                             std::integral_constant<std::size_t, I2>()));
  }
};

// Evaluate without return value
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         typename T0,
         typename T1,
         typename T2,
         typename F>
struct for_each_3<I0, Inc0, I1, Inc1, I2, Inc2, T0, T1, T2, F, true>
{
  static void eval(T0&& t0,
                   T1&& t1,
                   T2&& t2,
                   F f,
                   std::integral_constant<std::size_t, I0>,
                   std::integral_constant<std::size_t, Inc0>,
                   std::integral_constant<std::size_t, I1>,
                   std::integral_constant<std::size_t, Inc1>,
                   std::integral_constant<std::size_t, I2>,
                   std::integral_constant<std::size_t, Inc2>)
  {
    f(get<I0>(t0),
      get<I1>(t1),
      get<I2>(t2),
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, I1>(),
      std::integral_constant<std::size_t, I2>());
  }
};

} // namespace detail

// Evaluate with return value
template<std::size_t I0 = 0,
         std::size_t Inc0 = 1,
         std::size_t I1 = I0,
         std::size_t Inc1 = Inc0,
         std::size_t I2 = I0,
         std::size_t Inc2 = Inc0,
         typename T0,
         typename T1,
         typename T2,
         typename F>
auto
for_each_return(T0&& t0, T1&& t1, T2&& t2, F f) noexcept -> decltype(
  detail::for_each_return_3<I0, Inc0, I1, Inc1, I2, Inc2, T0, T1, T2, F>::eval(
    std::forward<T0>(t0),
    std::forward<T1>(t1),
    std::forward<T2>(t2),
    f,
    std::integral_constant<std::size_t, I0>(),
    std::integral_constant<std::size_t, Inc0>(),
    std::integral_constant<std::size_t, I1>(),
    std::integral_constant<std::size_t, Inc1>(),
    std::integral_constant<std::size_t, I2>(),
    std::integral_constant<std::size_t, Inc2>()))
{
  return detail::
    for_each_return_3<I0, Inc0, I1, Inc1, I2, Inc2, T0, T1, T2, F>::eval(
      std::forward<T0>(t0),
      std::forward<T1>(t1),
      std::forward<T2>(t2),
      f,
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, Inc0>(),
      std::integral_constant<std::size_t, I1>(),
      std::integral_constant<std::size_t, Inc1>(),
      std::integral_constant<std::size_t, I2>(),
      std::integral_constant<std::size_t, Inc2>());
}

// Evaluate without return value
template<std::size_t I0 = 0,
         std::size_t Inc0 = 1,
         std::size_t I1 = I0,
         std::size_t Inc1 = Inc0,
         std::size_t I2 = I0,
         std::size_t Inc2 = Inc0,
         typename T0,
         typename T1,
         typename T2,
         typename F>
void
for_each(T0&& t0, T1&& t1, T2&& t2, F f)
{
  detail::for_each_3<I0, Inc0, I1, Inc1, I2, Inc2, T0, T1, T2, F>::eval(
    std::forward<T0>(t0),
    std::forward<T1>(t1),
    std::forward<T2>(t2),
    f,
    std::integral_constant<std::size_t, I0>(),
    std::integral_constant<std::size_t, Inc0>(),
    std::integral_constant<std::size_t, I1>(),
    std::integral_constant<std::size_t, Inc1>(),
    std::integral_constant<std::size_t, I2>(),
    std::integral_constant<std::size_t, Inc2>());
}
/** @} */

/** @brief
 *  Compile-time for_each loop (four parameter version)
 *
 *  @tparam Inc0 The increment of the 0th argument (must be nonzero)
 *  @tparam Inc1 The increment of the 1th argument
 *  @tparam Inc2 The increment of the 2th argument
 *  @tparam Inc3 The increment of the 3th argument
 *  @tparam T0   The type of the 0th argument
 *  @tparam T1   The type of the 1th argument
 *  @tparam T2   The type of the 2th argument
 *  @tparam T3   The type of the 3th argument
 *  @tparam F    The functor to be applied
 *
 * @{
 */

namespace detail {

// Evaluate with return value
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         std::size_t I3,
         std::size_t Inc3,
         typename T0,
         typename T1,
         typename T2,
         typename T3,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_return_4
{
  static auto constexpr eval(T0&& t0,
                             T1&& t1,
                             T2&& t2,
                             T3&& t3,
                             F f,
                             std::integral_constant<std::size_t, I0>,
                             std::integral_constant<std::size_t, Inc0>,
                             std::integral_constant<std::size_t, I1>,
                             std::integral_constant<std::size_t, Inc1>,
                             std::integral_constant<std::size_t, I2>,
                             std::integral_constant<std::size_t, Inc2>,
                             std::integral_constant<std::size_t, I3>,
                             std::integral_constant<std::size_t, Inc3>) noexcept
    -> decltype(std::tuple_cat(
      std::make_tuple(f(get<I0>(t0),
                        get<I1>(t1),
                        get<I2>(t2),
                        get<I3>(t3),
                        std::integral_constant<std::size_t, I0>(),
                        std::integral_constant<std::size_t, I1>(),
                        std::integral_constant<std::size_t, I2>(),
                        std::integral_constant<std::size_t, I3>())),
      for_each_return_4<
        I0 + Inc0,
        Inc0,
        I1 + Inc1,
        Inc1,
        I2 + Inc2,
        Inc2,
        I3 + Inc3,
        Inc3,
        T0,
        T1,
        T2,
        T3,
        F>::eval(std::forward<T0>(t0),
                 std::forward<T1>(t1),
                 std::forward<T2>(t2),
                 std::forward<T3>(t3),
                 f,
                 std::integral_constant<std::size_t, I0 + Inc0>(),
                 std::integral_constant<std::size_t, Inc0>(),
                 std::integral_constant<std::size_t, I1 + Inc1>(),
                 std::integral_constant<std::size_t, Inc1>(),
                 std::integral_constant<std::size_t, I2 + Inc2>(),
                 std::integral_constant<std::size_t, Inc2>(),
                 std::integral_constant<std::size_t, I3 + Inc3>(),
                 std::integral_constant<std::size_t, Inc3>())))
  {
    return std::tuple_cat(
      std::make_tuple(f(get<I0>(t0),
                        get<I1>(t1),
                        get<I2>(t2),
                        get<I3>(t3),
                        std::integral_constant<std::size_t, I0>(),
                        std::integral_constant<std::size_t, I1>(),
                        std::integral_constant<std::size_t, I2>(),
                        std::integral_constant<std::size_t, I3>())),
      for_each_return_4<
        I0 + Inc0,
        Inc0,
        I1 + Inc1,
        Inc1,
        I2 + Inc2,
        Inc2,
        I3 + Inc3,
        Inc3,
        T0,
        T1,
        T2,
        T3,
        F>::eval(std::forward<T0>(t0),
                 std::forward<T1>(t1),
                 std::forward<T2>(t2),
                 std::forward<T3>(t3),
                 f,
                 std::integral_constant<std::size_t, I0 + Inc0>(),
                 std::integral_constant<std::size_t, Inc0>(),
                 std::integral_constant<std::size_t, I1 + Inc1>(),
                 std::integral_constant<std::size_t, Inc1>(),
                 std::integral_constant<std::size_t, I2 + Inc2>(),
                 std::integral_constant<std::size_t, Inc2>(),
                 std::integral_constant<std::size_t, I3 + Inc3>(),
                 std::integral_constant<std::size_t, Inc3>()));
  }
};

// Evaluate without return value
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         std::size_t I3,
         std::size_t Inc3,
         typename T0,
         typename T1,
         typename T2,
         typename T3,
         typename F,
         bool =
           (I0 + Inc0 >=
            std::tuple_size<typename std::remove_reference<T0>::type>::value)>
struct for_each_4
{
  static void eval(T0&& t0,
                   T1&& t1,
                   T2&& t2,
                   T3&& t3,
                   F f,
                   std::integral_constant<std::size_t, I0>,
                   std::integral_constant<std::size_t, Inc0>,
                   std::integral_constant<std::size_t, I1>,
                   std::integral_constant<std::size_t, Inc1>,
                   std::integral_constant<std::size_t, I2>,
                   std::integral_constant<std::size_t, Inc2>,
                   std::integral_constant<std::size_t, I3>,
                   std::integral_constant<std::size_t, Inc3>)
  {
    f(get<I0>(t0),
      get<I1>(t1),
      get<I2>(t2),
      get<I3>(t3),
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, I1>(),
      std::integral_constant<std::size_t, I2>(),
      std::integral_constant<std::size_t, I3>());
    for_each_4<I0 + Inc0,
               Inc0,
               I1 + Inc1,
               Inc1,
               I2 + Inc2,
               Inc2,
               I3 + Inc3,
               Inc3,
               T0,
               T1,
               T2,
               T3,
               F>::eval(std::forward<T0>(t0),
                        std::forward<T1>(t1),
                        std::forward<T2>(t2),
                        std::forward<T3>(t3),
                        f,
                        std::integral_constant<std::size_t, I0 + Inc0>(),
                        std::integral_constant<std::size_t, Inc0>(),
                        std::integral_constant<std::size_t, I1 + Inc1>(),
                        std::integral_constant<std::size_t, Inc1>(),
                        std::integral_constant<std::size_t, I2 + Inc2>(),
                        std::integral_constant<std::size_t, Inc2>(),
                        std::integral_constant<std::size_t, I3 + Inc3>(),
                        std::integral_constant<std::size_t, Inc3>());
  }
};

// Evaluate with return value
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         std::size_t I3,
         std::size_t Inc3,
         typename T0,
         typename T1,
         typename T2,
         typename T3,
         typename F>
struct for_each_return_4<I0,
                         Inc0,
                         I1,
                         Inc1,
                         I2,
                         Inc2,
                         I3,
                         Inc3,
                         T0,
                         T1,
                         T2,
                         T3,
                         F,
                         true>
{
  static auto constexpr eval(T0&& t0,
                             T1&& t1,
                             T2&& t2,
                             T3&& t3,
                             F f,
                             std::integral_constant<std::size_t, I0>,
                             std::integral_constant<std::size_t, Inc0>,
                             std::integral_constant<std::size_t, I1>,
                             std::integral_constant<std::size_t, Inc1>,
                             std::integral_constant<std::size_t, I2>,
                             std::integral_constant<std::size_t, Inc2>,
                             std::integral_constant<std::size_t, I3>,
                             std::integral_constant<std::size_t, Inc3>) noexcept
    -> decltype(std::make_tuple(f(get<I0>(t0),
                                  get<I1>(t1),
                                  get<I2>(t2),
                                  get<I3>(t3),
                                  std::integral_constant<std::size_t, I0>(),
                                  std::integral_constant<std::size_t, I1>(),
                                  std::integral_constant<std::size_t, I2>(),
                                  std::integral_constant<std::size_t, I3>())))
  {
    return std::make_tuple(f(get<I0>(t0),
                             get<I1>(t1),
                             get<I2>(t2),
                             get<I3>(t3),
                             std::integral_constant<std::size_t, I0>(),
                             std::integral_constant<std::size_t, I1>(),
                             std::integral_constant<std::size_t, I2>(),
                             std::integral_constant<std::size_t, I3>()));
  }
};

// Evaluate without return value
template<std::size_t I0,
         std::size_t Inc0,
         std::size_t I1,
         std::size_t Inc1,
         std::size_t I2,
         std::size_t Inc2,
         std::size_t I3,
         std::size_t Inc3,
         typename T0,
         typename T1,
         typename T2,
         typename T3,
         typename F>
struct for_each_4<I0,
                  Inc0,
                  I1,
                  Inc1,
                  I2,
                  Inc2,
                  I3,
                  Inc3,
                  T0,
                  T1,
                  T2,
                  T3,
                  F,
                  true>
{
  static void eval(T0&& t0,
                   T1&& t1,
                   T2&& t2,
                   T3&& t3,
                   F f,
                   std::integral_constant<std::size_t, I0>,
                   std::integral_constant<std::size_t, Inc0>,
                   std::integral_constant<std::size_t, I1>,
                   std::integral_constant<std::size_t, Inc1>,
                   std::integral_constant<std::size_t, I2>,
                   std::integral_constant<std::size_t, Inc2>,
                   std::integral_constant<std::size_t, I3>,
                   std::integral_constant<std::size_t, Inc3>)
  {
    f(get<I0>(t0),
      get<I1>(t1),
      get<I2>(t2),
      get<I3>(t3),
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, I1>(),
      std::integral_constant<std::size_t, I2>(),
      std::integral_constant<std::size_t, I3>());
  }
};

} // namespace detail

// Evaluate with return value
template<std::size_t I0 = 0,
         std::size_t Inc0 = 1,
         std::size_t I1 = I0,
         std::size_t Inc1 = Inc0,
         std::size_t I2 = I0,
         std::size_t Inc2 = Inc0,
         std::size_t I3 = I0,
         std::size_t Inc3 = Inc0,
         typename T0,
         typename T1,
         typename T2,
         typename T3,
         typename F>
auto
for_each_return(T0&& t0, T1&& t1, T2&& t2, T3&& t3, F f) noexcept
  -> decltype(detail::for_each_return_4<
              I0,
              Inc0,
              I1,
              Inc1,
              I2,
              Inc2,
              I3,
              Inc3,
              T0,
              T1,
              T2,
              T3,
              F>::eval(std::forward<T0>(t0),
                       std::forward<T1>(t1),
                       std::forward<T2>(t2),
                       std::forward<T3>(t3),
                       f,
                       std::integral_constant<std::size_t, I0>(),
                       std::integral_constant<std::size_t, Inc0>(),
                       std::integral_constant<std::size_t, I1>(),
                       std::integral_constant<std::size_t, Inc1>(),
                       std::integral_constant<std::size_t, I2>(),
                       std::integral_constant<std::size_t, Inc2>(),
                       std::integral_constant<std::size_t, I3>(),
                       std::integral_constant<std::size_t, Inc3>()))
{
  return detail::for_each_return_4<
    I0,
    Inc0,
    I1,
    Inc1,
    I2,
    Inc2,
    I3,
    Inc3,
    T0,
    T1,
    T2,
    T3,
    F>::eval(std::forward<T0>(t0),
             std::forward<T1>(t1),
             std::forward<T2>(t2),
             std::forward<T3>(t3),
             f,
             std::integral_constant<std::size_t, I0>(),
             std::integral_constant<std::size_t, Inc0>(),
             std::integral_constant<std::size_t, I1>(),
             std::integral_constant<std::size_t, Inc1>(),
             std::integral_constant<std::size_t, I2>(),
             std::integral_constant<std::size_t, Inc2>(),
             std::integral_constant<std::size_t, I3>(),
             std::integral_constant<std::size_t, Inc3>());
}

// Evaluate without return value
template<std::size_t I0 = 0,
         std::size_t Inc0 = 1,
         std::size_t I1 = I0,
         std::size_t Inc1 = Inc0,
         std::size_t I2 = I0,
         std::size_t Inc2 = Inc0,
         std::size_t I3 = I0,
         std::size_t Inc3 = Inc0,
         typename T0,
         typename T1,
         typename T2,
         typename T3,
         typename F>
void
for_each(T0&& t0, T1&& t1, T2&& t2, T3&& t3, F f)
{
  detail::
    for_each_4<I0, Inc0, I1, Inc1, I2, Inc2, I3, Inc3, T0, T1, T2, T3, F>::eval(
      std::forward<T0>(t0),
      std::forward<T1>(t1),
      std::forward<T2>(t2),
      std::forward<T3>(t3),
      f,
      std::integral_constant<std::size_t, I0>(),
      std::integral_constant<std::size_t, Inc0>(),
      std::integral_constant<std::size_t, I1>(),
      std::integral_constant<std::size_t, Inc1>(),
      std::integral_constant<std::size_t, I2>(),
      std::integral_constant<std::size_t, Inc2>(),
      std::integral_constant<std::size_t, I3>(),
      std::integral_constant<std::size_t, Inc3>());
}
/** @} */

#if __cplusplus < 201402L

// Stores a tuple of indices.  Used by tuple and pair, and by bind() to
// extract the elements in a tuple.
template<std::size_t... _Indexes>
struct _Index_tuple
{
};

// Concatenates two _Index_tuples.
template<typename _Itup1, typename _Itup2>
struct _Itup_cat;

template<std::size_t... _Ind1, std::size_t... _Ind2>
struct _Itup_cat<_Index_tuple<_Ind1...>, _Index_tuple<_Ind2...>>
{
  using __type = _Index_tuple<_Ind1..., (_Ind2 + sizeof...(_Ind1))...>;
};

// Builds an _Index_tuple<0, 1, 2, ..., _Num-1>.
template<std::size_t _Num>
struct _Build_index_tuple
  : _Itup_cat<typename _Build_index_tuple<_Num / 2>::__type,
              typename _Build_index_tuple<_Num - _Num / 2>::__type>
{
};

template<>
struct _Build_index_tuple<1>
{
  using __type = _Index_tuple<0>;
};

template<>
struct _Build_index_tuple<0>
{
  using __type = _Index_tuple<>;
};

/// Class template integer_sequence (C++11 backport from C++14)
template<typename _Tp, _Tp... _Idx>
struct integer_sequence
{
  using value_type = _Tp;
  static constexpr std::size_t size() { return sizeof...(_Idx); }
};

/// Class template _Make_integer_sequence (C++11 backport from C++14)
template<typename _Tp,
         _Tp _Num,
         typename _ISeq = typename _Build_index_tuple<_Num>::__type>
struct _Make_integer_sequence;

/// Class template _Make_integer_sequence (C++11 backport from C++14)
template<typename _Tp, _Tp _Num, std::size_t... _Idx>
struct _Make_integer_sequence<_Tp, _Num, _Index_tuple<_Idx...>>
{
  static_assert(_Num >= 0, "Cannot make integer sequence of negative length");

  using __type = integer_sequence<_Tp, static_cast<_Tp>(_Idx)...>;
};

/// Alias template \ref fdbb::utils::make_integer_sequence (C++11 backport from
/// C++14)
template<typename _Tp, _Tp _Num>
using make_integer_sequence =
  typename _Make_integer_sequence<_Tp, _Num>::__type;

/// Alias template \ref fdbb::utils::index_sequence (C++11 backport from C++14)
template<std::size_t... _Idx>
using index_sequence = integer_sequence<std::size_t, _Idx...>;

/// Alias template \ref fdbb::utils::make_index_sequence (C++11 backport from
/// C++14)
template<std::size_t _Num>
using make_index_sequence = make_integer_sequence<std::size_t, _Num>;

/// Alias template \ref fdbb::utils::index_sequence_for (C++11 backport from
/// C++14)
template<typename... _Types>
using index_sequence_for = make_index_sequence<sizeof...(_Types)>;

#else

/// @brief Alias template \ref fdbb::utils::make_integer_sequence from \ref
/// std::make_integer_sequence (C++14)
template<typename _Tp, _Tp... _Idx>
using integer_sequence = std::integer_sequence<_Tp, _Idx...>;

/// @brief Alias temmplate \ref fdbb::utils::index_sequence from \ref
/// std::index_squence (C++14)
template<size_t... _Idx>
using index_sequence = std::index_sequence<_Idx...>;

/// @brief Alias template \ref fdbb::utils::make_integer_sequence from \ref
/// std::make_integer_sequence (C++14)
template<size_t _Num>
using make_integer_sequence = std::make_index_sequence<_Num>;

/// @brief Alias template \ref fdbb::utils::index_sequence_for from \ref
/// std::index_sequence_for (C++14)
template<typename... _Types>
using index_sequence_for = std::index_sequence_for<_Types...>;

/// @brief Alias template \ref fdbb::utils::make_index_sequence from \ref
/// std::make_index_sequence (C++14)
template<size_t _Num>
using make_index_sequence = std::make_index_sequence<_Num>;

#endif

/** @brief
 *  Returns content of \ref index_sequence as a \ref std::tuple
 */
template<size_t... _Idx>
auto constexpr make_tuple(index_sequence<_Idx...>) noexcept
  -> decltype(std::make_tuple(_Idx...))
{
  return std::make_tuple(_Idx...);
}

/** @brief
 *  Returns content of \ref integer_sequence as \refstd::tuple
 */
template<typename _Tp, _Tp... _Idx>
auto constexpr make_tuple(integer_sequence<_Tp, _Idx...>) noexcept
  -> decltype(std::make_tuple(_Idx...))
{
  return std::make_tuple(_Idx...);
}

/** @brief
 *  Returns the sum of entries in the tuple
 *
 *  @{
 */
template<typename... Ts>
struct __tuple_sum_type;

template<typename T>
struct __tuple_sum_type<T>
{
  using type = T;
};

template<typename T, typename... Ts>
struct __tuple_sum_type<T, Ts...>
{
  using type = decltype(std::declval<T>() +
                        std::declval<typename __tuple_sum_type<Ts...>::type>());
};

template<typename T>
T
__tuple_sum(T&& t)
{
  return t;
}

template<typename T, typename... Ts>
typename __tuple_sum_type<T, Ts...>::type
__tuple_sum(T&& t, Ts&&... ts)
{
  return std::forward<T>(t) + __tuple_sum(std::forward<Ts>(ts)...);
}

template<typename T, size_t... Is>
auto constexpr _tuple_sum(T&& t, index_sequence<Is...>) noexcept
  -> decltype(__tuple_sum(get<Is>(t)...))
{
  return __tuple_sum(get<Is>(t)...);
}

template<typename Tuple>
auto constexpr tuple_sum(Tuple&& t) noexcept
  -> decltype(_tuple_sum(std::forward<Tuple>(t),
                         make_index_sequence<std::tuple_size<Tuple>{}>{}))
{
  return _tuple_sum(std::forward<Tuple>(t),
                    make_index_sequence<std::tuple_size<Tuple>{}>{});
}
/** @} */

#if __cplusplus <= 201402L

/**
 * C++14 and below does not provide std::apply()
 */
namespace detail {
template<class F, class Tuple, std::size_t... I>
constexpr auto
apply_impl(F&& f, Tuple&& t, index_sequence<I...>)
  -> decltype(f(std::get<I>(std::forward<Tuple>(t))...))
{
  return f(std::get<I>(std::forward<Tuple>(t))...);
}
} // namespace detail

template<class F, class Tuple>
constexpr auto
apply(F&& f, Tuple&& t) -> decltype(detail::apply_impl(
  std::forward<F>(f),
  std::forward<Tuple>(t),
  make_index_sequence<
    std::tuple_size<typename std::decay<Tuple>::type>::value>{}))
{
  return detail::apply_impl(
    std::forward<F>(f),
    std::forward<Tuple>(t),
    make_index_sequence<
      std::tuple_size<typename std::decay<Tuple>::type>::value>{});
}

#else

/**
 * C++17 provides std::apply()
 */
template<class F, class... Args>
using invoke = std::invoke<F, Args...>;

template<class F, class Tuple>
using apply = std::apply<T, Tuple>;

#endif

} // namespace utils

} // namespace fdbb

#endif // FDBB_UTILS_HPP
