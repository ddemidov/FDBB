/** @file fdbbBlockExprForward.hpp
 *
 *  @brief Block expression class (forward declarations)
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_BLOCKEXPR_FORWARD_HPP
#define FDBB_BLOCKEXPR_FORWARD_HPP

#include <tuple>

#include "fdbbConfig.hpp"

namespace fdbb {

/// Forward declaration
struct fdbbBlockBase
{
};

struct fdbbBlockVectorBase : public fdbbBlockBase
{
};
struct fdbbBlockRowVectorBase : public fdbbBlockVectorBase
{
};
struct fdbbBlockColVectorBase : public fdbbBlockVectorBase
{
};

template<std::size_t Tag, typename T, std::size_t _rows = 1>
struct fdbbBlockRowVector;
template<std::size_t Tag, typename T, std::size_t _cols = 1>
struct fdbbBlockColVector;
template<std::size_t Tag, typename T, std::size_t _rows = 1>
struct fdbbBlockRowVectorView;
template<std::size_t Tag, typename T, std::size_t _cols = 1>
struct fdbbBlockColVectorView;

struct fdbbBlockMatrixBase : public fdbbBlockBase
{
};

template<std::size_t Tag,
         typename T,
         std::size_t _rows = 1,
         std::size_t _cols = 1,
         bool transposed = false>
struct fdbbBlockMatrix;
template<std::size_t Tag,
         typename T,
         std::size_t _rows = 1,
         std::size_t _cols = 1,
         bool transposed = false>
struct fdbbBlockMatrixView;

template<std::size_t Tag,
         std::size_t _rows = 1,
         std::size_t _cols = 1,
         bool transposed = false,
         typename... Ts>
struct fdbbBlockExpr;

} // namespace fdbb

namespace std {

template<size_t Tag, typename T, std::size_t _rows>
struct tuple_size<::fdbb::fdbbBlockRowVector<Tag, T, _rows>>
  : public integral_constant<std::size_t, _rows>
{
};

template<size_t Tag, typename T, std::size_t _cols>
struct tuple_size<::fdbb::fdbbBlockColVector<Tag, T, _cols>>
  : public integral_constant<std::size_t, _cols>
{
};

template<size_t Tag, typename T, std::size_t _rows>
struct tuple_size<::fdbb::fdbbBlockRowVectorView<Tag, T, _rows>>
  : public integral_constant<std::size_t, _rows>
{
};

template<size_t Tag, typename T, std::size_t _cols>
struct tuple_size<::fdbb::fdbbBlockColVectorView<Tag, T, _cols>>
  : public integral_constant<std::size_t, _cols>
{
};

template<size_t Tag,
         typename T,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed>
struct tuple_size<::fdbb::fdbbBlockMatrix<Tag, T, _rows, _cols, transposed>>
  : public integral_constant<std::size_t, _rows * _cols>
{
};

template<size_t Tag,
         typename T,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed>
struct tuple_size<::fdbb::fdbbBlockMatrixView<Tag, T, _rows, _cols, transposed>>
  : public integral_constant<std::size_t, _rows * _cols>
{
};

template<size_t Tag,
         std::size_t _rows,
         std::size_t _cols,
         bool transposed,
         typename... Ts>
struct tuple_size<::fdbb::fdbbBlockExpr<Tag, _rows, _cols, transposed, Ts...>>
  : public integral_constant<std::size_t, _rows * _cols>
{
};

} // namespace std

#endif // FDBB_BLOCKEXPR_FORWARD_HPP
