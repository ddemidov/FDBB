/** @file fdbbConservative1d.hpp
 *
 *  @brief 1D implementation for conservative variables
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_CONSERVATIVE_1D_H
#define FDBB_CONSERVATIVE_1D_H

#include <type_traits>

#include "fdbbConfig.hpp"
#include "fdbbEnums.hpp"
#include "fdbbTraits.hpp"
#include "fdbbTypes.hpp"

namespace fdbb {

/** @brief
 *
 *  Specialization of the fdbbVariables structure for conservative
 *  variables in 1d space dimensions and arbitrary equation of state
 *
 *  \f[
 *      U = \begin{bmatrix}
 *            u_0\\
 *            u_1\\
 *            u_2
 *          \end{bmatrix}
 *        = \begin{bmatrix}
 *            \rho\\
 *            \rho v_0\\
 *            \rho E
 *          \end{bmatrix}
 *  \f]
 *  where
 *  \f$ \rho      \f$ is the volumetric mass density,
 *  \f$ \rho v_0  \f$ is the momentum in x-direction, and
 *  \f$ \rho E    \f$ is the total energy per unit volume
 */
template<typename EOS, typename Traits>
struct fdbbVariables<EOS, 1, EnumForm::conservative, Traits>
{
  /// @brief Equation of state
  using eos = EOS;

  /// @brief Type traits
  using traits = Traits;

  /// @brief Dimension
  static constexpr index_t dim = 1;

  /// @brief Return output stream
  static std::ostream& print(std::ostream& os)
  {
    os << "Conservative variables in 1d, ";
    eos::print(os);
    return os;
  }

  /** @brief
   *  Volumetric mass density variable \f$ \rho = u_0 \f$ for
   *  conservative variables in 1d
   *
   *  @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rho(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      traits::template getVariable<EnumVar::rho>(std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::rho>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Momentum variable \f$ \rho v_0 = u_1 \f$ for conservative
   *  variables in 1d
   *
   *  @ingroup VariablesPrimary
   */
  template<index_t idim, typename... Vars>
  static FDBB_INLINE auto constexpr rhov(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      idim == 0,
      decltype(traits::template getVariable<EnumVar::rhov_x>(
        std::forward<Vars>(vars)...))>::type
#endif
  {
    return traits::template getVariable<EnumVar::rhov_x>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Total energy per unit volume variable \f$ \rho E = u_2 \f$ for
   *  conservative variables in 1d
   *
   *  @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoE(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      traits::template getVariable<EnumVar::rhoE>(std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::rhoE>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Magnitude of momentum \f$ |\rho v_0| \f$ for conservative
   *  variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhov_mag(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_fabs(rhov<0>(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_fabs(rhov<0>(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Magnitude of momentum \f$ |\rho v_0|^2 \f$ squared for
   *  conservative variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhov_mag2(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                               rhov<0>(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                          rhov<0>(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Total energy per unit mass variable \f$ E = u_2/u_0 \f$ for
   *  conservative variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr E(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(
      traits::template getVariable<EnumVar::rhoE>(std::forward<Vars>(vars)...),
      traits::template getVariable<EnumVar::rho>(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_div(
      traits::template getVariable<EnumVar::rhoE>(std::forward<Vars>(vars)...),
      traits::template getVariable<EnumVar::rho>(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Velocity variable \f$ v_0 = u_1/u_0 \f$ for conservative
   *  variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<index_t idim, typename... Vars>
  static FDBB_INLINE auto constexpr v(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(rhov<idim>(std::forward<Vars>(vars)...),
                               rho(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_div(rhov<idim>(std::forward<Vars>(vars)...),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Magnitude of velocity \f$ |v_0| \f$ for conservative variables
   *  in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr v_mag(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_fabs(v<0>(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_fabs(v<0>(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Magnitude of velocity \f$ |v_0|^2 \f$ squared for conservative
   *  variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr v_mag2(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      fdbb::elem_div(fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                                    rhov<0>(std::forward<Vars>(vars)...)),
                     fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                                    rho(std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::elem_div(fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                                         rhov<0>(std::forward<Vars>(vars)...)),
                          fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                                         rho(std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  Kinetic energy per unit volume variable \f$ \rho E_kin = \frac12 ((\rho
   *  v_0)^2)/\rho \f$ for conservative variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoE_kin(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
      fdbb::elem_div(fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                                    rhov<0>(std::forward<Vars>(vars)...)),
                     rho(std::forward<Vars>(vars)...)))
#endif
  {
    return CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
           fdbb::elem_div(fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                                         rhov<0>(std::forward<Vars>(vars)...)),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Kinetic energy per unit mass variable \f$ E_kin = \frac12 ((\rho
   *  v_0)^2)/\rho^2 \f$ for conservative variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr E_kin(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
      fdbb::elem_div(fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                                    rhov<0>(std::forward<Vars>(vars)...)),
                     fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                                    rho(std::forward<Vars>(vars)...))))
#endif
  {
    return CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
           fdbb::elem_div(fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                                         rhov<0>(std::forward<Vars>(vars)...)),
                          fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                                         rho(std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  Internal energy per unit volume variable \f$ \rho e = \rho E -
   *  \frac12 ((\rho v_0)^2)/\rho \f$ for conservative variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoe(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(rhoE(std::forward<Vars>(vars)...) -
                rhoE_kin(std::forward<Vars>(vars)...))
#endif
  {
    return rhoE(std::forward<Vars>(vars)...) -
           rhoE_kin(std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Internal energy per unit mass variable \f$ e = E - \frac12
   *  ((\rho v_0)^2)/\rho^2 \f$ for conservative variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr e(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(E(std::forward<Vars>(vars)...) -
                E_kin(std::forward<Vars>(vars)...))
#endif
  {
    return E(std::forward<Vars>(vars)...) - E_kin(std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Total enthalpy per unit volume \f$ \rho H = \rho E + p \f$ for
   *  conservative variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoH(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_base_of<fdbbEOS_pVT, eos>::value,
      decltype(rhoE(std::forward<Vars>(vars)...) +
               eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                           e(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return rhoE(std::forward<Vars>(vars)...) +
           eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                       e(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Total enthalpy per unit mass \f$ H = E + p/\rho \f$ for
   *  conservative variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr H(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_base_of<fdbbEOS_pVT, eos>::value,
      decltype(fdbb::elem_div(rhoE(std::forward<Vars>(vars)...) +
                                eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                                            e(std::forward<Vars>(vars)...)),
                              rho(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return fdbb::elem_div(rhoE(std::forward<Vars>(vars)...) +
                            eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                                        e(std::forward<Vars>(vars)...)),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Enthalpy per unit volume \f$ \rho h = \rho e + p \f$ for
   *  conservative variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoh(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_base_of<fdbbEOS_pVT, eos>::value,
      decltype(rhoe(std::forward<Vars>(vars)...) +
               eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                           e(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return rhoe(std::forward<Vars>(vars)...) +
           eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                       e(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Enthalpy per unit mass \f$ h = e + p/\rho \f$ for conservative
   *  variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr h(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_base_of<fdbbEOS_pVT, eos>::value,
      decltype(fdbb::elem_div(rhoe(std::forward<Vars>(vars)...) +
                                eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                                            e(std::forward<Vars>(vars)...)),
                              rho(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return fdbb::elem_div(rhoe(std::forward<Vars>(vars)...) +
                            eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                                        e(std::forward<Vars>(vars)...)),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Absolute pressure variable \f$ p \f$ for conservative
   *  variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr p(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_base_of<fdbbEOS_pVT, eos>::value,
      decltype(eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                           e(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return eos::p_rhoe(rho(std::forward<Vars>(vars)...),
                       e(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Speed of sound variable \f$ c \f$ for conservative
   *  variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr c(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_base_of<fdbbEOS_pVT, eos>::value,
      decltype(eos::c_rhoe(rho(std::forward<Vars>(vars)...),
                           e(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return eos::c_rhoe(rho(std::forward<Vars>(vars)...),
                       e(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Directional velocty \f$ v_dir = v_x*d_x/|d_x| \f$ for conservative
   *  variables in 1d.
   *
   *  @tparam    T The type of the entries of direction vector d
   *
   *  @param[in] d_x  x component of vector d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename T, typename... Vars>
  static FDBB_INLINE auto constexpr v_dir(T&& d_x, Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                                              std::forward<T>(d_x)),
                               fdbb::elem_fabs(std::forward<T>(d_x))))
#endif
  {
    return fdbb::elem_div(
      fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...), std::forward<T>(d_x)),
      fdbb::elem_fabs(std::forward<T>(d_x)));
  }

  /** @brief
   *  State vector of conservative variables in 1d
   *
   *  @ingroup VariablesPrimary
   */
  template<std::size_t Tag, typename... Vars>
  static FDBB_INLINE auto constexpr conservative(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(rhov<0>(std::forward<Vars>(vars)...)),
      std::move(rhoE(std::forward<Vars>(vars)...))))
#endif
  {
    return make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(rhov<0>(std::forward<Vars>(vars)...)),
      std::move(rhoE(std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  State vector of primitive variables in 1d
   *
   *  @ingroup VariablesSecondary
   */
  template<std::size_t Tag, typename... Vars>
  static FDBB_INLINE auto constexpr primitive(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(v<0>(std::forward<Vars>(vars)...)),
      std::move(p(std::forward<Vars>(vars)...))))
#endif
  {
    return make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(v<0>(std::forward<Vars>(vars)...)),
      std::move(p(std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  Vector of characteristic variables in 1d
   *  Current implementation holds only for ideal gases!
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *
   *  @param[in] n_x  x component of vector n
   *
   *  @ingroup VariablesSecondary
   */
  template<std::size_t Tag, typename T, typename... Vars>
  static FDBB_INLINE auto constexpr characteristic(T&& n_x,
                                                   Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(v_dir(std::forward<T>(n_x), std::forward<Vars>(vars)...) -
                c(std::forward<Vars>(vars)...)),
      std::move(v_dir(std::forward<T>(n_x), std::forward<Vars>(vars)...)),
      std::move(v_dir(std::forward<T>(n_x), std::forward<Vars>(vars)...) +
                c(std::forward<Vars>(vars)...))))
#endif
  {
    return make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(v_dir(std::forward<T>(n_x), std::forward<Vars>(vars)...) -
                c(std::forward<Vars>(vars)...)),
      std::move(v_dir(std::forward<T>(n_x), std::forward<Vars>(vars)...)),
      std::move(v_dir(std::forward<T>(n_x), std::forward<Vars>(vars)...) +
                c(std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  Vector of Riemann invariants in 1d
   *  Current implementation holds only for ideal gases!
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *
   *  @param[in] n_x  x component of vector n
   *
   *  @ingroup VariablesSecondary
   */
  template<std::size_t Tag, typename T, typename... Vars>
  static FDBB_INLINE auto constexpr riemann(T&& n_x, Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(
        v_dir(std::forward<T>(n_x), std::forward<Vars>(vars)...) -
        fdbb::elem_div(
          fdbb::elem_mul(CONSTANT(2.0, rho(std::forward<Vars>(vars)...)),
                         c(std::forward<Vars>(vars)...)),
          eos::gamma - CONSTANT(1.0, rho(std::forward<Vars>(vars)...)))),
      std::move(fdbb::elem_mul(
        eos::cv,
        fdbb::elem_log(fdbb::elem_div(
          p(std::forward<Vars>(vars)...),
          fdbb::elem_pow(rho(std::forward<Vars>(vars)...), eos::gamma))))),
      std::move(
        v_dir(std::forward<T>(n_x), std::forward<Vars>(vars)...) +
        fdbb::elem_div(
          fdbb::elem_mul(CONSTANT(2.0, rho(std::forward<Vars>(vars)...)),
                         c(std::forward<Vars>(vars)...)),
          eos::gamma - CONSTANT(1.0, rho(std::forward<Vars>(vars)...))))))
#endif
  {
    return make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(
        v_dir(std::forward<T>(n_x), std::forward<Vars>(vars)...) -
        fdbb::elem_div(
          fdbb::elem_mul(CONSTANT(2.0, rho(std::forward<Vars>(vars)...)),
                         c(std::forward<Vars>(vars)...)),
          eos::gamma - CONSTANT(1.0, rho(std::forward<Vars>(vars)...)))),
      std::move(fdbb::elem_mul(
        eos::cv,
        fdbb::elem_log(fdbb::elem_div(
          p(std::forward<Vars>(vars)...),
          fdbb::elem_pow(rho(std::forward<Vars>(vars)...), eos::gamma))))),
      std::move(
        v_dir(std::forward<T>(n_x), std::forward<Vars>(vars)...) +
        fdbb::elem_div(
          fdbb::elem_mul(CONSTANT(2.0, rho(std::forward<Vars>(vars)...)),
                         c(std::forward<Vars>(vars)...)),
          eos::gamma - CONSTANT(1.0, rho(std::forward<Vars>(vars)...)))));
  }
};

/** @brief
 *  Specialization of the fdbbFluxes structure for conservative
 *  variables in 1d space dimensions and arbitrary equation of state
 *
 *  Inviscid fluxes
 *  \f[
 *      F = \begin{bmatrix}
 *            f_0\\
 *            f_1\\
 *            f_2
 *          \end{bmatrix} =
 *          \begin{bmatrix}
 *            \rho v_0\\
 *            \rho v_0^2 + p\\
 *            v_0 (\rho E + p)
 *          \end{bmatrix}.
 *  \f]
 *  where \f$ p \f$ is the pressure.
 *
 *  Viscous fluxes
 */
template<typename Var>
struct fdbbFluxes<Var, 1>
{
  /// @brief Variables
  using variables = Var;

  /// @brief Equation of state
  using eos = typename variables::eos;

  /// @brief Return output stream
  static std::ostream& print(std::ostream& os)
  {
    os << "Inviscid fluxes in 1d, ";
    variables::print(os);
    return os;
  }

  /// @brief Component \f$ f_0 = \rho v_0 \f$ of the inviscid flux in
  /// x-direction
  template<EnumCoord coord, index_t ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::x && ivar == 0,
      decltype(variables::template rhov<0>(std::forward<Vars>(vars)...))>::type
#endif
  {
    return variables::template rhov<0>(std::forward<Vars>(vars)...);
  }

  /// @brief Component \f$ f_1 = \rho v_0^2 + p \f$ of the inviscid
  /// flux in x-direction
  template<EnumCoord coord, index_t ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::x && ivar == 1 &&
        std::is_base_of<fdbbEOS_pVT, eos>::value,
      decltype(
        fdbb::elem_div(
          fdbb::elem_mul(
            variables::template rhov<0>(std::forward<Vars>(vars)...),
            variables::template rhov<0>(std::forward<Vars>(vars)...)),
          variables::template rho(std::forward<Vars>(vars)...)) +
        eos::p_rhoe(variables::template rho(std::forward<Vars>(vars)...),
                    variables::template e(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return fdbb::elem_div(
             fdbb::elem_mul(
               variables::template rhov<0>(std::forward<Vars>(vars)...),
               variables::template rhov<0>(std::forward<Vars>(vars)...)),
             variables::template rho(std::forward<Vars>(vars)...)) +
           eos::p_rhoe(variables::template rho(std::forward<Vars>(vars)...),
                       variables::template e(std::forward<Vars>(vars)...));
  }

  /// @brief Component \f$ f_2 = v_0 (\rho E + p) \f$ of the inviscid
  /// flux in x-direction
  template<EnumCoord coord, index_t ivar, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      coord == EnumCoord::x && ivar == 2 &&
        std::is_base_of<fdbbEOS_pVT, eos>::value,
      decltype(fdbb::elem_mul(
        variables::template v<0>(std::forward<Vars>(vars)...),
        (variables::template rhoE(std::forward<Vars>(vars)...) +
         eos::template p_rhoe(
           variables::template rho(std::forward<Vars>(vars)...),
           variables::template e(std::forward<Vars>(vars)...)))))>::type
#endif
  {
    return fdbb::elem_mul(
      variables::template v<0>(std::forward<Vars>(vars)...),
      (variables::template rhoE(std::forward<Vars>(vars)...) +
       eos::template p_rhoe(
         variables::template rho(std::forward<Vars>(vars)...),
         variables::template e(std::forward<Vars>(vars)...))));
  }

  /// @brief Vector of the inviscid fluxes in x-direction
  template<std::size_t Tag, typename... Vars>
  static FDBB_INLINE auto constexpr inviscid(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(inviscid<EnumCoord::x, 0>(std::forward<Vars>(vars)...)),
      std::move(inviscid<EnumCoord::x, 1>(std::forward<Vars>(vars)...)),
      std::move(inviscid<EnumCoord::x, 2>(std::forward<Vars>(vars)...))))

#endif
  {

    return make_fdbbBlockExpr<Tag, 3, 1>(
      std::move(inviscid<EnumCoord::x, 0>(std::forward<Vars>(vars)...)),
      std::move(inviscid<EnumCoord::x, 1>(std::forward<Vars>(vars)...)),
      std::move(inviscid<EnumCoord::x, 2>(std::forward<Vars>(vars)...)));
  }

  /** @brief Viscous fluxes
   *  @ingroup FluxesViscous
   */
};

/** @brief
 *  Specialization of the fdbbJacobians structure for conservative
 *  variables in 1d space dimensions and arbitrary equation of state
 */
template<typename Var>
struct fdbbJacobians<Var, 1>
{
  /// @brief Variables
  using variables = Var;

  /// @brief Equation of state
  using eos = typename variables::eos;

  /// @brief Return output stream
  static std::ostream& print(std::ostream& os)
  {
    os << "Inviscid flux-Jacobian in 1d, ";
    variables::print(os);
    return os;
  }
};

} // namespace fdbb

#endif // FDBB_CONSERVATIVE_1D_H
