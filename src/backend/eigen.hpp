/** @file eigen.hpp
 *
 *  @brief Implementation details for Eigen library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_EIGEN_HPP
#define BACKEND_EIGEN_HPP

#ifdef SUPPORT_EIGEN

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <type_traits>

namespace fdbb {

/** @brief
 *  If T is of type EnumETL::EIGEN, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::EIGEN,
                  typename std::enable_if<std::is_base_of<
                    Eigen::EigenBase<typename fdbb::remove_all<T>::type>,
                    typename fdbb::remove_all<T>::type>::value>::type>
  : public std::true_type
{
};

/// Detect Eigen base-type wrapped in G+Smo
template<typename T>
struct is_type_of<T,
                  EnumETL::EIGEN,
                  typename std::enable_if<std::is_base_of<
                    Eigen::EigenBase<typename fdbb::remove_all<T>::type::Base>,
                    typename fdbb::remove_all<T>::type::Base>::value>::type>
  : public std::true_type
{
};

/** @brief
 *  If T is of type EnumETL::EIGEN_MATRIX, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::EIGEN_MATRIX,
                  typename std::enable_if<std::is_base_of<
                    Eigen::MatrixBase<typename fdbb::remove_all<T>::type>,
                    typename fdbb::remove_all<T>::type>::value>::type>
  : public std::true_type
{
};

/// Detect Eigen matrix-type wrapped in G+Smo
template<typename T>
struct is_type_of<T,
                  EnumETL::EIGEN_MATRIX,
                  typename std::enable_if<std::is_base_of<
                    Eigen::MatrixBase<typename fdbb::remove_all<T>::type::Base>,
                    typename fdbb::remove_all<T>::type::Base>::value>::type>
  : public std::true_type
{
};

/** @brief
 *  If T is of type EnumETL::EIGEN_ARRAY, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::EIGEN_ARRAY,
                  typename std::enable_if<std::is_base_of<
                    Eigen::ArrayBase<typename fdbb::remove_all<T>::type>,
                    typename fdbb::remove_all<T>::type>::value>::type>
  : public std::true_type
{
};

/// Detect Eigen array-type wrapped in G+Smo
template<typename T>
struct is_type_of<T,
                  EnumETL::EIGEN_ARRAY,
                  typename std::enable_if<std::is_base_of<
                    Eigen::ArrayBase<typename fdbb::remove_all<T>::type::Base>,
                    typename fdbb::remove_all<T>::type::Base>::value>::type>
  : public std::true_type
{
};

/** @brief
 *  Result type of the expression (Eigen type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of<Expr, EnumETL::EIGEN>::type>
{
  using type = typename fdbb::remove_all<Expr>::type::PlainObject;
};

/** @brief
 *  Scalar value type of the expression (Eigen type)
 */
template<typename Expr>
struct value_type<Expr,
                  typename fdbb::enable_if_type_of<Expr, EnumETL::EIGEN>::type>
{
  using type = typename fdbb::remove_all<Expr>::type::Scalar;
};

namespace detail {

/** @brief
 *  Indicator for specialized Eigen implementation of
 *  fdbb::detail::make_temp_impl<Tag,Expr>(Expr&& expr) function
 */
template<typename Expr>
struct has_make_temp_impl<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::EIGEN>::type>
  : public std::true_type
{
};

/** @brief
 *  Eigen type creation from expressions
 */
template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr make_temp_impl(Expr&& expr) noexcept
#if !defined(DOXYGEN)
  ->
  typename fdbb::enable_if_type_of<Expr,
                                   EnumETL::EIGEN,
                                   typename fdbb::result_type<Expr>::type>::type
#endif
{
  using Temp = typename fdbb::result_type<Expr>::type;
  return Temp(std::forward<Expr>(expr));
}

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  fdbb::elem_mul<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_mul_impl<
  A,
  B,
  typename fdbb::enable_if_any_type_of<A, B, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{
};

/** @brief
 *  Element-wise multiplication of Eigen::Matrix types
 */
template<typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype((a.array() * b.array()).matrix())
#endif
  {
    return (a.array() * b.array()).matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  fdbb::elem_div<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_div_impl<
  A,
  B,
  typename fdbb::enable_if_any_type_of<A, B, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{
};

/** @brief
 *  Element-wise division of Eigen::Matrix types
 */
template<typename A, typename B>
struct elem_div_impl<A, B, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype((a.array() / b.array()).matrix())
#endif
  {
    return (a.array() / b.array()).matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  fdbb::elem_pow<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{
};

/** @brief
 *  Element-wise pow-function of Eigen::Matrix types
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(pow(a.array(), b).matrix())
#endif
  {
    return pow(a.array(), b).matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  fdbb::elem_fabs<A>(A&& a) function
 */
template<typename A>
struct get_elem_fabs_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{
};

/** @brief
 *  Element-wise absolute value of Eigen::Array type
 *
 *  @note
 *  Eigen does not distinguish between abs and fabs.
 */
template<typename A>
struct elem_fabs_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(abs(std::forward<A>(a)))
#endif
  {
    return abs(std::forward<A>(a));
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  fdbb::elem_fabs<A>(A&& a) function
 */
template<typename A>
struct get_elem_fabs_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{
};

/** @brief
 *  Element-wise absolute value of Eigen::Matrix type
 *
 *  @note
 *  Eigen does not distinguish between abs and fabs.
 */
template<typename A>
struct elem_fabs_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(abs(a.array()).matrix())
#endif
  {
    return abs(a.array()).matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  fdbb::elem_exp10<A>(A&& a) function
 */
template<typename A>
struct get_elem_exp10_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{
};

/** @brief
 *  Element-wise exp10(x) function for x of Eigen::Array type
 */
template<typename A>
struct elem_exp10_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      exp(std::forward<A>(a) *
          2.302585092994045684017991454684364207601101488628772976033))
#endif
  {
    return exp(std::forward<A>(a) *
               2.302585092994045684017991454684364207601101488628772976033);
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  fdbb::elem_exp10<A>(A&& a) function
 */
template<typename A>
struct get_elem_exp10_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{
};

/** @brief
 *  Element-wise exp10(x) function for x of Eigen::Matrix type
 */
template<typename A>
struct elem_exp10_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(exp(a.array() *
                    2.302585092994045684017991454684364207601101488628772976033)
                  .matrix())
#endif
  {
    return exp(a.array() *
               2.302585092994045684017991454684364207601101488628772976033)
      .matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  fdbb::elem_exp2<A>(A&& a) function
 */
template<typename A>
struct get_elem_exp2_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{
};

/** @brief
 *  Element-wise exp2(x) function for x of Eigen::Array type
 */
template<typename A>
struct elem_exp2_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      exp(std::forward<A>(a) *
          0.693147180559945309417232121458176568075500134360255254120))
#endif
  {
    return exp(std::forward<A>(a) *
               0.693147180559945309417232121458176568075500134360255254120);
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  fdbb::elem_exp2<A>(A&& a) function
 */
template<typename A>
struct get_elem_exp2_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{
};

/** @brief
 *  Element-wise exp2(x) function for x of Eigen::Matrix type
 */
template<typename A>
struct elem_exp2_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(exp(a.array() *
                    0.693147180559945309417232121458176568075500134360255254120)
                  .matrix())
#endif
  {
    return exp(a.array() *
               0.693147180559945309417232121458176568075500134360255254120)
      .matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  fdbb::elem_rsqrt<A>(A&& a) function
 */
template<typename A>
struct get_elem_rsqrt_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{
};

/** @brief
 *  Element-wise inverse square root of Eigen::Array type
 */
template<typename A>
struct elem_rsqrt_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(pow(std::forward<A>(a), -0.5))
#endif
  {
    return pow(std::forward<A>(a), -0.5);
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  fdbb::elem_rsqrt<A>(A&& a) function
 */
template<typename A>
struct get_elem_rsqrt_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{
};

/** @brief
 *  Element-wise inverse square root of Eigen::Matrix type
 */
template<typename A>
struct elem_rsqrt_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(pow(a.array(), -0.5).matrix())
#endif
  {
    return pow(a.array(), -0.5).matrix();
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  fdbb::elem_log2<A>(A&& a) function
 */
template<typename A>
struct get_elem_log2_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{
};

/** @brief
 *  Element-wise log2(x) function for x of Eigen::Array type
 */
template<typename A>
struct elem_log2_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(log(std::forward<A>(a)) *
                1.442695040888963407359924681001892137426645954152985934135)
#endif
  {
    return log(std::forward<A>(a)) *
           1.442695040888963407359924681001892137426645954152985934135;
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  fdbb::elem_log2<A>(A&& a) function
 */
template<typename A>
struct get_elem_log2_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{
};

/** @brief
 *  Element-wise log2(x) function for x of Eigen::Matrix type
 */
template<typename A>
struct elem_log2_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto constexpr eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> decltype(log(a.array()).matrix() *
                1.442695040888963407359924681001892137426645954152985934135)
#endif
  {
    return log(a.array()).matrix() *
           1.442695040888963407359924681001892137426645954152985934135;
  }
};

/// Helper macro for generating element-wise unary operations
#if !defined(DOXYGEN)
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief                                                                   \
   *  Selector for specialized Eigen::Matrix implementation of                 \
   *  fdbb::elem_##OPNAME##<A>(A&& a) function                                 \
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>          \
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>            \
  {                                                                            \
  };                                                                           \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for Eigen::Matrix types                     \
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::EIGEN_MATRIX>                        \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept                     \
      -> decltype(a.array().OPNAME().matrix())                                 \
    {                                                                          \
      return a.array().OPNAME().matrix();                                      \
    }                                                                          \
  };
#else
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief                                                                   \
   *  Selector for specialized Eigen::Matrix implementation of                 \
   *  fdbb::elem_##OPNAME##<A>(A&& a) function                                 \
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>          \
    : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>            \
  {                                                                            \
  };                                                                           \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for Eigen::Matrix types                     \
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::EIGEN_MATRIX>                        \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept                     \
    {                                                                          \
      return a.array().OPNAME().matrix();                                      \
    }                                                                          \
  };
#endif

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  fdbb::elem_conj<A>(A&& a) function
 */
template<typename A>
struct get_elem_conj_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{
};

/** @brief
 *  Element-wise complex conjugate function for Eigen::Array types
 */
template<typename A>
struct elem_conj_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto eval(A&& a) // constexpr and noexcept not possible
#if !defined(DOXYGEN)
    -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of conj function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  fdbb::elem_conj<A>(A&& a) function
 */
template<typename A>
struct get_elem_conj_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{
};

/** @brief
 *  Element-wise complex conjugate function for Eigen::Matrix types
 */
template<typename A>
struct elem_conj_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto eval(A&& a) // constexpr and noexcept not possible
#if !defined(DOXYGEN)
    -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of conj function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  fdbb::elem_imag<A>(A&& a) function
 */
template<typename A>
struct get_elem_imag_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{
};

/** @brief
 *  Element-wise imag function for Eigen::Array types
 */
template<typename A>
struct elem_imag_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto eval(A&& a) // constexpr and noexcept not possible
#if !defined(DOXYGEN)
    -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of imag function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  fdbb::elem_imag<A>(A&& a) function
 */
template<typename A>
struct get_elem_imag_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{
};

/** @brief
 *  Element-wise imag function for Eigen::Matrix types
 */
template<typename A>
struct elem_imag_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto eval(A&& a) // constexpr and noexcept not possible
#if !defined(DOXYGEN)
    -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of imag function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized Eigen::Array implementation of
 *  fdbb::elem_real<A>(A&& a) function
 */
template<typename A>
struct get_elem_real_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_ARRAY>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_ARRAY>
{
};

/** @brief
 *  Element-wise real function for Eigen::Array types
 */
template<typename A>
struct elem_real_impl<A, EnumETL::EIGEN_ARRAY>
{
  static FDBB_INLINE auto eval(A&& a) // constexpr and noexcept not possible
#if !defined(DOXYGEN)
    -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of real function available");
    return std::forward<A>(a);
  }
};

/** @brief
 *  Selector for specialized Eigen::Matrix implementation of
 *  fdbb::elem_real<A>(A&& a) function
 */
template<typename A>
struct get_elem_real_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::EIGEN_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::EIGEN_MATRIX>
{
};

/** @brief
 *  Element-wise real function for Eigen::Matrix types
 */
template<typename A>
struct elem_real_impl<A, EnumETL::EIGEN_MATRIX>
{
  static FDBB_INLINE auto eval(A&& a) // constexpr and noexcept not possible
#if !defined(DOXYGEN)
    -> decltype(std::forward<A>(a))
#endif
  {
    throw std::logic_error("No implementation of real function available");
    return std::forward<A>(a);
  }
};

} // namespace detail
} // namespace fdbb

#endif // SUPPORT_EIGEN
#endif // BACKEND_EIGEN_HPP
