/** @file blockexpression.hpp
 *
 *  @brief Implementation details for block expressions
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef BACKEND_BLOCK_EXPRESSIONS_HPP
#define BACKEND_BLOCK_EXPRESSIONS_HPP

#include <tuple>
#include <type_traits>

#include "fdbbBlockExpressions.hpp"

namespace fdbb {

/// The block expression backend makes use of other backends via
/// high-level methods. Forward declarations of these methods are
/// given below.

/// Forward declaration
template<typename TC, typename T, typename Expr>
static FDBB_INLINE auto constexpr make_constant(const T value,
                                                Expr&& expr) noexcept ->
  typename std::enable_if<detail::has_make_constant_impl<Expr>::value,
                          decltype(detail::make_constant_impl<TC>(value,
                                                                  expr))>::type;

/// Forward declaration
template<typename TC, typename T, typename Expr>
static FDBB_INLINE auto constexpr make_constant(const T value,
                                                Expr&& expr) noexcept ->
  typename std::enable_if<!detail::has_make_constant_impl<Expr>::value,
                          typename fdbb::value_type<Expr>::type>::type;

/// Forward declaration
template<std::size_t Tag, typename Temp, typename Expr>
static FDBB_INLINE auto constexpr make_temp(Expr&& expr) noexcept ->
  typename std::enable_if<!std::is_same<Temp, Expr>::value,
                          decltype(Temp(std::forward<Expr>(expr)))>::type;

/// Forward declaration
template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr make_temp(Expr&& expr) noexcept ->
  typename std::enable_if<detail::has_make_temp_impl<Expr>::value,
                          decltype(detail::make_temp_impl<Tag, Expr>(
                            std::forward<Expr>(expr)))>::type;

/// Forward declaration
template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr make_temp(Expr&& expr) noexcept ->
  typename std::enable_if<!detail::has_make_temp_impl<Expr>::value,
                          decltype(std::forward<Expr>(expr))>::type;

/// Forward declaration
template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr tag(Expr&& expr) noexcept ->
  typename std::enable_if<
    detail::has_tag_impl<Expr>::value,
    decltype(detail::tag_impl<Tag, Expr>(std::forward<Expr>(expr)))>::type;

/// Forward declaration
template<std::size_t Tag, typename Expr>
static FDBB_INLINE auto constexpr tag(Expr&& expr) noexcept ->
  typename std::enable_if<!detail::has_tag_impl<Expr>::value,
                          decltype(std::forward<Expr>(expr))>::type;

/// Forward declaration
#define FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  template<typename A, typename B>                                             \
  auto constexpr elem_##OPNAME(A&& a, B&& b) noexcept->decltype(               \
    detail::elem_##OPNAME##_impl<                                              \
      A,                                                                       \
      B,                                                                       \
      detail::get_elem_##OPNAME##_impl<A, B>::value>::eval(std::forward<A>(a), \
                                                           std::forward<B>(    \
                                                             b)));

FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow)

#undef FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/// Forward declaration
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  template<typename A>                                                         \
  auto constexpr elem_##OPNAME(A&& a) noexcept->decltype(                      \
    detail::elem_##OPNAME##_impl<                                              \
      A,                                                                       \
      detail::get_elem_##OPNAME##_impl<A>::value>::eval(std::forward<A>(a)));

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
 *  If T is of type EnumETL::BLOCKEXPR, provides the member constant
 * value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::BLOCKEXPR,
                  typename std::enable_if<std::is_base_of<
                    fdbb::fdbbBlockBase,
                    typename fdbb::remove_all<T>::type>::value>::type>
  : public std::true_type
{
};

/** @brief
 *  Result type of the expression (BLOCKEXPR type)
 */
template<typename Expr>
struct result_type<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::BLOCKEXPR>::type>
{
  using type = typename fdbb::remove_all<Expr>::type::result_type;
};

/** @brief
 *  Scalar value type of the expression (BLOCKEXPR type)
 */
template<typename Expr>
struct value_type<
  Expr,
  typename fdbb::enable_if_type_of<Expr, EnumETL::BLOCKEXPR>::type>
{
  using type =
    typename fdbb::value_type<typename fdbb::result_type<Expr>::type>::type;
};

namespace detail {

/** @brief
 *  Selector for specialized BLOCKEXPR implementation of
 *  fdbb::elem_mul<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_mul_impl<
  A,
  B,
  typename fdbb::enable_if_any_type_of<A, B, EnumETL::BLOCKEXPR>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLOCKEXPR>
{
};

/// @brief Functor for element-wise multiplication of two objects
struct __elem_mul
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  auto constexpr operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>) const
    noexcept -> decltype(fdbb::elem_mul(t0, t1))
  {
    return fdbb::elem_mul(t0, t1);
  }
};

/** @brief
 *  Element-wise multiplication of BLOCKEXPR types
 */
template<typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::BLOCKEXPR>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<
                utils::hash<std::size_t>('m',
                                         'u',
                                         'l',
                                         fdbb::remove_all<A>::type::tag,
                                         fdbb::remove_all<B>::type::tag),
                fdbb::remove_all<A>::type::rows,
                fdbb::remove_all<A>::type::cols>(
      utils::for_each_return(a, b, __elem_mul())))
#endif
  {
    return make_fdbbBlockExpr<utils::hash<std::size_t>(
                                'm',
                                'u',
                                'l',
                                fdbb::remove_all<A>::type::tag,
                                fdbb::remove_all<B>::type::tag),
                              fdbb::remove_all<A>::type::rows,
                              fdbb::remove_all<A>::type::cols>(
      utils::for_each_return(a, b, __elem_mul()));
  }
};

/** @brief
 *  Selector for specialized BLOCKEXPR implementation of
 *  fdbb::elem_div<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_div_impl<
  A,
  B,
  typename fdbb::enable_if_any_type_of<A, B, EnumETL::BLOCKEXPR>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLOCKEXPR>
{
};

/// @brief Functor for element-wise division of two objects
struct __elem_div
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  auto constexpr operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>) const
    noexcept -> decltype(fdbb::elem_div(t0, t1))
  {
    return fdbb::elem_div(t0, t1);
  }
};

/** @brief
 *  Element-wise division of BLOCKEXPR types
 */
template<typename A, typename B>
struct elem_div_impl<A, B, EnumETL::BLOCKEXPR>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<
                utils::hash<std::size_t>('d',
                                         'i',
                                         'v',
                                         fdbb::remove_all<A>::type::tag,
                                         fdbb::remove_all<B>::type::tag),
                fdbb::remove_all<A>::type::rows,
                fdbb::remove_all<A>::type::cols>(
      utils::for_each_return(a, b, __elem_div())))
#endif
  {
    return make_fdbbBlockExpr<utils::hash<std::size_t>(
                                'd',
                                'i',
                                'v',
                                fdbb::remove_all<A>::type::tag,
                                fdbb::remove_all<B>::type::tag),
                              fdbb::remove_all<A>::type::rows,
                              fdbb::remove_all<A>::type::cols>(
      utils::for_each_return(a, b, __elem_div()));
  }
};

/** @brief
 *  Selector for specialized BLOCKEXPR implementation of
 *  fdbb::elem_pow<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_type_of<A, EnumETL::BLOCKEXPR>::type>
  : public std::integral_constant<EnumETL, EnumETL::BLOCKEXPR>
{
};

/// @brief Functor for element-wise pow-function of two objects
struct __elem_pow
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  auto constexpr operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>) const
    noexcept -> decltype(fdbb::elem_pow(t0, t1))
  {
    return fdbb::elem_pow(t0, t1);
  }
};

/** @brief
 *  Element-wise pow-function of BLOCKEXPR types
 *  @{
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::BLOCKEXPR>
{
  template<typename _B = B>
  static FDBB_INLINE auto constexpr eval(A&& a, _B&& b) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_arithmetic<_B>::value,
      decltype(make_fdbbBlockExpr<
               utils::hash<std::size_t>('p',
                                        'o',
                                        'w',
                                        fdbb::remove_all<A>::type::tag,
                                        862448381),
               fdbb::remove_all<A>::type::rows,
               fdbb::remove_all<A>::type::cols>(
        utils::for_each_return<0, 1, 0, 0>(a,
                                           std::make_tuple(b),
                                           __elem_pow())))>::type
#endif
  {
    return make_fdbbBlockExpr<
      utils::hash<std::size_t>(
        'p', 'o', 'w', fdbb::remove_all<A>::type::tag, 862448381),
      fdbb::remove_all<A>::type::rows,
      fdbb::remove_all<A>::type::cols>(
      utils::for_each_return<0, 1, 0, 0>(a, std::make_tuple(b), __elem_pow()));
  }

  template<typename _B = B>
  static FDBB_INLINE auto constexpr eval(A&& a, _B&& b) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      !std::is_arithmetic<_B>::value,
      decltype(make_fdbbBlockExpr<
               utils::hash<std::size_t>('p',
                                        'o',
                                        'w',
                                        fdbb::remove_all<A>::type::tag,
                                        fdbb::remove_all<_B>::type::tag),
               fdbb::remove_all<A>::type::rows,
               fdbb::remove_all<A>::type::cols>(
        utils::for_each_return(a, b, __elem_pow())))>::type
#endif
  {
    return make_fdbbBlockExpr<utils::hash<std::size_t>(
                                'p',
                                'o',
                                'w',
                                fdbb::remove_all<A>::type::tag,
                                fdbb::remove_all<_B>::type::tag),
                              fdbb::remove_all<A>::type::rows,
                              fdbb::remove_all<A>::type::cols>(
      utils::for_each_return(a, b, __elem_pow()));
  }
};
/** @} */

#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME, ...)            \
  /** @brief                                                                   \
   *  Selector for specialized BLOCKEXPR implementation of                     \
   *  fdbb::elem_##OPNAME##<A>(A&& a) function                                 \
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::BLOCKEXPR>::type>             \
    : public std::integral_constant<EnumETL, EnumETL::BLOCKEXPR>               \
  {                                                                            \
  };                                                                           \
                                                                               \
  /** @brief                                                                   \
   *  Functor for element-wise OPNAME function for BLOCKEXPR types             \
   */                                                                          \
  struct __elem_##OPNAME                                                       \
  {                                                                            \
    template<typename T, std::size_t I>                                        \
    auto constexpr operator()(const T& t,                                      \
                              std::integral_constant<std::size_t, I>) const    \
      noexcept -> decltype(fdbb::elem_##OPNAME(t))                             \
    {                                                                          \
      return fdbb::elem_##OPNAME(t);                                           \
    }                                                                          \
  };                                                                           \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for BLOCKEXPR types                         \
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::BLOCKEXPR>                           \
  {                                                                            \
    static FDBB_INLINE auto constexpr eval(A&& a) noexcept -> decltype(        \
      make_fdbbBlockExpr<                                                      \
        utils::hash<std::size_t>(__VA_ARGS__, fdbb::remove_all<A>::type::tag), \
        fdbb::remove_all<A>::type::rows,                                       \
        fdbb::remove_all<A>::type::cols>(                                      \
        utils::for_each_return(a, __elem_##OPNAME())))                         \
                                                                               \
    {                                                                          \
      return make_fdbbBlockExpr<                                               \
        utils::hash<std::size_t>(__VA_ARGS__, fdbb::remove_all<A>::type::tag), \
        fdbb::remove_all<A>::type::rows,                                       \
        fdbb::remove_all<A>::type::cols>(                                      \
        utils::for_each_return(a, __elem_##OPNAME()));                         \
    }                                                                          \
  };

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs, 'a', 'b', 's')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos, 'a', 'c', 'o', 's')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh, 'a', 'c', 'o', 's', 'h')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin, 'a', 's', 'i', 'n')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh, 'a', 's', 'i', 'n', 'h')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan, 'a', 't', 'a', 'n')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh, 'a', 't', 'a', 'n', 'h')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil, 'c', 'e', 'i', 'l')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj, 'c', 'o', 'n', 'j')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos, 'c', 'o', 's')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh, 'c', 'o', 's', 'h')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf, 'e', 'r', 'f')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc, 'e', 'r', 'f', 'c')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp, 'e', 'x', 'p')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10, 'e', 'x', 'p', '1', '0')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2, 'e', 'x', 'p', '2')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs, 'f', 'a', 'b', 's')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor, 'f', 'l', 'o', 'o', 'r')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag, 'i', 'm', 'a', 'g')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log, 'l', 'o', 'g')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10, 'l', 'o', 'g', '1', '0')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2, 'l', 'o', 'g', '2')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real, 'r', 'e', 'a', 'l')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round, 'r', 'o', 'u', 'n', 'd')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt, 'r', 's', 'q', 'r', 't')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign, 's', 'i', 'g', 'n')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin, 's', 'i', 'n')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh, 's', 'i', 'n', 'h')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt, 's', 'q', 'r', 't')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan, 't', 'a', 'n')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh, 't', 'a', 'n', 'h')
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc, 't', 'r', 'u', 'n', 'c')

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

} // namespace detail
} // namespace fdbb

#endif // BACKEND_BLOCKEXPR_HPP
