/** @file itpp.hpp
 *
 *  @brief Implementation details for IT++ library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 *
 *  @note
 *  IT++ does not require a specialization for make_temp since the
 *  result of any vector/matrix expression is automatically stored as
 *  vector/matrix. Consider the example code:
 *  \code{.cpp}
 *  itpp::Vec<double> v1(10),v2(10);
 *  auto w = v1+v2;
 *  \endcode
 *  The type of \c w is \c itpp::Vec<double>.
 */
#pragma once
#ifndef BACKEND_ITPP_HPP
#define BACKEND_ITPP_HPP

#ifdef SUPPORT_ITPP

#include <itpp/itbase.h>
#include <type_traits>

namespace fdbb {

/** @brief
 *  If T is of type EnumETL::ITPP_VECTOR, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::ITPP_VECTOR,
                  typename std::enable_if<std::is_base_of<
                    itpp::Vec<typename fdbb::remove_all<T>::type::value_type>,
                    typename fdbb::remove_all<T>::type>::value>::type>
  : public std::true_type
{
};

/** @brief
 *  If T is of type EnumETL::ITPP_MATRIX, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<T,
                  EnumETL::ITPP_MATRIX,
                  typename std::enable_if<std::is_base_of<
                    itpp::Mat<typename fdbb::remove_all<T>::type::value_type>,
                    typename fdbb::remove_all<T>::type>::value>::type>
  : public std::true_type
{
};

/** @brief
 *  If T is of type EnumETL::ITPP, provides the member constant
 *  value equal to true. Otherwise value is false.
 */
template<typename T>
struct is_type_of<
  T,
  EnumETL::ITPP,
  typename std::enable_if<
    std::is_base_of<itpp::Vec<typename fdbb::remove_all<T>::type::value_type>,
                    typename fdbb::remove_all<T>::type>::value ||
    std::is_base_of<itpp::Mat<typename fdbb::remove_all<T>::type::value_type>,
                    typename fdbb::remove_all<T>::type>::value>::type>
  : public std::true_type
{
};

/** @brief
 *  Result type of the expression (IT++ type)
 */
template<typename Expr>
struct result_type<Expr,
                   typename fdbb::enable_if_type_of<Expr, EnumETL::ITPP>::type>
{
  using type = typename fdbb::remove_all<Expr>::type;
};

/** @brief
 *  Scalar value type of the expression (IT++ type)
 */
template<typename Expr>
struct value_type<Expr,
                  typename fdbb::enable_if_type_of<Expr, EnumETL::ITPP>::type>
{
  using type = typename fdbb::remove_all<Expr>::type::value_type;
};

namespace detail {

/** @brief
 *  Selector for specialized IT++ implementation of
 *  fdbb::elem_mul<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_mul_impl<
  A,
  B,
  typename std::enable_if<std::is_same<
    decltype(itpp::elem_mult(std::declval<A>(), std::declval<B>())),
    decltype(itpp::elem_mult(std::declval<A>(), std::declval<B>()))>::value>::
    type> : public std::integral_constant<EnumETL, EnumETL::ITPP>
{
};

/** @brief
 *  Element-wise multiplication of IT++ types
 */
template<typename A, typename B>
struct elem_mul_impl<A, B, EnumETL::ITPP>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(itpp::elem_mult(std::forward<A>(a), std::forward<B>(b)))
#endif
  {
    return itpp::elem_mult(std::forward<A>(a), std::forward<B>(b));
  }
};

/** @brief
 *  Selector for specialized IT++ implementation of
 *  fdbb::elem_div<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_div_impl<
  A,
  B,
  typename std::enable_if<
    std::is_same<decltype(itpp::elem_div(std::declval<A>(), std::declval<B>())),
                 decltype(itpp::elem_div(std::declval<A>(),
                                         std::declval<B>()))>::value>::type>
  : public std::integral_constant<EnumETL, EnumETL::ITPP>
{
};

/** @brief
 *  Element-wise division of IT++ types
 */
template<typename A, typename B>
struct elem_div_impl<A, B, EnumETL::ITPP>
{
  static FDBB_INLINE auto constexpr eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> decltype(itpp::elem_div(std::forward<A>(a), std::forward<B>(b)))
#endif
  {
    return itpp::elem_div(std::forward<A>(a), std::forward<B>(b));
  }
};

/// Helper macro for generating element-wise unary operations
#if !defined(DOXYGEN)
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief                                                                   \
   *  Selector for specialized IT++ vector implementation of                   \
   *  fdbb::elem_##OPNAME##<A>(A&& a) function                                 \
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::ITPP_VECTOR>::type>           \
    : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR>             \
  {                                                                            \
  };                                                                           \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for IT++ vector types                       \
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::ITPP_VECTOR>                         \
  {                                                                            \
    static FDBB_INLINE auto eval(A&& a) noexcept ->                            \
      typename fdbb::remove_all<A>::type                                       \
    {                                                                          \
      typename fdbb::remove_all<A>::type temp(a.length());                     \
      _Pragma(                                                                 \
        "omp parallel for shared(temp)") for (auto i = 0; i < a.length(); i++) \
        temp[i] = std::OPNAME(a(i));                                           \
      return temp;                                                             \
    }                                                                          \
  };                                                                           \
                                                                               \
  /** @brief                                                                   \
   *  Selector for specialized IT++ matrix implementation of                   \
   *  fdbb::elem_##OPNAME##<A>(A&& a) function                                 \
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::ITPP_MATRIX>::type>           \
    : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX>             \
  {                                                                            \
  };                                                                           \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for IT++ matrix types                       \
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::ITPP_MATRIX>                         \
  {                                                                            \
    static FDBB_INLINE auto eval(A&& a) noexcept ->                            \
      typename fdbb::remove_all<A>::type                                       \
    {                                                                          \
      typename fdbb::remove_all<A>::type temp(a.rows(), a.cols());             \
      _Pragma(                                                                 \
        "omp parallel for shared(temp) collapse(2)") for (auto i = 0;          \
                                                          i < a.rows();        \
                                                          i++) for (auto j =   \
                                                                      0;       \
                                                                    j <        \
                                                                    a.cols();  \
                                                                    j++)       \
        temp[i, j] = std::OPNAME(a(i, j));                                     \
      return temp;                                                             \
    }                                                                          \
  };
#else
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  /** @brief                                                                   \
   *  Selector for specialized IT++ vector implementation of                   \
   *  fdbb::elem_##OPNAME##<A>(A&& a) function                                 \
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::ITPP_VECTOR>::type>           \
    : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR>             \
  {                                                                            \
  };                                                                           \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for IT++ vector types                       \
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::ITPP_VECTOR>                         \
  {                                                                            \
    static FDBB_INLINE auto eval(A&& a) noexcept                               \
    {                                                                          \
      typename fdbb::remove_all<A>::type temp(a.length());                     \
      _Pragma(                                                                 \
        "omp parallel for shared(temp)") for (auto i = 0; i < a.length(); i++) \
        temp[i] = std::OPNAME(a(i));                                           \
      return temp;                                                             \
    }                                                                          \
  };                                                                           \
                                                                               \
  /** @brief                                                                   \
   *  Selector for specialized IT++ matrix implementation of                   \
   *  fdbb::elem_##OPNAME##<A>(A&& a) function                                 \
   */                                                                          \
  template<typename A>                                                         \
  struct get_elem_##OPNAME##_impl<                                             \
    A,                                                                         \
    typename fdbb::enable_if_type_of<A, EnumETL::ITPP_MATRIX>::type>           \
    : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX>             \
  {                                                                            \
  };                                                                           \
                                                                               \
  /** @brief                                                                   \
   *  Element-wise OPNAME function for IT++ matrix types                       \
   */                                                                          \
  template<typename A>                                                         \
  struct elem_##OPNAME##_impl<A, EnumETL::ITPP_MATRIX>                         \
  {                                                                            \
    static FDBB_INLINE auto eval(A&& a) noexcept                               \
    {                                                                          \
      typename fdbb::remove_all<A>::type temp(a.rows(), a.cols());             \
      _Pragma(                                                                 \
        "omp parallel for shared(temp) collapse(2)") for (auto i = 0;          \
                                                          i < a.rows();        \
                                                          i++) for (auto j =   \
                                                                      0;       \
                                                                    j <        \
                                                                    a.cols();  \
                                                                    j++)       \
        temp[i, j] = std::OPNAME(a(i, j));                                     \
      return temp;                                                             \
    }                                                                          \
  };
#endif

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
// FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
 *  Selector for specialized IT++ vector implementation of
 *  fdbb::elem_exp10<A>(A&& a) function
 */
template<typename A>
struct get_elem_exp10_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ITPP_VECTOR>::type>
  : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR>
{
};

/** @brief
 *  Element-wise exp10(x) function for x of IT++ vector types
 */
template<typename A>
struct elem_exp10_impl<A, EnumETL::ITPP_VECTOR>
{
  static FDBB_INLINE auto eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::remove_all<A>::type
#endif
  {
    typename fdbb::remove_all<A>::type temp(a.length());
#pragma omp parallel for shared(temp)
    for (auto i = 0; i < a.length(); i++)
      temp[i] = std::exp(
        a(i) * 2.302585092994045684017991454684364207601101488628772976033);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ matrix implementation of
 *  fdbb::elem_exp10<A>(A&& a) function
 */
template<typename A>
struct get_elem_exp10_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ITPP_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX>
{
};

/** @brief
 *  Element-wise exp10(x) function for x of IT++ matrix types
 */
template<typename A>
struct elem_exp10_impl<A, EnumETL::ITPP_MATRIX>
{
  static FDBB_INLINE auto eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::remove_all<A>::type
#endif
  {
    typename fdbb::remove_all<A>::type temp(a.rows(), a.cols());
#pragma omp parallel for shared(temp) collapse(2)
    for (auto i = 0; i < a.rows(); i++)
      for (auto j = 0; j < a.cols(); j++)
        temp[i, j] = std::exp(
          a(i) * 2.302585092994045684017991454684364207601101488628772976033);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ vector implementation of
 *  fdbb::elem_sign<A>(A&& a) function
 */
template<typename A>
struct get_elem_sign_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ITPP_VECTOR>::type>
  : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR>
{
};

/** @brief
 *  Element-wise sign(x) function for x of IT++ vector types
 */
template<typename A>
struct elem_sign_impl<A, EnumETL::ITPP_VECTOR>
{
  static FDBB_INLINE auto eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::remove_all<A>::type
#endif
  {
    typename fdbb::remove_all<A>::type temp(a.length());
#pragma omp parallel for shared(temp)
    for (auto i = 0; i < a.length(); i++)
      temp[i] = (a(i) > 0.0 ? 1.0 : a(i) < 0.0 ? -1.0 : 0.0);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ matrix implementation of
 *  fdbb::elem_sign<A>(A&& a) function
 */
template<typename A>
struct get_elem_sign_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ITPP_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX>
{
};

/** @brief
 *  Element-wise sign(x) function for x of IT++ matrix types
 */
template<typename A>
struct elem_sign_impl<A, EnumETL::ITPP_MATRIX>
{
  static FDBB_INLINE auto eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::remove_all<A>::type
#endif
  {
    typename fdbb::remove_all<A>::type temp(a.rows(), a.cols());
#pragma omp parallel for shared(temp) collapse(2)
    for (auto i = 0; i < a.rows(); i++)
      for (auto j = 0; j < a.cols(); j++)
        temp[i, j] = (a(i, j) > 0.0 ? 1.0 : a(i, j) < 0.0 ? -1.0 : 0.0);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ vector implementation of
 *  fdbb::elem_conj<A>(A&& a) function
 */
template<typename A>
struct get_elem_conj_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ITPP_VECTOR>::type>
  : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR>
{
};

/** @brief
 *  Element-wise complex conjugate of IT++ vector types
 */
template<typename A>
struct elem_conj_impl<A, EnumETL::ITPP_VECTOR>
{
  static FDBB_INLINE auto eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::remove_all<A>::type
#endif
  {
    typename fdbb::remove_all<A>::type temp(a.length());
#pragma omp parallel for shared(temp)
    for (auto i = 0; i < a.length(); i++)
      temp[i] =
        fdbb::utils::to_type<typename fdbb::remove_all<A>::type::value_type>(
          std::conj(a(i)));
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ matrix implementation of
 *  fdbb::elem_conj<A>(A&& a) function
 */
template<typename A>
struct get_elem_conj_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ITPP_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX>
{
};

/** @brief
 *  Element-wise complex conjugate of IT++ matrix types
 */
template<typename A>
struct elem_conj_impl<A, EnumETL::ITPP_MATRIX>
{
  static FDBB_INLINE auto eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::remove_all<A>::type
#endif
  {
    typename fdbb::remove_all<A>::type temp(a.rows(), a.cols());
#pragma omp parallel for shared(temp) collapse(2)
    for (auto i = 0; i < a.rows(); i++)
      for (auto j = 0; j < a.cols(); j++)
        temp[i, j] =
          fdbb::utils::to_type<typename fdbb::remove_all<A>::type::value_type>(
            std::conj(a(i, j)));
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ vector implementation of
 *  fdbb::elem_rsqrt<A>(A&& a) function
 */
template<typename A>
struct get_elem_rsqrt_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ITPP_VECTOR>::type>
  : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR>
{
};

/** @brief
 *  Element-wise rsqrt(x) function for x of IT++ vector types
 */
template<typename A>
struct elem_rsqrt_impl<A, EnumETL::ITPP_VECTOR>
{
  static FDBB_INLINE auto eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::remove_all<A>::type
#endif
  {
    typename fdbb::remove_all<A>::type temp(a.length());
#pragma omp parallel for shared(temp)
    for (auto i = 0; i < a.length(); i++)
      temp[i] = std::pow(a(i), -0.5);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ matrix implementation of
 *  fdbb::elem_rsqrt<A>(A&& a) function
 */
template<typename A>
struct get_elem_rsqrt_impl<
  A,
  typename fdbb::enable_if_type_of<A, EnumETL::ITPP_MATRIX>::type>
  : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX>
{
};

/** @brief
 *  Element-wise rsqrt(x) function for x of IT++ matrix types
 */
template<typename A>
struct elem_rsqrt_impl<A, EnumETL::ITPP_MATRIX>
{
  static FDBB_INLINE auto eval(A&& a) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::remove_all<A>::type
#endif
  {
    typename fdbb::remove_all<A>::type temp(a.rows(), a.cols());
#pragma omp parallel for shared(temp) collapse(2)
    for (auto i = 0; i < a.rows(); i++)
      for (auto j = 0; j < a.cols(); j++)
        temp[i, j] = std::pow(a(i, j), -0.5);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ vector implementation of
 *  fdbb::elem_pow<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_type_of_and_cond<A,
                                            EnumETL::ITPP_VECTOR,
                                            std::is_arithmetic<B>::value>::type>
  : public std::integral_constant<EnumETL, EnumETL::ITPP_VECTOR>
{
};

/** @brief
 *  Element-wise pow(x,y) function for x of IT++ vector types
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::ITPP_VECTOR>
{
  static FDBB_INLINE auto eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::remove_all<A>::type
#endif
  {
    typename fdbb::remove_all<A>::type temp(a.length());
#pragma omp parallel for shared(temp)
    for (auto i = 0; i < a.length(); i++)
      temp[i] = std::pow(a(i), b);
    return temp;
  }
};

/** @brief
 *  Selector for specialized IT++ matrix implementation of
 *  fdbb::elem_pow<A,B>(A&& a, B&& b) function
 */
template<typename A, typename B>
struct get_elem_pow_impl<
  A,
  B,
  typename fdbb::enable_if_type_of_and_cond<A,
                                            EnumETL::ITPP_MATRIX,
                                            std::is_arithmetic<B>::value>::type>
  : public std::integral_constant<EnumETL, EnumETL::ITPP_MATRIX>
{
};

/** @brief
 *  Element-wise pow(x,y) function for x of IT++ matrix types
 */
template<typename A, typename B>
struct elem_pow_impl<A, B, EnumETL::ITPP_MATRIX>
{
  static FDBB_INLINE auto eval(A&& a, B&& b) noexcept
#if !defined(DOXYGEN)
    -> typename fdbb::remove_all<A>::type
#endif
  {
    typename fdbb::remove_all<A>::type temp(a.rows(), a.cols());
#pragma omp parallel for shared(temp) collapse(2)
    for (auto i = 0; i < a.rows(); i++)
      for (auto j = 0; j < a.cols(); j++)
        temp[i, j] = std::pow(a(i, j), b);
    return temp;
  }
};

} // namespace detail
} // namespace fdbb

#endif // SUPPORT_ITPP
#endif // BACKEND_ITPP_HPP
