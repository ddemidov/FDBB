/** @file fdbb.h
 *
 *  @brief Main header to be included by clients using the FDBB library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author: Matthias Moller
 */
#pragma once
#ifndef FDBB_H
#define FDBB_H

/* ------------------- Configuration ---------- */
#include <fdbbConfig.hpp>

/*  ------------------ Debug ------------------ */
#include <fdbbDebug.hpp>

/* ----------- Forward declarations ----------- */
#include <fdbbCacheForward.hpp>
#include <fdbbBlockExpressionsForward.hpp>

/* ------------------- Core ------------------- */
#include <fdbbCache.hpp>
#include <fdbbBlockExpressions.hpp>
#include <fdbbCompatibility.hpp>
#include <fdbbConstant.hpp>
#include <fdbbEnums.hpp>
#include <fdbbTraits.hpp>
#include <fdbbTypes.hpp>
#include <fdbbUtils.hpp>

/*  ------------------ Equation of states ----- */
#include <fdbbEOS.hpp>

/* ---- Conservative variable formulations ---- */
#include <fdbbConservative1d.hpp>
#include <fdbbConservative2d.hpp>
#include <fdbbConservative3d.hpp>

/* ------- Entropy variable formulations ------ */
//#include <fdbbEntropy1d.hpp>
//#include <fdbbEntropy2d.hpp>
//#include <fdbbEntropy3d.hpp>

/* ------ Primitive variable formulations ----- */
#include <fdbbPrimitive1d.hpp>
#include <fdbbPrimitive2d.hpp>
#include <fdbbPrimitive3d.hpp>

/* --- Riemann invariants formulations --- */
#include <fdbbRiemannInvariants1d.hpp>
#include <fdbbRiemannInvariants2d.hpp>
#include <fdbbRiemannInvariants3d.hpp>

#endif // FDBB_H
