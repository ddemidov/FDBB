/** @file fdbbCache.hpp
 *
 *  @brief Cache class
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_CACHE_HPP
#define FDBB_CACHE_HPP

#include <memory>

#include "fdbbCacheForward.hpp"
#include "fdbbCompatibility.hpp"
#include "fdbbConstant.hpp"
#include "fdbbUtils.hpp"

namespace fdbb {

/** @namespace fdbb::cache
 *
 *  @brief
 *  The \ref fdbb::cache namespace, containing the standard caching
 *  functionality of the FDBB library
 *
 *  The \ref fdbb::cache namespace contains an implementation of a
 *  caching strategy for the FDBB library. The caching strategy is
 *  implemented as expression template wrapper library. A lightweight
 *  object is created for each operator of the expression tree storing
 *  references to the argument(s). In a later implementation,
 *  mechanisms to optimize the expression tree to reduce redundant
 *  computation of terms might be included.
 *
 *  @note This Source Code Form is subject to the terms of the Mozilla
 *  Public License, v. 2.0. If a copy of the MPL was not distributed
 *  with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace cache {

/** @brief
 *  A cache object storing an object of type \ref Expr including
 *  ownership of that type
 *
 *  A cache object stores an object of type \ref Expr and has
 *  ownership of that type. The type must be created externally and
 *  passed to the cache object via its move constructor.
 */
template<std::size_t Tag, typename Expr>
struct fdbbExpr : public fdbbExprBase
{
private:
  /// @brief Self type
  using self_type = fdbbExpr<Tag, Expr>;

  /// @brief Expression type
  using expr_type = typename fdbb::remove_all<Expr>::type;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  fdbbExpr() = default;

  /// @brief Constructor (copy from expression)
  explicit fdbbExpr(const Expr& expr)
    : expr(expr)
  {
  }

  /// @brief Constructor (move from expression)
  explicit fdbbExpr(Expr&& expr)
    : expr(expr)
  {
  }

  /// @brief Constructor (evaluate from cache object)
  template<typename T,
           typename = typename std::enable_if<
             std::is_base_of<fdbbExprBase, T>::value>::type>
  explicit fdbbExpr(const T& obj)
    : expr(obj.get())
  {
  }

  /// @brief Constructor (evaluate from cache object)
  template<typename T,
           typename = typename std::enable_if<
             std::is_base_of<fdbbExprBase, T>::value>::type>
  explicit fdbbExpr(T&& obj)
    : expr(obj.get())
  {
  }

  /// @brief Returns constant reference to raw expression data
  constexpr expr_type& get() const { return expr; }

  /// @brief Returns reference to raw expression data
  expr_type& get() { return expr; }

  /// @brief Returns pointer to raw expression data
  expr_type* operator->() { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const
  {
    os << "expr<" << Tag << ">(" << expr << ")";
  }
};

/// @brief Output to string
template<std::size_t Tag, typename Expr>
std::ostream&
operator<<(std::ostream& os, fdbbExpr<Tag, Expr> expr)
{
  expr.print(os);
  return os;
}

/** @brief
 *  A view on a cache object storing a reference to an object of type
 *  \ref Expr excluding ownership of that object
 *
 *  A view on a cache object stores a reference to an object of type
 *  \ref Expr but does not have ownership of that type. The object is
 *  managed externally. The view just serves as a lughtweight hull.
 */
template<std::size_t Tag, typename Expr>
struct fdbbExprView : public fdbbExprBase
{
private:
  /// @brief Self type
  using self_type = fdbbExprView<Tag, Expr>;

  /// @brief Expression type
  using expr_type = typename fdbb::remove_all<Expr>::type;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

private:
  /// @brief Raw expression data
  const expr_type* expr;

public:
  /// @brief Default constructor
  fdbbExprView() = default;

  /// @brief Constructor (wrap expression)
  explicit fdbbExprView(const Expr& expr)
    : expr(&expr)
  {
  }

  /// @brief Constructor (copy from other expression view)
  template<std::size_t TagOther>
  explicit fdbbExprView(const fdbbExprView<TagOther, Expr>& other)
    : expr(&other.get())
  {
  }

  /// @brief Returns constant reference to raw expression data
  const expr_type& get() const { return *expr; }

  /// @brief Returns constant pointer to raw expression data
  constexpr expr_type* operator->() const { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const
  {
    if (std::is_base_of<fdbbExprBase, expr_type>::value)
      os << "view<" << Tag << ">(" << *expr << ")";
    else
      os << "view<" << Tag << ">";
  }
};

/// @brief Output to string
template<std::size_t Tag, typename Expr>
std::ostream&
operator<<(std::ostream& os, fdbbExprView<Tag, Expr> expr)
{
  expr.print(os);
  return os;
}

/** @brief
 *  Element-wise unitary operators for a cache object
 */
#define FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                 \
  template<std::size_t Tag, typename Expr>                                     \
  struct fdbbExprUnaryOp_elem_##OPNAME : public fdbbExprBase                   \
  {                                                                            \
  private:                                                                     \
    /** @brief Self type */                                                    \
    using self_type = fdbbExprUnaryOp_elem_##OPNAME<Tag, Expr>;                \
                                                                               \
    /** @brief Expression type */                                              \
    using expr_type = typename fdbb::remove_all<Expr>::type;                   \
                                                                               \
  public:                                                                      \
    /** @brief Result type */                                                  \
    using result_type =                                                        \
      decltype(fdbb::elem_##OPNAME(std::declval<expr_type>().get()));          \
                                                                               \
    /** @brief Tag */                                                          \
    static constexpr std::size_t tag = Tag;                                    \
                                                                               \
  private:                                                                     \
    /** @brief Raw expression data */                                          \
    expr_type expr;                                                            \
                                                                               \
    /** @brief Result expression data */                                       \
    result_type result;                                                        \
                                                                               \
  public:                                                                      \
    /** @brief Default constructor */                                          \
    fdbbExprUnaryOp_elem_##OPNAME() = default;                                 \
                                                                               \
    /** @brief Constructor */                                                  \
    fdbbExprUnaryOp_elem_##OPNAME(Expr&& expr)                                 \
      : expr(expr)                                                             \
      , result(fdbb::elem_##OPNAME(expr.get()))                                \
    {                                                                          \
    }                                                                          \
                                                                               \
    /** @brief Returns constant reference to evaluated expression */           \
    const result_type& get() const { return result; }                          \
                                                                               \
    /** @brief Returns pointer to evaluated expression */                      \
    result_type* operator->() { return result; }                               \
                                                                               \
    /** @brief Pretty prints expression */                                     \
    void print(std::ostream& os) const                                         \
    {                                                                          \
      os << #OPNAME << "(" << expr << ")<" << Tag << ">";                      \
    }                                                                          \
  };                                                                           \
                                                                               \
  template<std::size_t Tag, typename Expr>                                     \
  std::ostream& operator<<(std::ostream& os,                                   \
                           fdbbExprUnaryOp_elem_##OPNAME<Tag, Expr> expr)      \
  {                                                                            \
    expr.print(os);                                                            \
    return os;                                                                 \
  }

FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(abs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(acosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(asinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(atanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(ceil)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(conj)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cos)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(cosh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erf)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(erfc)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(exp2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(fabs)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(floor)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(imag)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log10)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(log2)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(real)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(round)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(rsqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sign)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sin)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sinh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(sqrt)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tan)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(tanh)
FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS(trunc)

#undef FDBB_GENERATE_UNARY_ELEMENTOPERATION_OVERLOADS

/** @brief
 *  Element-wise binary operators between two cache objects
 */
#define FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(OPNAME)                \
  template<std::size_t Tag, typename Expr1, typename Expr2>                    \
  struct fdbbExprBinaryOp_elem_##OPNAME : public fdbbExprBase                  \
  {                                                                            \
  private:                                                                     \
    /** @brief Self type */                                                    \
    using self_type = fdbbExprBinaryOp_elem_##OPNAME<Tag, Expr1, Expr2>;       \
                                                                               \
    /** @brief First expression type */                                        \
    using expr1_type = typename fdbb::remove_all<Expr1>::type;                 \
                                                                               \
    /** @brief Second expression type */                                       \
    using expr2_type = typename fdbb::remove_all<Expr2>::type;                 \
                                                                               \
  public:                                                                      \
    /** @brief Result type */                                                  \
    using result_type =                                                        \
      decltype(fdbb::elem_##OPNAME(std::declval<expr1_type>().get(),           \
                                   std::declval<expr2_type>().get()));         \
                                                                               \
    /** @brief Tag */                                                          \
    static constexpr std::size_t tag = Tag;                                    \
                                                                               \
  private:                                                                     \
    /** @brief First raw expression data */                                    \
    expr1_type expr1;                                                          \
                                                                               \
    /** @brief Second raw expression data */                                   \
    expr2_type expr2;                                                          \
                                                                               \
    /** @brief Result expression data */                                       \
    result_type result;                                                        \
                                                                               \
  public:                                                                      \
    /** @brief Default constructor */                                          \
    fdbbExprBinaryOp_elem_##OPNAME() = default;                                \
                                                                               \
    /** @brief Constructor */                                                  \
    fdbbExprBinaryOp_elem_##OPNAME(Expr1&& expr1, Expr2&& expr2)               \
      : expr1(expr1)                                                           \
      , expr2(expr2)                                                           \
      , result(fdbb::elem_##OPNAME(expr1.get(), expr2.get()))                  \
    {                                                                          \
    }                                                                          \
                                                                               \
    /** @brief Returns constant reference to evaluated expression */           \
    const result_type& get() const { return result; }                          \
                                                                               \
    /** @brief Returns pointer to evaluated expression */                      \
    result_type* operator->() { return result; }                               \
                                                                               \
    /** @brief Pretty prints expression */                                     \
    void print(std::ostream& os) const                                         \
    {                                                                          \
      os << #OPNAME << "(" << expr1 << "," << expr2 << ")<" << Tag << ">";     \
    }                                                                          \
  };                                                                           \
                                                                               \
  template<std::size_t Tag, typename Expr1, typename Expr2>                    \
  std::ostream& operator<<(                                                    \
    std::ostream& os, fdbbExprBinaryOp_elem_##OPNAME<Tag, Expr1, Expr2> expr)  \
  {                                                                            \
    expr.print(os);                                                            \
    return os;                                                                 \
  }

FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(div)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(mul)
FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS(pow)

#undef FDBB_GENERATE_BINARY_ELEMENTOPERATION_OVERLOADS

/** @brief
 *  Binary operators between two cache objects
 */
#define FDBB_GENERATE_BINARY_OPERATION_OVERLOADS(OPNAME, OP)                   \
  template<std::size_t Tag, typename Expr1, typename Expr2>                    \
  struct fdbbExprBinaryOp_##OPNAME : public fdbbExprBase                       \
  {                                                                            \
  private:                                                                     \
    /** @brief Self type */                                                    \
    using self_type = fdbbExprBinaryOp_##OPNAME<Tag, Expr1, Expr2>;            \
                                                                               \
    /** @brief First expression type */                                        \
    using expr1_type = typename fdbb::remove_all<Expr1>::type;                 \
                                                                               \
    /** @brief Second expression type */                                       \
    using expr2_type = typename fdbb::remove_all<Expr2>::type;                 \
                                                                               \
  public:                                                                      \
    /** @brief Result type */                                                  \
    using result_type = decltype(                                              \
      std::declval<expr1_type>().get() OP std::declval<expr2_type>().get());   \
                                                                               \
    /** @brief Tag */                                                          \
    static constexpr std::size_t tag = Tag;                                    \
                                                                               \
  private:                                                                     \
    /** @brief First raw expression data */                                    \
    expr1_type expr1;                                                          \
                                                                               \
    /** @brief Second raw expression data */                                   \
    expr2_type expr2;                                                          \
                                                                               \
    /** @brief Result expression data */                                       \
    result_type result;                                                        \
                                                                               \
  public:                                                                      \
    /** @brief Default constructor */                                          \
    fdbbExprBinaryOp_##OPNAME() = default;                                     \
                                                                               \
    /** @brief Constructor */                                                  \
    fdbbExprBinaryOp_##OPNAME(Expr1&& expr1, Expr2&& expr2)                    \
      : expr1(expr1)                                                           \
      , expr2(expr2)                                                           \
      , result(expr1.get() OP expr2.get())                                     \
    {                                                                          \
    }                                                                          \
                                                                               \
    /** @brief Pretty prints expression */                                     \
    void print(std::ostream& os) const                                         \
    {                                                                          \
      os << "(" << expr1 << #OP << expr2 << ")<" << Tag << ">";                \
    }                                                                          \
                                                                               \
    /** @brief Returns evaluated expression */                                 \
    const result_type& get() const { return result; }                          \
  };                                                                           \
                                                                               \
  template<std::size_t Tag, typename Expr1, typename Expr2>                    \
  std::ostream& operator<<(std::ostream& os,                                   \
                           fdbbExprBinaryOp_##OPNAME<Tag, Expr1, Expr2> expr)  \
  {                                                                            \
    expr.print(os);                                                            \
    return os;                                                                 \
  }

FDBB_GENERATE_BINARY_OPERATION_OVERLOADS(add, +)
FDBB_GENERATE_BINARY_OPERATION_OVERLOADS(sub, -)
FDBB_GENERATE_BINARY_OPERATION_OVERLOADS(mul, *)

#undef FDBB_GENERATE_BINARY_OPERATION_OVERLOADS

/** @brief
 *  Binary operator/ for cache object
 */
template<std::size_t Tag, typename Expr1, typename Expr2>
struct fdbbExprBinaryOp_div : public fdbbExprBase
{
private:
  /// @brief Self type
  using self_type = fdbbExprBinaryOp_div<Tag, Expr1, Expr2>;

  /// @brief First expression type
  using expr1_type = typename fdbb::remove_all<Expr1>::type;

  /// @brief Second expression type
  using expr2_type = typename fdbb::remove_all<Expr2>::type;

public:
  /// @brief Result type
  using result_type = decltype(std::declval<expr1_type>().get() /
                               std::declval<expr2_type>().get());

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

private:
  /// @brief First raw expression data
  expr1_type expr1;

  /// @brief Second raw expression data
  expr2_type expr2;

  /// @brief Result expression data
  result_type result;

public:
  /// @brief Default constructor
  fdbbExprBinaryOp_div() = default;

  /// @brief Constructor
  fdbbExprBinaryOp_div(Expr1&& expr1, Expr2&& expr2)
    : expr1(expr1)
    , expr2(expr2)
    , result(expr1.get() / expr2.get())
  {
  }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const
  {
    os << "(" << expr1 << "/" << expr2 << ")<" << Tag << ">";
  }

  /// @brief Returns evaluated expression
  const result_type& get() const { return result; }
};

template<std::size_t Tag, typename Expr1, typename Expr2>
std::ostream&
operator<<(std::ostream& os, fdbbExprBinaryOp_div<Tag, Expr1, Expr2> expr)
{
  expr.print(os);
  return os;
}

/** @brief
 *  Binary operator+ between two cache objects
 */
template<
  typename Expr1,
  typename Expr2,
  typename =
    typename fdbb::enable_if_all_type_of<Expr1, Expr2, EnumETL::CACHE>::type>
auto
operator+(Expr1&& expr1, Expr2&& expr2) noexcept -> fdbbExprBinaryOp_add<
  utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                           '+',
                           fdbb::remove_all<Expr2>::type::tag),
  Expr1,
  Expr2>
{
  return fdbbExprBinaryOp_add<utils::hash<std::size_t>(
                                fdbb::remove_all<Expr1>::type::tag,
                                '+',
                                fdbb::remove_all<Expr2>::type::tag),
                              Expr1,
                              Expr2>(std::forward<Expr1>(expr1),
                                     std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator+ between an object of arithmetic type and a cache object
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr2,
           EnumETL::CACHE,
           std::is_arithmetic<Expr1>::value>::type>
auto
operator+(Expr1&& expr1, Expr2&& expr2) noexcept -> fdbbExprBinaryOp_add<
  utils::hash<std::size_t>(725795011, '+', fdbb::remove_all<Expr2>::type::tag),
  fdbbExpr<725795011, Expr1>,
  Expr2>
{
  return fdbbExprBinaryOp_add<
    utils::hash<std::size_t>(
      725795011, '+', fdbb::remove_all<Expr2>::type::tag),
    fdbbExpr<725795011, Expr1>,
    Expr2>(fdbbExpr<725795011, Expr1>(Expr1(expr1)),
           std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator+ between a cache object and an object of arithmetic type
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr1,
           EnumETL::CACHE,
           std::is_arithmetic<Expr2>::value>::type>
auto
operator+(Expr1&& expr1, Expr2&& expr2) noexcept -> fdbbExprBinaryOp_add<
  utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '+', 169422329),
  Expr1,
  fdbbExpr<169422329, Expr2>>
{
  return fdbbExprBinaryOp_add<
    utils::hash<std::size_t>(
      fdbb::remove_all<Expr1>::type::tag, '+', 169422329),
    Expr1,
    fdbbExpr<169422329, Expr2>>(std::forward<Expr1>(expr1),
                                fdbbExpr<169422329, Expr2>(Expr2(expr2)));
}

/** @brief
 *  Binary operator- between two cache objects
 */
template<
  typename Expr1,
  typename Expr2,
  typename =
    typename fdbb::enable_if_all_type_of<Expr1, Expr2, EnumETL::CACHE>::type>
auto
operator-(Expr1&& expr1, Expr2&& expr2) noexcept -> fdbbExprBinaryOp_sub<
  utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                           '-',
                           fdbb::remove_all<Expr2>::type::tag),
  Expr1,
  Expr2>
{
  return fdbbExprBinaryOp_sub<utils::hash<std::size_t>(
                                fdbb::remove_all<Expr1>::type::tag,
                                '-',
                                fdbb::remove_all<Expr2>::type::tag),
                              Expr1,
                              Expr2>(std::forward<Expr1>(expr1),
                                     std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator- between an object of arithmetic type and a cache object
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr2,
           EnumETL::CACHE,
           std::is_arithmetic<Expr1>::value>::type>
auto
operator-(Expr1&& expr1, Expr2&& expr2) noexcept -> fdbbExprBinaryOp_sub<
  utils::hash<std::size_t>(574849605, '-', fdbb::remove_all<Expr2>::type::tag),
  fdbbExpr<574849605, Expr1>,
  Expr2>
{
  return fdbbExprBinaryOp_sub<
    utils::hash<std::size_t>(
      574849605, '-', fdbb::remove_all<Expr2>::type::tag),
    fdbbExpr<574849605, Expr1>,
    Expr2>(fdbbExpr<574849605, Expr1>(Expr1(expr1)),
           std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator- between a cache object and an object of arithmetic type
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr1,
           EnumETL::CACHE,
           std::is_arithmetic<Expr2>::value>::type>
auto
operator-(Expr1&& expr1, Expr2&& expr2) noexcept -> fdbbExprBinaryOp_sub<
  utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '-', 681648267),
  Expr1,
  fdbbExpr<681648267, Expr2>>
{
  return fdbbExprBinaryOp_sub<
    utils::hash<std::size_t>(
      fdbb::remove_all<Expr1>::type::tag, '-', 681648267),
    Expr1,
    fdbbExpr<681648267, Expr2>>(std::forward<Expr1>(expr1),
                                fdbbExpr<681648267, Expr2>(Expr2(expr2)));
}

/** @brief
 *  Binary operator* between two cache objects
 */
template<
  typename Expr1,
  typename Expr2,
  typename =
    typename fdbb::enable_if_all_type_of<Expr1, Expr2, EnumETL::CACHE>::type>
auto operator*(Expr1&& expr1, Expr2&& expr2) noexcept -> fdbbExprBinaryOp_mul<
  utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                           '*',
                           fdbb::remove_all<Expr2>::type::tag),
  Expr1,
  Expr2>
{
  return fdbbExprBinaryOp_mul<utils::hash<std::size_t>(
                                fdbb::remove_all<Expr1>::type::tag,
                                '*',
                                fdbb::remove_all<Expr2>::type::tag),
                              Expr1,
                              Expr2>(std::forward<Expr1>(expr1),
                                     std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator* between an object of arithmetic type and a cache object
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr2,
           EnumETL::CACHE,
           std::is_arithmetic<Expr1>::value>::type>
auto operator*(Expr1&& expr1, Expr2&& expr2) noexcept -> fdbbExprBinaryOp_mul<
  utils::hash<std::size_t>(839176472, '*', fdbb::remove_all<Expr2>::type::tag),
  fdbbExpr<839176472, Expr1>,
  Expr2>
{
  return fdbbExprBinaryOp_mul<
    utils::hash<std::size_t>(
      839176472, '*', fdbb::remove_all<Expr2>::type::tag),
    fdbbExpr<839176472, Expr1>,
    Expr2>(fdbbExpr<839176472, Expr1>(Expr1(expr1)),
           std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator* between a cache object and an object of arithmetic type
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr1,
           EnumETL::CACHE,
           std::is_arithmetic<Expr2>::value>::type>
auto operator*(Expr1&& expr1, Expr2&& expr2) noexcept -> fdbbExprBinaryOp_mul<
  utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '*', 689934539),
  Expr1,
  fdbbExpr<689934539, Expr2>>
{
  return fdbbExprBinaryOp_mul<
    utils::hash<std::size_t>(
      fdbb::remove_all<Expr1>::type::tag, '*', 689934539),
    Expr1,
    fdbbExpr<689934539, Expr2>>(std::forward<Expr1>(expr1),
                                fdbbExpr<689934539, Expr2>(Expr2(expr2)));
}

/** @brief
 *  Binary operator/ between two cache objects
 */
template<
  typename Expr1,
  typename Expr2,
  typename =
    typename fdbb::enable_if_all_type_of<Expr1, Expr2, EnumETL::CACHE>::type>
auto
operator/(Expr1&& expr1, Expr2&& expr2) noexcept -> fdbbExprBinaryOp_div<
  utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                           '/',
                           fdbb::remove_all<Expr2>::type::tag),
  Expr1,
  Expr2>
{
  return fdbbExprBinaryOp_div<utils::hash<std::size_t>(
                                fdbb::remove_all<Expr1>::type::tag,
                                '/',
                                fdbb::remove_all<Expr2>::type::tag),
                              Expr1,
                              Expr2>(std::forward<Expr1>(expr1),
                                     std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator/ between an object of arithmetic type and a cache object
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr2,
           EnumETL::CACHE,
           std::is_arithmetic<Expr1>::value>::type>
auto
operator/(Expr1&& expr1, Expr2&& expr2) noexcept -> fdbbExprBinaryOp_div<
  utils::hash<std::size_t>(351934064, '/', fdbb::remove_all<Expr2>::type::tag),
  fdbbExpr<351934064, Expr1>,
  Expr2>
{
  return fdbbExprBinaryOp_div<
    utils::hash<std::size_t>(
      351934064, '/', fdbb::remove_all<Expr2>::type::tag),
    fdbbExpr<351934064, Expr1>,
    Expr2>(fdbbExpr<351934064, Expr1>(Expr1(expr1)),
           std::forward<Expr2>(expr2));
}

/** @brief
 *  Binary operator/ between a cache object and an object of arithmetic type
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr1,
           EnumETL::CACHE,
           std::is_arithmetic<Expr2>::value>::type>
auto
operator/(Expr1&& expr1, Expr2&& expr2) noexcept -> fdbbExprBinaryOp_div<
  utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag, '/', 253548959),
  Expr1,
  fdbbExpr<253548959, Expr2>>
{
  return fdbbExprBinaryOp_div<
    utils::hash<std::size_t>(
      fdbb::remove_all<Expr1>::type::tag, '/', 253548959),
    Expr1,
    fdbbExpr<253548959, Expr2>>(std::forward<Expr1>(expr1),
                                fdbbExpr<253548959, Expr2>(Expr2(expr2)));
}

} // namespace cache

/** @namespace fdbb::cache2
 *
 *  @brief
 *  The \ref fdbb::cache2 namespace, containing the lightweight
 *  caching functionality of the FDBB library
 *
 *  The \ref fdbb::cache2 namespace contains a lightweight
 *  implementation of a caching strategy for the FDBB library. In
 *  contrast to the caching mechanism implemented in the \ref
 *  fdbb:cache namespace, this implementation does not store
 *  individual objects for each operator of the expression tree but
 *  fuses all subexpressions into a single object.
 *
 *  @note This Source Code Form is subject to the terms of the Mozilla
 *  Public License, v. 2.0. If a copy of the MPL was not distributed
 *  with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace cache2 {

/** @brief
 *  A cache object storing an object of type \ref Expr including
 *  ownership of that type
 *
 *  A cache object stores an object of type \ref Expr and has
 *  ownership of that type. The type must be created externally and
 *  passed to the cache object via its move constructor.
 */
template<std::size_t Tag, typename Expr>
struct fdbbExpr : public fdbbExprBase
{
private:
  /// @brief Self type
  using self_type = fdbbExpr<Tag, Expr>;

  /// @brief Expression type
  using expr_type = typename fdbb::remove_all<Expr>::type;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

private:
  /// @brief Raw expression data
  expr_type expr;

public:
  /// @brief Default constructor
  fdbbExpr() = default;

  /// @brief Constructor (copy from expression)
  explicit fdbbExpr(const Expr& expr)
    : expr(expr)
  {
  }

  /// @brief Constructor (move from expression)
  explicit fdbbExpr(Expr&& expr)
    : expr(expr)
  {
  }

  /// @brief Constructor (evaluate from cache object)
  template<typename T,
           typename = typename std::enable_if<
             std::is_base_of<fdbbExprBase, T>::value>::type>
  explicit fdbbExpr(const T& obj)
    : expr(obj.get())
  {
  }

  /// @brief Constructor (evaluate from cache object)
  template<typename T,
           typename = typename std::enable_if<
             std::is_base_of<fdbbExprBase, T>::value>::type>
  explicit fdbbExpr(T&& obj)
    : expr(obj.get())
  {
  }

  /// @brief Returns constant reference to raw expression data
  const expr_type& get() const { return expr; }

  /// @brief Returns reference to raw expression data
  const expr_type& get() { return expr; }

  /// @brief Returns pointer to raw expression data
  expr_type* operator->() { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const { os << "expr<" << Tag << ">"; }
};

/// @brief Output to string
template<std::size_t Tag, typename Expr>
std::ostream&
operator<<(std::ostream& os, fdbbExpr<Tag, Expr> expr)
{
  expr.print(os);
  return os;
}

/** @brief
 *  A view on a cache object storing a reference to an object of type
 *  \ref Expr excluding ownership of that object
 *
 *  A view on a cache object stores a reference to an object of type
 *  \ref Expr but does not have ownership of that type. The object is
 *  managed externally. The view just serves as a lughtweight hull.
 */
template<std::size_t Tag, typename Expr>
struct fdbbExprView : public fdbbExprBase
{
private:
  /// @brief Self type
  using self_type = fdbbExprView<Tag, Expr>;

  /// @brief Expression type
  using expr_type = typename fdbb::remove_all<Expr>::type;

public:
  /// @brief Result type
  using result_type = expr_type;

  /// @brief Tag
  static constexpr std::size_t tag = Tag;

private:
  /// @brief Raw expression data
  const expr_type* expr;

public:
  /// @brief Default constructor
  fdbbExprView() = default;

  /// @brief Constructor (wrap expression)
  explicit fdbbExprView(const Expr& expr)
    : expr(&expr)
  {
  }

  /// @brief Constructor (copy from other expression view)
  template<std::size_t TagOther>
  explicit fdbbExprView(const fdbbExprView<TagOther, Expr>& other)
    : expr(&other.get())
  {
  }

  /// @brief Returns constant reference to raw expression data
  const expr_type& get() const { return *expr; }

  /// @brief Returns pointer to raw expression data
  constexpr expr_type* operator->() const { return expr; }

  /// @brief Pretty prints expression
  void print(std::ostream& os) const { os << "view<" << Tag << ">"; }
};

/// @brief Output to string
template<std::size_t Tag, typename Expr>
std::ostream&
operator<<(std::ostream& os, fdbbExprView<Tag, Expr> expr)
{
  expr.print(os);
  return os;
}

/** @brief
 *  Binary operator+ between two cache objects
 */
template<
  typename Expr1,
  typename Expr2,
  typename =
    typename fdbb::enable_if_all_type_of<Expr1, Expr2, EnumETL::CACHE2>::type>
auto
operator+(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  fdbbExpr<utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                                    '+',
                                    fdbb::remove_all<Expr2>::type::tag),
           decltype(expr1.get() + expr2.get())>(std::move(expr1.get() +
                                                          expr2.get())))
{
  return fdbbExpr<utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                                           '+',
                                           fdbb::remove_all<Expr2>::type::tag),
                  decltype(expr1.get() + expr2.get())>(
    std::move(expr1.get() + expr2.get()));
}

/** @brief
 *  Binary operator+ between an object of arithmetic type and a cache object
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr2,
           EnumETL::CACHE2,
           std::is_arithmetic<Expr1>::value>::type>
auto
operator+(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  fdbbExpr<utils::hash<std::size_t>(725795011,
                                    '+',
                                    fdbb::remove_all<Expr2>::type::tag),
           decltype(expr1 + expr2.get())>(std::move(expr1 + expr2.get())))
{
  return fdbbExpr<utils::hash<std::size_t>(
                    725795011, '+', fdbb::remove_all<Expr2>::type::tag),
                  decltype(expr1 + expr2.get())>(
    std::move(expr1 + expr2.get()));
}

/** @brief
 *  Binary operator+ between a cache object and an object of arithmetic type
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr1,
           EnumETL::CACHE2,
           std::is_arithmetic<Expr2>::value>::type>
auto
operator+(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  fdbbExpr<utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                                    '+',
                                    169422329),
           decltype(expr1.get() + expr2)>(std::move(expr1.get() + expr2)))
{
  return fdbbExpr<utils::hash<std::size_t>(
                    fdbb::remove_all<Expr1>::type::tag, '+', 169422329),
                  decltype(expr1.get() + expr2)>(
    std::move(expr1.get() + expr2));
}

/** @brief
 *  Binary operator- between two cache objects
 */
template<
  typename Expr1,
  typename Expr2,
  typename =
    typename fdbb::enable_if_all_type_of<Expr1, Expr2, EnumETL::CACHE2>::type>
auto
operator-(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  fdbbExpr<utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                                    '-',
                                    fdbb::remove_all<Expr2>::type::tag),
           decltype(expr1.get() - expr2.get())>(std::move(expr1.get() -
                                                          expr2.get())))
{
  return fdbbExpr<utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                                           '-',
                                           fdbb::remove_all<Expr2>::type::tag),
                  decltype(expr1.get() - expr2.get())>(
    std::move(expr1.get() - expr2.get()));
}

/** @brief
 *  Binary operator- between an object of arithmetic type and a cache object
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr2,
           EnumETL::CACHE2,
           std::is_arithmetic<Expr1>::value>::type>
auto
operator-(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  fdbbExpr<utils::hash<std::size_t>(574849605,
                                    '-',
                                    fdbb::remove_all<Expr2>::type::tag),
           decltype(expr1 - expr2.get())>(std::move(expr1 - expr2.get())))
{
  return fdbbExpr<utils::hash<std::size_t>(
                    574849605, '-', fdbb::remove_all<Expr2>::type::tag),
                  decltype(expr1 - expr2.get())>(
    std::move(expr1 - expr2.get()));
}

/** @brief
 *  Binary operator- between a cache object and an object of arithmetic type
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr1,
           EnumETL::CACHE2,
           std::is_arithmetic<Expr2>::value>::type>
auto
operator-(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  fdbbExpr<utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                                    '-',
                                    681648267),
           decltype(expr1.get() - expr2)>(std::move(expr1.get() - expr2)))
{
  return fdbbExpr<utils::hash<std::size_t>(
                    fdbb::remove_all<Expr1>::type::tag, '-', 681648267),
                  decltype(expr1.get() - expr2)>(
    std::move(expr1.get() - expr2));
}

/** @brief
 *  Binary operator* between two cache objects
 */
template<
  typename Expr1,
  typename Expr2,
  typename =
    typename fdbb::enable_if_all_type_of<Expr1, Expr2, EnumETL::CACHE2>::type>
auto operator*(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  fdbbExpr<utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                                    '*',
                                    fdbb::remove_all<Expr2>::type::tag),
           decltype(expr1.get() * expr2.get())>(std::move(expr1.get() *
                                                          expr2.get())))
{
  return fdbbExpr<utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                                           '*',
                                           fdbb::remove_all<Expr2>::type::tag),
                  decltype(expr1.get() * expr2.get())>(
    std::move(expr1.get() * expr2.get()));
}

/** @brief
 *  Binary operator* between an object of arithmetic type and a cache object
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr2,
           EnumETL::CACHE2,
           std::is_arithmetic<Expr1>::value>::type>
auto operator*(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  fdbbExpr<utils::hash<std::size_t>(839176472,
                                    '*',
                                    fdbb::remove_all<Expr2>::type::tag),
           decltype(expr1 * expr2.get())>(std::move(expr1 * expr2.get())))
{
  return fdbbExpr<utils::hash<std::size_t>(
                    839176472, '*', fdbb::remove_all<Expr2>::type::tag),
                  decltype(expr1 * expr2.get())>(
    std::move(expr1 * expr2.get()));
}

/** @brief
 *  Binary operator* between a cache object and an object of arithmetic type
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr1,
           EnumETL::CACHE2,
           std::is_arithmetic<Expr2>::value>::type>
auto operator*(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  fdbbExpr<utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                                    '*',
                                    689934539),
           decltype(expr1.get() * expr2)>(std::move(expr1.get() * expr2)))
{
  return fdbbExpr<utils::hash<std::size_t>(
                    fdbb::remove_all<Expr1>::type::tag, '*', 689934539),
                  decltype(expr1.get() * expr2)>(
    std::move(expr1.get() * expr2));
}

/** @brief
 *  Binary operator/ between two cache objects
 */
template<
  typename Expr1,
  typename Expr2,
  typename =
    typename fdbb::enable_if_all_type_of<Expr1, Expr2, EnumETL::CACHE2>::type>
auto
operator/(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  fdbbExpr<utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                                    '/',
                                    fdbb::remove_all<Expr2>::type::tag),
           decltype(expr1.get() / expr2.get())>(std::move(expr1.get() /
                                                          expr2.get())))
{
  return fdbbExpr<utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                                           '/',
                                           fdbb::remove_all<Expr2>::type::tag),
                  decltype(expr1.get() / expr2.get())>(
    std::move(expr1.get() / expr2.get()));
}

/** @brief
 *  Binary operator/ between an object of arithmetic type and a cache object
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr2,
           EnumETL::CACHE2,
           std::is_arithmetic<Expr1>::value>::type>
auto
operator/(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  fdbbExpr<utils::hash<std::size_t>(351934064,
                                    '/',
                                    fdbb::remove_all<Expr2>::type::tag),
           decltype(expr1 / expr2.get())>(std::move(expr1 / expr2.get())))
{
  return fdbbExpr<utils::hash<std::size_t>(
                    351934064, '/', fdbb::remove_all<Expr2>::type::tag),
                  decltype(expr1 / expr2.get())>(
    std::move(expr1 / expr2.get()));
}

/** @brief
 *  Binary operator/ between a cache object and an object of arithmetic type
 */
template<typename Expr1,
         typename Expr2,
         typename = typename fdbb::enable_if_type_of_and_cond<
           Expr1,
           EnumETL::CACHE2,
           std::is_arithmetic<Expr2>::value>::type>
auto
operator/(Expr1&& expr1, Expr2&& expr2) noexcept -> decltype(
  fdbbExpr<utils::hash<std::size_t>(fdbb::remove_all<Expr1>::type::tag,
                                    '/',
                                    253548959),
           decltype(expr1.get() / expr2)>(std::move(expr1.get() / expr2)))
{
  return fdbbExpr<utils::hash<std::size_t>(
                    fdbb::remove_all<Expr1>::type::tag, '/', 253548959),
                  decltype(expr1.get() / expr2)>(
    std::move(expr1.get() / expr2));
}

} // namespace cache2

} // namespace fdbb

#endif // FDBB_CACHE_HPP
