/** @file fdbbPrimitive2d.hpp
 *
 *  @brief 2D implementation for primitive variables
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_PRIMITIVE_2D_H
#define FDBB_PRIMITIVE_2D_H

#include <type_traits>

#include "fdbbConfig.hpp"
#include "fdbbEnums.hpp"
#include "fdbbTraits.hpp"
#include "fdbbTypes.hpp"

namespace fdbb {

/** @brief
 *
 *  Specialization of the fdbbVariables structure for primitive
 *  variables in 2d space dimensions and arbitrary equation of state
 *
 *  \f[
 *      U = \begin{bmatrix}
 *            u_0\\
 *            u_1\\
 *            u_2\\
 *            u_3
 *          \end{bmatrix}
 *        = \begin{bmatrix}
 *            \rho\\
 *            v_0\\
 *            v_1\\
 *            p
 *          \end{bmatrix}
 *  \f]
 *  where
 *  \f$ \rho \f$ is the volumetric mass density,
 *  \f$ v_0  \f$ is the velocity in x-direction,
 *  \f$ v_1  \f$ is the velocity in y-direction, and
 *  \f$ p    \f$ is the absolute pressure
 */
template<typename EOS, typename Traits>
struct fdbbVariables<EOS, 2, EnumForm::primitive, Traits>
{
  /// @brief Equation of state
  using eos = EOS;

  /// @brief Type traits
  using traits = Traits;

  /// @brief Dimension
  static constexpr index_t dim = 1;

  /// @brief Return output stream
  static std::ostream& print(std::ostream& os)
  {
    os << "Primitive variables in 2d, ";
    eos::print(os);
    return os;
  }

  /** @brief
   *  Volumetric mass density variable \f$ \rho = u_0 \f$ for primitive
   *  variables in 2d
   *
   *  @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rho(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      traits::template getVariable<EnumVar::rho>(std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::rho>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Velocity variable \f$ v_i = u_{i+1}, i=0,1\f$ for
   *  primitive variables in 2d
   *
   *  @ingroup VariablesPrimary
   *
   *  @{
   */
  template<index_t idim, typename... Vars>
  static FDBB_INLINE auto constexpr v(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    ->
    typename std::enable_if<idim == 0,
                            decltype(traits::template getVariable<EnumVar::v_x>(
                              std::forward<Vars>(vars)...))>::type
#endif
  {
    return traits::template getVariable<EnumVar::v_x>(
      std::forward<Vars>(vars)...);
  }

  template<index_t idim, typename... Vars>
  static FDBB_INLINE auto constexpr v(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    ->
    typename std::enable_if<idim == 1,
                            decltype(traits::template getVariable<EnumVar::v_y>(
                              std::forward<Vars>(vars)...))>::type
#endif
  {
    return traits::template getVariable<EnumVar::v_y>(
      std::forward<Vars>(vars)...);
  }
  /** @} */

  /** @brief
   *  Absolute pressure variable \f$ p = u_3 \f$ for
   *  primitive variables in 2d
   *
   *  @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr p(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      traits::template getVariable<EnumVar::p>(std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::p>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Internal energy per unit mass variable \f$ e \f$ for primitive
   *  variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr e(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_base_of<fdbbEOS_pVT, eos>::value,
      decltype(eos::e_rhop(rho(std::forward<Vars>(vars)...),
                           p(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return eos::e_rhop(rho(std::forward<Vars>(vars)...),
                       p(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Kinetic energy per unit mass variable \f$ E_kin = \frac12 (v_0^2
   *  +v_1^2)\f$ for primitive variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr E_kin(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
                (fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                                v<0>(std::forward<Vars>(vars)...)) +
                 fdbb::elem_mul(v<1>(std::forward<Vars>(vars)...),
                                v<1>(std::forward<Vars>(vars)...))))
#endif
  {
    return CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
           (fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                           v<0>(std::forward<Vars>(vars)...)) +
            fdbb::elem_mul(v<1>(std::forward<Vars>(vars)...),
                           v<1>(std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  Total energy per unit mass variable \f$ E = e + \frac12 (v_0^2+
   *  v_1^2) \f$ for primitive variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr E(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(e(std::forward<Vars>(vars)...) +
                E_kin(std::forward<Vars>(vars)...))
#endif
  {
    return e(std::forward<Vars>(vars)...) + E_kin(std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Total energy per unit volume variable \f$ \rho E = \rho *
   *  (e + \frac12 (v_0^2+v_1^2) \f$ for primitive variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoE(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                               E(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                          E(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Momentum variable \f$ \rho v_0 \f$ for primitive
   *  variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<index_t idim, typename... Vars>
  static FDBB_INLINE auto constexpr rhov(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(v<idim>(std::forward<Vars>(vars)...),
                               rho(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(v<idim>(std::forward<Vars>(vars)...),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Magnitude of momentum \f$ \| \mathbf{m} \| \f$ for primitive
   *  variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhov_mag(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      fdbb::elem_sqrt(fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                                     rhov<0>(std::forward<Vars>(vars)...)) +
                      fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                                     rhov<1>(std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::elem_sqrt(
      fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                     rhov<0>(std::forward<Vars>(vars)...)) +
      fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                     rhov<1>(std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  Magnitude of momentum \f$ \| \mathbf{m} \|^2 \f$ squared for
   *  primitive variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhov_mag2(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                               rhov<0>(std::forward<Vars>(vars)...)) +
                fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                               rhov<1>(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(rhov<0>(std::forward<Vars>(vars)...),
                          rhov<0>(std::forward<Vars>(vars)...)) +
           fdbb::elem_mul(rhov<1>(std::forward<Vars>(vars)...),
                          rhov<1>(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Magnitude of velocity \f$ \| \mathbf{v} \| \f$ for primitive
   *  variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr v_mag(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      fdbb::elem_sqrt(fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                                     v<0>(std::forward<Vars>(vars)...)) +
                      fdbb::elem_mul(v<1>(std::forward<Vars>(vars)...),
                                     v<1>(std::forward<Vars>(vars)...))))
#endif
  {
    return fdbb::elem_sqrt(fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                                          v<0>(std::forward<Vars>(vars)...)) +
                           fdbb::elem_mul(v<1>(std::forward<Vars>(vars)...),
                                          v<1>(std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  Magnitude of velocity \f$ \| \mathbf{v} \|^2 \f$ squared for
   *  primitive variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr v_mag2(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                               v<0>(std::forward<Vars>(vars)...)) +
                fdbb::elem_mul(v<1>(std::forward<Vars>(vars)...),
                               v<1>(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                          v<0>(std::forward<Vars>(vars)...)) +
           fdbb::elem_mul(v<1>(std::forward<Vars>(vars)...),
                          v<1>(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Kinetic energy per unit volume variable \f$ \rho E_kin = \frac12 ((\rho
   *  v_0)^2+(\rho v_1)^2)/\rho \f$ for primitive variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoE_kin(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
      fdbb::elem_mul(fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                                    v<0>(std::forward<Vars>(vars)...)) +
                       fdbb::elem_mul(v<1>(std::forward<Vars>(vars)...),
                                      v<1>(std::forward<Vars>(vars)...)),
                     rho(std::forward<Vars>(vars)...)))
#endif
  {
    return CONSTANT(0.5, rho(std::forward<Vars>(vars)...)) *
           fdbb::elem_mul(fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...),
                                         v<0>(std::forward<Vars>(vars)...)) +
                            fdbb::elem_mul(v<1>(std::forward<Vars>(vars)...),
                                           v<1>(std::forward<Vars>(vars)...)),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Internal energy per unit volume variable \f$ \rho e = \rho E -
   *  \frac12 ((\rho v_0)^2+(\rho v_1)^2)/\rho \f$ for primitive
   *  variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoe(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(rhoE(std::forward<Vars>(vars)...) -
                rhoE_kin(std::forward<Vars>(vars)...))
#endif
  {
    return rhoE(std::forward<Vars>(vars)...) -
           rhoE_kin(std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Total enthalpy per unit volume \f$ \rho H = \rho E + p \f$ for
   *  primitive variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoH(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(rhoE(std::forward<Vars>(vars)...) +
                p(std::forward<Vars>(vars)...))
#endif
  {
    return rhoE(std::forward<Vars>(vars)...) + p(std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Total enthalpy per unit mass \f$ H = E + p/\rho \f$ for
   *  primitive variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr H(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(E(std::forward<Vars>(vars)...) +
                fdbb::elem_div(p(std::forward<Vars>(vars)...),
                               rho(std::forward<Vars>(vars)...)))
#endif
  {
    return E(std::forward<Vars>(vars)...) +
           fdbb::elem_div(p(std::forward<Vars>(vars)...),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Enthalpy per unit volume \f$ \rho h = \rho e + p \f$ for
   *  primitive variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rhoh(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(rhoe(std::forward<Vars>(vars)...) +
                p(std::forward<Vars>(vars)...))
#endif
  {
    return rhoe(std::forward<Vars>(vars)...) + p(std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Enthalpy per unit mass \f$ h = e + p/\rho \f$ for primitive
   *  variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr h(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(e(std::forward<Vars>(vars)...) +
                fdbb::elem_div(p(std::forward<Vars>(vars)...),
                               rho(std::forward<Vars>(vars)...)))
#endif
  {
    return e(std::forward<Vars>(vars)...) +
           fdbb::elem_div(p(std::forward<Vars>(vars)...),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Speed of sound variable \f$ c \f$ for primitve
   *  variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr c(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      std::is_base_of<fdbbEOS_pVT, eos>::value,
      decltype(eos::c_rhop(rho(std::forward<Vars>(vars)...),
                           p(std::forward<Vars>(vars)...)))>::type
#endif
  {
    return eos::c_rhop(rho(std::forward<Vars>(vars)...),
                       p(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Directional velocty \f$ v_dir = (v_x*d_x+v_y*d_y)/|d| \f$ for primitive
   *  variables in 2d.
   *
   *  @tparam    T The type of the entries of direction vector d
   *
   *  @param[in] d_x  x component of vector d
   *  @param[in] d_y  y component of vector d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename T, typename... Vars>
  static FDBB_INLINE auto constexpr v_dir(T&& d_x,
                                          T&& d_y,
                                          Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(
      (fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...), std::forward<T>(d_x)) +
       fdbb::elem_mul(v<1>(std::forward<Vars>(vars)...), std::forward<T>(d_y))),
      fdbb::elem_sqrt(
        fdbb::elem_mul(std::forward<T>(d_x), std::forward<T>(d_x)) +
        fdbb::elem_mul(std::forward<T>(d_y), std::forward<T>(d_y)))))
#endif
  {
    return fdbb::elem_div(
      (fdbb::elem_mul(v<0>(std::forward<Vars>(vars)...), std::forward<T>(d_x)) +
       fdbb::elem_mul(v<1>(std::forward<Vars>(vars)...), std::forward<T>(d_y))),
      fdbb::elem_sqrt(
        fdbb::elem_mul(std::forward<T>(d_x), std::forward<T>(d_x)) +
        fdbb::elem_mul(std::forward<T>(d_y), std::forward<T>(d_y))));
  }

  /** @brief
   *  State vector of conservative variables in 2d
   *
   *  @ingroup VariablesPrimary
   */
  template<std::size_t Tag, typename... Vars>
  static FDBB_INLINE auto constexpr conservative(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<Tag, 4, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(rhov<0>(std::forward<Vars>(vars)...)),
      std::move(rhov<1>(std::forward<Vars>(vars)...)),
      std::move(rhoE(std::forward<Vars>(vars)...))))
#endif
  {
    return make_fdbbBlockExpr<Tag, 4, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(rhov<0>(std::forward<Vars>(vars)...)),
      std::move(rhov<1>(std::forward<Vars>(vars)...)),
      std::move(rhoE(std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  State vector of primitive variables in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<std::size_t Tag, typename... Vars>
  static FDBB_INLINE auto constexpr primitive(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<Tag, 4, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(v<0>(std::forward<Vars>(vars)...)),
      std::move(v<1>(std::forward<Vars>(vars)...)),
      std::move(p(std::forward<Vars>(vars)...))))
#endif
  {
    return make_fdbbBlockExpr<Tag, 4, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(v<0>(std::forward<Vars>(vars)...)),
      std::move(v<1>(std::forward<Vars>(vars)...)),
      std::move(p(std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  Vector of characteristic variables in 2d
   *  Current implementation holds only for ideal gases!
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *
   *  @param[in] n_x  x component of vector n
   *  @param[in] n_y  y component of vector n
   *
   *  @ingroup VariablesSecondary
   */
  template<std::size_t Tag, typename T, typename... Vars>
  static FDBB_INLINE auto constexpr characteristic(T&& n_x,
                                                   T&& n_y,
                                                   Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<Tag, 4, 1>(
      std::move(v_dir(std::forward<T>(n_x),
                      std::forward<T>(n_y),
                      std::forward<Vars>(vars)...) -
                c(std::forward<Vars>(vars)...)),
      std::move(v_dir(std::forward<T>(n_x),
                      std::forward<T>(n_y),
                      std::forward<Vars>(vars)...)),
      std::move(v_dir(std::forward<T>(n_x),
                      std::forward<T>(n_y),
                      std::forward<Vars>(vars)...)),
      std::move(v_dir(std::forward<T>(n_x),
                      std::forward<T>(n_y),
                      std::forward<Vars>(vars)...) +
                c(std::forward<Vars>(vars)...))))
#endif
  {
    return make_fdbbBlockExpr<Tag, 4, 1>(
      std::move(v_dir(std::forward<T>(n_x),
                      std::forward<T>(n_y),
                      std::forward<Vars>(vars)...) -
                c(std::forward<Vars>(vars)...)),
      std::move(v_dir(std::forward<T>(n_x),
                      std::forward<T>(n_y),
                      std::forward<Vars>(vars)...)),
      std::move(v_dir(std::forward<T>(n_x),
                      std::forward<T>(n_y),
                      std::forward<Vars>(vars)...)),
      std::move(v_dir(std::forward<T>(n_x),
                      std::forward<T>(n_y),
                      std::forward<Vars>(vars)...) +
                c(std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  Vector of Riemann invariants in 2d
   *  Current implementation holds only for ideal gases!
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *        and of entries of tangential vector \xi
   *
   *  @param[in] n_x    x component of vector n
   *  @param[in] n_y    y component of vector n
   *  @param[in] \xi_x  x component of vector \xi
   *  @param[in] \xi_y  y component of vector \xi
   *
   *  @ingroup VariablesSecondary
   */
  template<std::size_t Tag, typename T, typename... Vars>
  static FDBB_INLINE auto constexpr riemann(T&& n_x,
                                            T&& n_y,
                                            T&& xi_x,
                                            T&& xi_y,
                                            Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<Tag, 4, 1>(
      std::move(
        v_dir(std::forward<T>(n_x),
              std::forward<T>(n_y),
              std::forward<Vars>(vars)...) -
        fdbb::elem_div(
          fdbb::elem_mul(CONSTANT(2.0, rho(std::forward<Vars>(vars)...)),
                         c(std::forward<Vars>(vars)...)),
          eos::gamma - CONSTANT(1.0, rho(std::forward<Vars>(vars)...)))),
      std::move(fdbb::elem_mul(
        eos::cv,
        fdbb::elem_log(fdbb::elem_div(
          p(std::forward<Vars>(vars)...),
          fdbb::elem_pow(rho(std::forward<Vars>(vars)...), eos::gamma))))),
      std::move(v_dir(std::forward<T>(xi_x),
                      std::forward<T>(xi_y),
                      std::forward<Vars>(vars)...)),
      std::move(
        v_dir(std::forward<T>(n_x),
              std::forward<T>(n_y),
              std::forward<Vars>(vars)...) +
        fdbb::elem_div(
          fdbb::elem_mul(CONSTANT(2.0, rho(std::forward<Vars>(vars)...)),
                         c(std::forward<Vars>(vars)...)),
          eos::gamma - CONSTANT(1.0, rho(std::forward<Vars>(vars)...))))))
#endif
  {
    return make_fdbbBlockExpr<Tag, 4, 1>(
      std::move(
        v_dir(std::forward<T>(n_x),
              std::forward<T>(n_y),
              std::forward<Vars>(vars)...) -
        fdbb::elem_div(
          fdbb::elem_mul(CONSTANT(2.0, rho(std::forward<Vars>(vars)...)),
                         c(std::forward<Vars>(vars)...)),
          eos::gamma - CONSTANT(1.0, rho(std::forward<Vars>(vars)...)))),
      std::move(fdbb::elem_mul(
        eos::cv,
        fdbb::elem_log(fdbb::elem_div(
          p(std::forward<Vars>(vars)...),
          fdbb::elem_pow(rho(std::forward<Vars>(vars)...), eos::gamma))))),
      std::move(v_dir(std::forward<T>(xi_x),
                      std::forward<T>(xi_y),
                      std::forward<Vars>(vars)...)),
      std::move(
        v_dir(std::forward<T>(n_x),
              std::forward<T>(n_y),
              std::forward<Vars>(vars)...) +
        fdbb::elem_div(
          fdbb::elem_mul(CONSTANT(2.0, rho(std::forward<Vars>(vars)...)),
                         c(std::forward<Vars>(vars)...)),
          eos::gamma - CONSTANT(1.0, rho(std::forward<Vars>(vars)...)))));
  }
};

} // namespace fdbb

#endif // FDBB_PRIMITIVE_2D_H
