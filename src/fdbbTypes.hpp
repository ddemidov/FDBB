/** @file fdbbTypes.hpp
 *
 *  @brief Types class of the Fluid Dynamics Building Blocks
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_TYPES_HPP
#define FDBB_TYPES_HPP

#include "fdbbConfig.hpp"
#include "fdbbEnums.hpp"
#include "fdbbTraits.hpp"

namespace fdbb {

/** @struct fdbbVariables
 *
 *  @brief
 *  The fdbbVariables structure provides member functions to calculate
 *  primary and secondary variables from the vector of variables
 *
 *  The fdbbVariables structure requires an Equation-of-state
 *  structure EOS to compute secondary variables
 *
 * @tparam EOS    The equation of state type
 * @tparam dim    The spatial dimension
 * @tparam Form   The formulation type
 * @tparam Traits The traits
 */
template<typename EOS,
         index_t dim,
         EnumForm Form,
         typename Traits = fdbbTraits<dim, Form>>
struct fdbbVariables
{
  /**
   * @defgroup VariablesPrimary   The primary variables
   * @defgroup VariablesSecondary The secondary variables
   */
};

/** @struct fdbbFluxes
 *
 *  @brief
 *  The fdbbFluxes structure provides member functions to calculate
 *  inviscid and viscous fluxes from the vector of variables
 *
 *  The fdbbFluxes structure requires a Variables structure to compute
 *  primary and secondary variables
 *
 * @tparam Var The variable type
 * @tparam dim The spatial dimension
 */
template<typename Var, index_t dim = Var::dim>
struct fdbbFluxes
{
  /**
   * @defgroup FluxesInviscid The inviscid fluxes
   * @defgroup FluxesViscous  The viscous fluxes
   */
};

/** @struct fdbbJacobians
 *
 *  @brief
 *  The fdbbJacobians structure provides member functions to calculate
 *  inviscid and viscous flux-Jacobians from the vector of variables
 *
 *  The fdbbJacobians structure requires a Variables structure to
 *  compute primary and secondary variables
 *
 * @tparam Var The variable type
 * @tparam dim The spatial dimension
 */
template<typename Var, index_t dim>
struct fdbbJacobians
{
  /**
   * @defgroup JacobiansInviscid The Jacobians for the inviscid fluxes
   * @defgroup JacobiansViscous  The Jacobians for the viscous fluxes
   */
};

/** @struct fdbbRiemannInvariants
 *
 *  @brief
 *  The fdbbRiemannInvariants structure provides member functions to
 *  calculate the Riemann invariants from the vector of variables.
 *
 *  The fdbbRiemannInvariants structure requires a Variables structure
 *  to compute primary and secondary variables
 *
 * @tparam Var The variable type
 * @tparam dim The spatial dimension
 */
template<typename Var, index_t dim>
struct fdbbRiemannInvariants
{
  /**
   * @defgroup RiemannInvariants The Riemann invariants
   */
};

} // namespace fdbb

#endif // FDBB_TYPES_HPP
