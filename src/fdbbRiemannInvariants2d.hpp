/** @file fdbbRiemannInvariants2d.hpp
 *
 *  @brief 2D implementation for Riemann invariants
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_RIEMANN_INVARIANTS_2D_H
#define FDBB_RIEMANN_INVARIANTS_2D_H

#include <type_traits>

#include "fdbbConfig.hpp"
#include "fdbbEnums.hpp"
#include "fdbbTraits.hpp"
#include "fdbbTypes.hpp"

namespace fdbb {

/** @brief
 *
 *  Specialization of the fdbbVariables structure for Riemann
 *  invariants in 2d space dimensions.
 *  This implementation is valid only for ideal gas!
 *
 *  \f[
 *      W = \begin{bmatrix}
 *            w_1\\
 *            w_2\\
 *            w_3\\
 *            w_4
 *          \end{bmatrix}
 *        = \begin{bmatrix}
 *            v_n-2c/(\gamma-1)\\
 *            c_v log(p/\rho^\gamma)\\
 *            v_\xi
 *            v_n+2c/(\gamma-1)
 *          \end{bmatrix}
 *  \f]
 *  where
 *  \f$ v_n       \f$ is the velocity component normal to the boundary,
 *  \f$ c         \f$ is the speed of sound,
 *  \f$ \gamma    \f$ is the adiabatic index,
 *  \f$ c_v       \f$ is the specific heat at constant volume,
 *  \f$ p         \f$ is the absolute pressure,
 *  \f$ \rho      \f$ is the volumetric mass density, and
 *  \f$ v_\xi     \f$ is the velocity component tangential to the boundary.
 */
template<typename EOS, typename Traits>
struct fdbbVariables<EOS, 2, EnumForm::Riemann_invariants, Traits>
{
  /// @brief Equation of state
  using eos = EOS;

  /// @brief Type traits
  using traits = Traits;

  /// @brief Dimension
  static constexpr index_t dim = 2;

  /// @brief Return output stream
  static std::ostream& print(std::ostream& os)
  {
    os << "Riemann invariants in 2d, ";
    eos::print(os);
    return os;
  }

  /** @brief
   *  First Riemann invariant \f$ w_1 \f$ in 2d
   *
   *  @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_1(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      traits::template getVariable<EnumVar::w_1>(std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::w_1>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Second Riemann invariant \f$ w_2 \f$ in 2d
   *
   *  @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_2(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      traits::template getVariable<EnumVar::w_2>(std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::w_2>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Third Riemann invariant \f$ w_3 \f$ in 2d
   *
   *  @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_3(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      traits::template getVariable<EnumVar::w_3>(std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::w_3>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Fourth Riemann invariant \f$ w_4 \f$ in 2d
   *
   *  @ingroup VariablesPrimary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr w_4(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      traits::template getVariable<EnumVar::w_4>(std::forward<Vars>(vars)...))
#endif
  {
    return traits::template getVariable<EnumVar::w_4>(
      std::forward<Vars>(vars)...);
  }

  /** @brief
   *  Speed of sound variable \f$ c \f$ for Riemann
   *  invariants in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr c(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(
      fdbb::elem_div(eos::gamma -
                       CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)),
                     CONSTANT(4.0, w_1(std::forward<Vars>(vars)...))),
      w_4(std::forward<Vars>(vars)...) - w_1(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(
      fdbb::elem_div(eos::gamma -
                       CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)),
                     CONSTANT(4.0, w_1(std::forward<Vars>(vars)...))),
      w_4(std::forward<Vars>(vars)...) - w_1(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Volumetric mass density variable \f$ rho \f$ for Riemann
   *  invariants in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr rho(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_pow(
      fdbb::elem_mul(
        fdbb::elem_div(fdbb::elem_mul(c(std::forward<Vars>(vars)...),
                                      c(std::forward<Vars>(vars)...)),
                       eos::gamma),
        fdbb::elem_exp(fdbb::elem_div(-w_2(std::forward<Vars>(vars)...),
                                      eos::cv))),
      fdbb::elem_div(CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)),
                     eos::gamma -
                       CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)))))
#endif
  {
    return fdbb::elem_pow(
      fdbb::elem_mul(
        fdbb::elem_div(fdbb::elem_mul(c(std::forward<Vars>(vars)...),
                                      c(std::forward<Vars>(vars)...)),
                       eos::gamma),
        fdbb::elem_exp(
          fdbb::elem_div(-w_2(std::forward<Vars>(vars)...), eos::cv))),
      fdbb::elem_div(CONSTANT(1.0, w_1(std::forward<Vars>(vars)...)),
                     eos::gamma -
                       CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))));
  }

  /** @brief
   *  Absolute pressure variable \f$ p \f$ for Riemann
   *  invariants in 2d
   *
   *  @ingroup VariablesSecondary
   */
  template<typename... Vars>
  static FDBB_INLINE auto constexpr p(Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_div(
      fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                     fdbb::elem_mul(c(std::forward<Vars>(vars)...),
                                    c(std::forward<Vars>(vars)...))),
      eos::gamma))
#endif
  {
    return fdbb::elem_div(
      fdbb::elem_mul(rho(std::forward<Vars>(vars)...),
                     fdbb::elem_mul(c(std::forward<Vars>(vars)...),
                                    c(std::forward<Vars>(vars)...))),
      eos::gamma);
  }

  /** @brief
   *  Velocity variable \f$ v_i \f$ for Riemann
   *  invariants in 2d
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *        and of entries of tangential vector \xi
   *
   *  @param[in] n_x    x component of vector n
   *  @param[in] n_y    y component of vector n
   *  @param[in] \xi_x  x component of vector \xi
   *  @param[in] \xi_y  y component of vector \xi
   *
   *  @ingroup VariablesPrimary
   *
   *  @{
   */

  template<index_t idim, typename T, typename... Vars>
  static FDBB_INLINE auto constexpr v(T&& n_x,
                                      T&& n_y,
                                      T&& xi_x,
                                      T&& xi_y,
                                      Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      idim == 0,
      decltype(
        fdbb::elem_mul(
          fdbb::elem_div(w_1(std::forward<Vars>(vars)...) +
                           w_4(std::forward<Vars>(vars)...),
                         CONSTANT(2.0, w_1(std::forward<Vars>(vars)...))),
          fdbb::elem_div(
            std::forward<T>(n_x),
            fdbb::elem_sqrt(
              fdbb::elem_mul(std::forward<T>(n_x), std::forward<T>(n_x)) +
              fdbb::elem_mul(std::forward<T>(n_y), std::forward<T>(n_y))))) +
        fdbb::elem_mul(
          w_3(std::forward<Vars>(vars)...),
          fdbb::elem_div(
            std::forward<T>(xi_x),
            fdbb::elem_sqrt(fdbb::elem_mul(std::forward<T>(xi_x),
                                           std::forward<T>(xi_x)) +
                            fdbb::elem_mul(std::forward<T>(xi_y),
                                           std::forward<T>(xi_y))))))>::type
#endif
  {
    return fdbb::elem_mul(
             fdbb::elem_div(w_1(std::forward<Vars>(vars)...) +
                              w_4(std::forward<Vars>(vars)...),
                            CONSTANT(2.0, w_1(std::forward<Vars>(vars)...))),
             fdbb::elem_div(
               std::forward<T>(n_x),
               fdbb::elem_sqrt(
                 fdbb::elem_mul(std::forward<T>(n_x), std::forward<T>(n_x)) +
                 fdbb::elem_mul(std::forward<T>(n_y), std::forward<T>(n_y))))) +
           fdbb::elem_mul(
             w_3(std::forward<Vars>(vars)...),
             fdbb::elem_div(
               std::forward<T>(xi_x),
               fdbb::elem_sqrt(
                 fdbb::elem_mul(std::forward<T>(xi_x), std::forward<T>(xi_x)) +
                 fdbb::elem_mul(std::forward<T>(xi_y),
                                std::forward<T>(xi_y)))));
  }

  template<index_t idim, typename T, typename... Vars>
  static FDBB_INLINE auto constexpr v(T&& n_x,
                                      T&& n_y,
                                      T&& xi_x,
                                      T&& xi_y,
                                      Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> typename std::enable_if<
      idim == 1,
      decltype(
        fdbb::elem_mul(
          fdbb::elem_div(w_1(std::forward<Vars>(vars)...) +
                           w_4(std::forward<Vars>(vars)...),
                         CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))),
          fdbb::elem_div(
            std::forward<T>(n_y),
            fdbb::elem_sqrt(
              fdbb::elem_mul(std::forward<T>(n_x), std::forward<T>(n_x)) +
              fdbb::elem_mul(std::forward<T>(n_y), std::forward<T>(n_y))))) +
        fdbb::elem_mul(
          w_3(std::forward<Vars>(vars)...),
          fdbb::elem_div(
            std::forward<T>(xi_y),
            fdbb::elem_sqrt(fdbb::elem_mul(std::forward<T>(xi_x),
                                           std::forward<T>(xi_x)) +
                            fdbb::elem_mul(std::forward<T>(xi_y),
                                           std::forward<T>(xi_y))))))>::type
#endif
  {
    return fdbb::elem_mul(
             fdbb::elem_div(w_1(std::forward<Vars>(vars)...) +
                              w_4(std::forward<Vars>(vars)...),
                            CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))),
             fdbb::elem_div(
               std::forward<T>(n_y),
               fdbb::elem_sqrt(
                 fdbb::elem_mul(std::forward<T>(n_x), std::forward<T>(n_x)) +
                 fdbb::elem_mul(std::forward<T>(n_y), std::forward<T>(n_y))))) +
           fdbb::elem_mul(
             w_3(std::forward<Vars>(vars)...),
             fdbb::elem_div(
               std::forward<T>(xi_y),
               fdbb::elem_sqrt(
                 fdbb::elem_mul(std::forward<T>(xi_x), std::forward<T>(xi_x)) +
                 fdbb::elem_mul(std::forward<T>(xi_y),
                                std::forward<T>(xi_y)))));
  }
  /** @} */

  /** @brief
   *  Momentum variable \f$ \rho v_i \f$ for Riemann
   *  invariants in 2d
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *        and of entries of tangential vector \xi
   *
   *  @param[in] n_x    x component of vector n
   *  @param[in] n_y    y component of vector n
   *  @param[in] \xi_x  x component of vector \xi
   *  @param[in] \xi_y  y component of vector \xi
   *
   *  @ingroup VariablesSecondary
   */
  template<index_t idim, typename T, typename... Vars>
  static FDBB_INLINE auto constexpr rhov(T&& n_x,
                                         T&& n_y,
                                         T&& xi_x,
                                         T&& xi_y,
                                         Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(fdbb::elem_mul(v<idim>(std::forward<T>(n_x),
                                       std::forward<T>(n_y),
                                       std::forward<T>(xi_x),
                                       std::forward<T>(xi_y),
                                       std::forward<Vars>(vars)...),
                               rho(std::forward<Vars>(vars)...)))
#endif
  {
    return fdbb::elem_mul(v<idim>(std::forward<T>(n_x),
                                  std::forward<T>(n_y),
                                  std::forward<T>(xi_x),
                                  std::forward<T>(xi_y),
                                  std::forward<Vars>(vars)...),
                          rho(std::forward<Vars>(vars)...));
  }

  /** @brief
   *  Total energy per unit volume variable \f$ \rho E \f$
   *  for Riemann invariants in 2d
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *        and of entries of tangential vector \xi
   *
   *  @param[in] n_x    x component of vector n
   *  @param[in] n_y    y component of vector n
   *  @param[in] \xi_x  x component of vector \xi
   *  @param[in] \xi_y  y component of vector \xi
   *
   *  @ingroup VariablesSecondary
   */
  template<typename T, typename... Vars>
  static FDBB_INLINE auto constexpr rhoE(T&& n_x,
                                         T&& n_y,
                                         T&& xi_x,
                                         T&& xi_y,
                                         Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(
      fdbb::elem_div(p(std::forward<Vars>(vars)...),
                     eos::gamma -
                       CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))) +
      fdbb::elem_mul(fdbb::elem_div(rho(std::forward<Vars>(vars)...),
                                    CONSTANT(2.0,
                                             w_1(std::forward<Vars>(vars)...))),
                     (fdbb::elem_mul(v<0>(std::forward<T>(n_x),
                                          std::forward<T>(n_y),
                                          std::forward<T>(xi_x),
                                          std::forward<T>(xi_y),
                                          std::forward<Vars>(vars)...),
                                     v<0>(std::forward<T>(n_x),
                                          std::forward<T>(n_y),
                                          std::forward<T>(xi_x),
                                          std::forward<T>(xi_y),
                                          std::forward<Vars>(vars)...)) +
                      fdbb::elem_mul(v<1>(std::forward<T>(n_x),
                                          std::forward<T>(n_y),
                                          std::forward<T>(xi_x),
                                          std::forward<T>(xi_y),
                                          std::forward<Vars>(vars)...),
                                     v<1>(std::forward<T>(n_x),
                                          std::forward<T>(n_y),
                                          std::forward<T>(xi_x),
                                          std::forward<T>(xi_y),
                                          std::forward<Vars>(vars)...)))))
#endif
  {
    return fdbb::elem_div(p(std::forward<Vars>(vars)...),
                          eos::gamma -
                            CONSTANT(1.0, w_1(std::forward<Vars>(vars)...))) +
           fdbb::elem_mul(
             fdbb::elem_div(rho(std::forward<Vars>(vars)...),
                            CONSTANT(2.0, w_1(std::forward<Vars>(vars)...))),
             (fdbb::elem_mul(v<0>(std::forward<T>(n_x),
                                  std::forward<T>(n_y),
                                  std::forward<T>(xi_x),
                                  std::forward<T>(xi_y),
                                  std::forward<Vars>(vars)...),
                             v<0>(std::forward<T>(n_x),
                                  std::forward<T>(n_y),
                                  std::forward<T>(xi_x),
                                  std::forward<T>(xi_y),
                                  std::forward<Vars>(vars)...)) +
              fdbb::elem_mul(v<1>(std::forward<T>(n_x),
                                  std::forward<T>(n_y),
                                  std::forward<T>(xi_x),
                                  std::forward<T>(xi_y),
                                  std::forward<Vars>(vars)...),
                             v<1>(std::forward<T>(n_x),
                                  std::forward<T>(n_y),
                                  std::forward<T>(xi_x),
                                  std::forward<T>(xi_y),
                                  std::forward<Vars>(vars)...))));
  }

  /** @brief
   *  State vector of conservative variables in 2d
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *        and of entries of tangential vector \xi
   *
   *  @param[in] n_x    x component of vector n
   *  @param[in] n_y    y component of vector n
   *  @param[in] \xi_x  x component of vector \xi
   *  @param[in] \xi_y  y component of vector \xi
   *
   *  @ingroup VariablesSecondary
   */
  template<std::size_t Tag, typename T, typename... Vars>
  static FDBB_INLINE auto constexpr conservative(T&& n_x,
                                                 T&& n_y,
                                                 T&& xi_x,
                                                 T&& xi_y,
                                                 Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<Tag, 4, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(rhov<0>(std::forward<T>(n_x),
                        std::forward<T>(n_y),
                        std::forward<T>(xi_x),
                        std::forward<T>(xi_y),
                        std::forward<Vars>(vars)...)),
      std::move(rhov<1>(std::forward<T>(n_x),
                        std::forward<T>(n_y),
                        std::forward<T>(xi_x),
                        std::forward<T>(xi_y),
                        std::forward<Vars>(vars)...)),
      std::move(rhoE(std::forward<T>(n_x),
                     std::forward<T>(n_y),
                     std::forward<T>(xi_x),
                     std::forward<T>(xi_y),
                     std::forward<Vars>(vars)...))))
#endif
  {
    return make_fdbbBlockExpr<Tag, 4, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(rhov<0>(std::forward<T>(n_x),
                        std::forward<T>(n_y),
                        std::forward<T>(xi_x),
                        std::forward<T>(xi_y),
                        std::forward<Vars>(vars)...)),
      std::move(rhov<1>(std::forward<T>(n_x),
                        std::forward<T>(n_y),
                        std::forward<T>(xi_x),
                        std::forward<T>(xi_y),
                        std::forward<Vars>(vars)...)),
      std::move(rhoE(std::forward<T>(n_x),
                     std::forward<T>(n_y),
                     std::forward<T>(xi_x),
                     std::forward<T>(xi_y),
                     std::forward<Vars>(vars)...)));
  }

  /** @brief
   *  State vector of primitive variables in 2d
   *
   *  @tparam    T The type of the entries of the entries of normal vector n
   *        and of entries of tangential vector \xi
   *
   *  @param[in] n_x    x component of vector n
   *  @param[in] n_y    y component of vector n
   *  @param[in] \xi_x  x component of vector \xi
   *  @param[in] \xi_y  y component of vector \xi
   *
   *  @ingroup VariablesSecondary
   */
  template<std::size_t Tag, typename T, typename... Vars>
  static FDBB_INLINE auto constexpr primitive(T&& n_x,
                                              T&& n_y,
                                              T&& xi_x,
                                              T&& xi_y,
                                              Vars&&... vars) noexcept
#if !defined(DOXYGEN)
    -> decltype(make_fdbbBlockExpr<Tag, 4, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(v<0>(std::forward<T>(n_x),
                     std::forward<T>(n_y),
                     std::forward<T>(xi_x),
                     std::forward<T>(xi_y),
                     std::forward<Vars>(vars)...)),
      std::move(v<1>(std::forward<T>(n_x),
                     std::forward<T>(n_y),
                     std::forward<T>(xi_x),
                     std::forward<T>(xi_y),
                     std::forward<Vars>(vars)...)),
      std::move(p(std::forward<Vars>(vars)...))))
#endif
  {
    return make_fdbbBlockExpr<Tag, 4, 1>(
      std::move(rho(std::forward<Vars>(vars)...)),
      std::move(v<0>(std::forward<T>(n_x),
                     std::forward<T>(n_y),
                     std::forward<T>(xi_x),
                     std::forward<T>(xi_y),
                     std::forward<Vars>(vars)...)),
      std::move(v<1>(std::forward<T>(n_x),
                     std::forward<T>(n_y),
                     std::forward<T>(xi_x),
                     std::forward<T>(xi_y),
                     std::forward<Vars>(vars)...)),
      std::move(p(std::forward<Vars>(vars)...)));
  }
};

} // namespace fdbb

#endif // FDBB_RIEMANN_INVARIANTS_2D_H
