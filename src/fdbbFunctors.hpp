/** @file fdbbFunctors.hpp
 *
 *  @brief Functors used in the FDBB library
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_FUNCTORS_HPP
#define FDBB_FUNCTORS_HPP

namespace fdbb {

namespace functor {

struct make_array
{
  template<class H, class... T>
  std::array<H, 1 + sizeof...(T)> operator()(const H& h, const T&... t)
  {
    return { h, t... };
  }
};

/// @brief Functor for element-wise dereferencing of each object
struct deref
{
  template<typename T0, std::size_t I0>
  constexpr auto operator()(const T0* t0,
                            std::integral_constant<std::size_t, I0>) const
    noexcept -> decltype(*t0)
  {
    return *t0;
  }
};

/// @brief Functor for element-wise printing the size of each object
struct print_size
{
  template<typename T0, std::size_t I0>
  void operator()(const T0& t0, std::integral_constant<std::size_t, I0>) const
    noexcept
  {
    std::cout << t0.size();
  }
};

/// @brief Functor for element-wise assignement of one object to another
struct assign
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  typename std::enable_if<!std::is_pointer<T0>::value, void>::type operator()(
    const T0& t0,
    T1& t1,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>) const noexcept
  {
    t1 = t0;
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  typename std::enable_if<std::is_pointer<T0>::value, void>::type operator()(
    const T0& t0,
    T1& t1,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>) const noexcept
  {
    t1 = *t0;
  }
};

/// @brief Functor for element-wise move-assignement of one object to another
struct move_assign
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  typename std::enable_if<!std::is_pointer<T0>::value, void>::type operator()(
    const T0& t0,
    T1& t1,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>) const noexcept
  {
    t1 = std::move(t0);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  typename std::enable_if<std::is_pointer<T0>::value, void>::type operator()(
    const T0& t0,
    T1& t1,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>) const noexcept
  {
    t1 = std::move(*t0);
  }
};

/// @brief Functor for element-wise copy-construction of one object from another
struct copy_construct
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  typename std::enable_if<!std::is_pointer<T0>::value, void>::type operator()(
    const T0& t0,
    T1& t1,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>) const noexcept
  {
    t1 = T1(t0);
  }

  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  typename std::enable_if<std::is_pointer<T0>::value, void>::type operator()(
    const T0& t0,
    T1& t1,
    std::integral_constant<std::size_t, I0>,
    std::integral_constant<std::size_t, I1>) const noexcept
  {
    t1 = *t0;
  }
};

/// @brief Functor for element-wise addition of two objects
struct add
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>) const
    noexcept -> decltype(t0 + t1)
  {
    return t0 + t1;
  }
};

/// @brief Functor for element-wsie subtraction of two objects
struct sub
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>) const
    noexcept -> decltype(t0 - t1)
  {
    return t0 - t1;
  }
};

/// @brief Functor for element-wise multiplication of two objects
struct mul
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>) const
    noexcept -> decltype(t0 * t1)
  {
    return t0 * t1;
  }
};

/// @brief Functor for element-wise division of two objects
struct div
{
  template<typename T0, typename T1, std::size_t I0, std::size_t I1>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>) const
    noexcept -> decltype(t0 / t1)
  {
    return t0 / t1;
  }
};

/// @brief Functor for multiplication of two objects (inner summation loop)
struct matmul_inner
{
  template<typename T0,
           typename T1,
           typename T2,
           std::size_t I0,
           std::size_t I1,
           std::size_t I2>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            const T2& t2,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>,
                            std::integral_constant<std::size_t, I2>) const
    noexcept -> decltype(t1 * t2)
  {
    return t1 * t2;
  }
};

/// @brief Functor for multiplication of two objects (outer loop)
struct matmul
{
  template<typename T0,
           typename T1,
           typename T2,
           typename T3,
           std::size_t I0,
           std::size_t I1,
           std::size_t I2,
           std::size_t I3>
  constexpr auto operator()(const T0& t0,
                            const T1& t1,
                            const T2& t2,
                            const T3& t3,
                            std::integral_constant<std::size_t, I0>,
                            std::integral_constant<std::size_t, I1>,
                            std::integral_constant<std::size_t, I2>,
                            std::integral_constant<std::size_t, I3>) const
    noexcept -> decltype(utils::tuple_sum(
      utils::for_each_return<
        0,
        1,
        (I0 /
         (std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1))) *
          (std::tuple_size<T2>::value / I1),
        1,
        I0 - (I0 / (std::tuple_size<T3>::value /
                    (std::tuple_size<T2>::value / I1))) *
               (std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1)),
        std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1)>(
        utils::make_tuple(
          utils::make_index_sequence<std::tuple_size<T2>::value / I1>{}),
        t2,
        t3,
        matmul_inner())))
  {
    // Remark: indices are calculated as follows
    // constexpr auto row0 = I1;
    // constexpr auto col0 = std::tuple_size<T2>::value/I1;
    //
    // constexpr auto row1 = col0;
    // constexpr auto col1 = std::tuple_size<T3>::value/row1;
    //
    // constexpr auto irow = I0/col1;;
    // constexpr auto icol = I0 - irow * col1;

    return utils::tuple_sum(
      utils::for_each_return<
        0,
        1,
        (I0 /
         (std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1))) *
          (std::tuple_size<T2>::value / I1),
        1,
        I0 - (I0 / (std::tuple_size<T3>::value /
                    (std::tuple_size<T2>::value / I1))) *
               (std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1)),
        std::tuple_size<T3>::value / (std::tuple_size<T2>::value / I1)>(
        utils::make_tuple(
          utils::make_index_sequence<std::tuple_size<T2>::value / I1>{}),
        t2,
        t3,
        matmul_inner()));
  }
};

} // namespace functor

} // namespace fdbb

#endif // FDBB_FUNCTORS_HPP
