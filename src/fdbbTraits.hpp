/** @file fdbbTraits.hpp
 *
 *  @brief Default traits for Fluid Dynamics Building Blocks
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_TRAITS_HPP
#define FDBB_TRAITS_HPP

#include <tuple>

#include "fdbbConfig.hpp"
#include "fdbbEnums.hpp"

namespace fdbb {

/** @struct map
 *
 * @brief Default mapping of variables to variadic argument lists
 *
 * The default mapping assumes that the order of arguments in
 * variadic parameter lists naturally reflects the order of
 * variables as they are stored in the variable vector.
 *
 * For instance, the vector of conservative variables in 3D reads
 *
 * \f[
 *     \left[ u0,
 *            u1,
 *            u2,
 *            u3,
 *            u4
 *     \right]^\top
 *     =
 *     \left[ \rho,
 *            \rho v_x,
 *            \rho v_y,
 *            \rho v_z,
 *            \rho E
 *     \right]^\top
 * \f]
 *
 * The default mapping assumes that the first five parameters to all
 * variable, flux, and Jacobian evaluations are u0,...,u4.
 *
 * \code{.cpp}
 * // Equation of state for ideal gas
 * using eos = fdbb::fdbbEOSidealGas<double,
 *                                        std::ratio<7,2>,
 *                                        std::ratio<5,2>>;
 *
 * // Conservative variables in 3D
 * using variables = fdbb::fdbbVariables<eos, 3, fdbb::EnumVar::conservative>;
 *
 * // Calculate internal energy per unit mass
 * auto e = variables::e(u0, u1, u2, u3, u4);
 * \endcode
 *
 * In order to implement another mapping between variables and
 * arguments you need to overwrite the specialization of the map
 * structure accordingly
 *
 * \code{.cpp}
 * template<> struct map<EnumVar::rho   , 1, EnumForm::conservative> { static
 * const int index = 2; };
 * template<> struct map<EnumVar::rhov_x, 1, EnumForm::conservative> { static
 * const int index = 0; };
 * template<> struct map<EnumVar::rhoe  , 1, EnumForm::conservative> { static
 * const int index = 1; };
 * \code
 *
 * @tparam Var  The variable type
 * @tparam dim  The spatial dimension
 * @tparam Form The formulation type
 */
template<EnumVar Var, int dim, EnumForm Form>
struct map
{
};

#if !defined(DOXYGEN)

// Conservative variables

template<>
struct map<EnumVar::rho, 1, EnumForm::conservative>
{
  static const int index = 0;
};
template<>
struct map<EnumVar::rhov_x, 1, EnumForm::conservative>
{
  static const int index = 1;
};
template<>
struct map<EnumVar::rhoE, 1, EnumForm::conservative>
{
  static const int index = 2;
};

template<>
struct map<EnumVar::rho, 2, EnumForm::conservative>
{
  static const int index = 0;
};
template<>
struct map<EnumVar::rhov_x, 2, EnumForm::conservative>
{
  static const int index = 1;
};
template<>
struct map<EnumVar::rhov_y, 2, EnumForm::conservative>
{
  static const int index = 2;
};
template<>
struct map<EnumVar::rhoE, 2, EnumForm::conservative>
{
  static const int index = 3;
};

template<>
struct map<EnumVar::rho, 3, EnumForm::conservative>
{
  static const int index = 0;
};
template<>
struct map<EnumVar::rhov_x, 3, EnumForm::conservative>
{
  static const int index = 1;
};
template<>
struct map<EnumVar::rhov_y, 3, EnumForm::conservative>
{
  static const int index = 2;
};
template<>
struct map<EnumVar::rhov_z, 3, EnumForm::conservative>
{
  static const int index = 3;
};
template<>
struct map<EnumVar::rhoE, 3, EnumForm::conservative>
{
  static const int index = 4;
};

// Primitive variables

template<>
struct map<EnumVar::rho, 1, EnumForm::primitive>
{
  static const int index = 0;
};
template<>
struct map<EnumVar::v_x, 1, EnumForm::primitive>
{
  static const int index = 1;
};
template<>
struct map<EnumVar::p, 1, EnumForm::primitive>
{
  static const int index = 2;
};

template<>
struct map<EnumVar::rho, 2, EnumForm::primitive>
{
  static const int index = 0;
};
template<>
struct map<EnumVar::v_x, 2, EnumForm::primitive>
{
  static const int index = 1;
};
template<>
struct map<EnumVar::v_y, 2, EnumForm::primitive>
{
  static const int index = 2;
};
template<>
struct map<EnumVar::p, 2, EnumForm::primitive>
{
  static const int index = 3;
};

template<>
struct map<EnumVar::rho, 3, EnumForm::primitive>
{
  static const int index = 0;
};
template<>
struct map<EnumVar::v_x, 3, EnumForm::primitive>
{
  static const int index = 1;
};
template<>
struct map<EnumVar::v_y, 3, EnumForm::primitive>
{
  static const int index = 2;
};
template<>
struct map<EnumVar::v_z, 3, EnumForm::primitive>
{
  static const int index = 3;
};
template<>
struct map<EnumVar::p, 3, EnumForm::primitive>
{
  static const int index = 4;
};

// Riemann invariants

template<>
struct map<EnumVar::w_1, 1, EnumForm::Riemann_invariants>
{
  static const int index = 0;
};
template<>
struct map<EnumVar::w_2, 1, EnumForm::Riemann_invariants>
{
  static const int index = 1;
};
template<>
struct map<EnumVar::w_3, 1, EnumForm::Riemann_invariants>
{
  static const int index = 2;
};

template<>
struct map<EnumVar::w_1, 2, EnumForm::Riemann_invariants>
{
  static const int index = 0;
};
template<>
struct map<EnumVar::w_2, 2, EnumForm::Riemann_invariants>
{
  static const int index = 1;
};
template<>
struct map<EnumVar::w_3, 2, EnumForm::Riemann_invariants>
{
  static const int index = 2;
};
template<>
struct map<EnumVar::w_4, 2, EnumForm::Riemann_invariants>
{
  static const int index = 3;
};

template<>
struct map<EnumVar::w_1, 3, EnumForm::Riemann_invariants>
{
  static const int index = 0;
};
template<>
struct map<EnumVar::w_2, 3, EnumForm::Riemann_invariants>
{
  static const int index = 1;
};
template<>
struct map<EnumVar::w_3, 3, EnumForm::Riemann_invariants>
{
  static const int index = 2;
};
template<>
struct map<EnumVar::w_4, 3, EnumForm::Riemann_invariants>
{
  static const int index = 3;
};
template<>
struct map<EnumVar::w_5, 3, EnumForm::Riemann_invariants>
{
  static const int index = 4;
};

#endif

/**
 * @brief Default implementation of traits.
 *
 * This implementation of traits is passed as default template
 * parameter to all components of the library. If required, the user
 * can implement a specialisation of traits and pass it as template
 * parameter to control the internal behavior of components.
 *
 * @tparam dim  The spatial dimension
 * @tparam Form The formulation type
 */
template<index_t dim, EnumForm Form>
struct fdbbTraits
{
  /**
   * @brief
   * Mapping between arguments and variables
   *
   * @note
   * This implementation perfectly forwards the var-th element of
   * the variadic parameter pack as the var-th variable.
   *
   *   getVar<var>(var0, var1, ...) -> var{var}
   *
   */
  template<EnumVar var, typename... Vars>
  static FDBB_INLINE auto constexpr getVariable(Vars&&... vars) noexcept
    -> const typename std::tuple_element<map<var, dim, Form>::index,
                                         std::tuple<Vars...>>::type
  {
    return std::get<map<var, dim, Form>::index>(std::tuple<Vars...>(vars...));
  }
};

/**
 * @brief Specialized implementation of traits.
 *
 * @tparam dim  The spatial dimension
 * @tparam Form The formulation type
 */
template<index_t dim, EnumForm Form, std::size_t arg>
struct fdbbTraits_SOA1
{
  /**
   * @brief
   * Mapping between arguments and variables
   *
   * @note
   * This implementation forwards the var-th component of the arg-th
   * element of the variadic parameter pack as the var-th variable.
   *
   *   getVar<var>(Var0, Var1, ...) -> Var{arg}[var]
   */
  template<EnumVar var, typename... Vars>
  static FDBB_INLINE auto constexpr getVariable(Vars&&... vars) noexcept
    -> const decltype(
      std::get<arg>(std::tuple<Vars...>(vars...))[map<var, dim, Form>::index])
  {
    return std::get<arg>(
      std::tuple<Vars...>(vars...))[map<var, dim, Form>::index];
  }
};

/**
 * @brief Specialized implementation of traits.
 *
 * @tparam dim  The spatial dimension
 * @tparam Form The formulation type
 */
template<index_t dim, EnumForm Form, std::size_t arg>
struct fdbbTraits_SOA2
{
  /**
   * @brief
   * Mapping between arguments and variables
   *
   * @note
   * This implementation forwards the var-th component of the arg-th
   * element of the variadic parameter pack as the var-th variable.
   *
   *   getVar<var>(Var0, Var1, ...) -> Var{arg}(var)
   */
  template<EnumVar var, typename... Vars>
  static FDBB_INLINE auto constexpr getVariable(Vars&&... vars) noexcept
    -> const decltype(
      std::get<arg>(std::tuple<Vars...>(vars...))(map<var, dim, Form>::index))
  {
    return std::get<arg>(std::tuple<Vars...>(vars...))(
      map<var, dim, Form>::index);
  }
};

/**
 * @brief Specialized implementation of traits.
 *
 * @tparam dim  The spatial dimension
 * @tparam Form The formulation type
 */
template<index_t dim, EnumForm Form, std::size_t arg>
struct fdbbTraits_SOA3
{
  /**
   * @brief
   * Mapping between arguments and variables
   *
   * @note
   * This implementation forwards the var-th component of the arg-th
   * element of the variadic parameter pack as the var-th variable.
   *
   *   getVar<ivar>(Var0, Var1, ...) -> std::get<var>(Var{arg})
   */
  template<EnumVar var, typename... Vars>
  static FDBB_INLINE auto constexpr getVariable(Vars&&... vars) noexcept
    -> const decltype(std::get<map<var, dim, Form>::index>(
      std::get<arg>(std::tuple<Vars...>(vars...))))
  {
    return std::get<map<var, dim, Form>::index>(
      std::get<arg>(std::tuple<Vars...>(vars...)));
  }
};

} // namespace fdbb

#endif // FDBB_TRAITS_HPP
