/** @file fdbbConfig.hpp
 *
 *  @brief Configuration class of the Fluid Dynamics Building Blocks
 *
 *  @copyright This file is part of the FDBB library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @author Matthias Moller
 */
#pragma once
#ifndef FDBB_CONFIG_HPP
#define FDBB_CONFIG_HPP

/** @namespace fdbb
 *
 *  @brief
 *  The \ref fdbb namespace, containing all public functionality of
 *  the FDBB library
 *
 *  The \ref fdbb namespace contains all functionality of the FDBB
 *  library that is exposed to the end-user. All functionality in this
 *  namespace has a stable API over future FDBB library releases.
 *
 *  @note This Source Code Form is subject to the terms of the Mozilla
 *  Public License, v. 2.0. If a copy of the MPL was not distributed
 *  with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace fdbb {

/** @enum EnumETL
 *
 *  @brief
 *  The EnumETL enumeration type, listing all supported vector
 *  expression template libraries explicitly supported by the library.
 */
enum class EnumETL
{
  /// @brief Generic implementation
  GENERIC,

  /// @brief Implementation for caching mechanism
  CACHE,

  /// @brief Implementation for lightweight caching mechanism
  CACHE2,

  /// @brief Implementation for block expressions
  BLOCKEXPR,

#ifdef SUPPORT_ARMADILLO
  /// @brief Armadillo library
  /// http://arma.sourceforge.net
  ARMADILLO,
#endif

#ifdef SUPPORT_ARRAYFIRE
  /// @brief ArrayFire library
  /// http://arrayfire.com
  ARRAYFIRE,
#endif

#ifdef SUPPORT_BLAZE
  /// @brief Blaze library
  /// https://bitbucket.org/blaze-lib/blaze
  BLAZE,
#endif

#ifdef SUPPORT_EIGEN
  /// @{
  /// @brief Eigen library
  /// http://eigen.tuxfamily.org
  EIGEN,
  EIGEN_ARRAY,
  EIGEN_MATRIX,
/// @}
#endif

#ifdef SUPPORT_ITPP
  /// @{
  /// @brief IT++ library
  /// http://itpp.sourceforge.net
  ITPP,
  ITPP_VECTOR,
  ITPP_MATRIX,
/// @}
#endif

#if defined(SUPPORT_MTL4) || defined(SUPPORT_CMTL4)
  /// @{
  /// @brief MTL4 library
  /// http://www.simunova.com/docs/mtl4/html/index.html
  MTL4,
  MTL4_VECTOR,
  MTL4_MATRIX,
/// @}
#endif

#ifdef SUPPORT_UBLAS
  /// @brief uBLAS library
  /// http://www.boost.org/doc/libs/1_60_0/libs/numeric/ublas/doc/
  UBLAS,
#endif

#ifdef SUPPORT_VEXCL
  /// @brief VexCL library
  /// https://github.com/ddemidov/vexcl
  VEXCL,
#endif

#ifdef SUPPORT_VIENNACL
  /// @brief ViennaCL library
  /// http://viennacl.sourceforge.net
  VIENNACL
#endif
};

/** @brief
 * Print EnumETL to string
 */
std::ostream&
operator<<(std::ostream& os, EnumETL etl)
{
  switch (etl) {
    case EnumETL::GENERIC:
      return os << "GENERIC";

    case EnumETL::CACHE:
      return os << "CACHE";

    case EnumETL::CACHE2:
      return os << "CACHE2";

    case EnumETL::BLOCKEXPR:
      return os << "BLOCKEXPR";

#ifdef SUPPORT_ARMADILLO
    case EnumETL::ARMADILLO:
      return os << "ARMADILLO";
#endif

#ifdef SUPPORT_ARRAYFIRE
    case EnumETL::ARRAYFIRE:
      return os << "ARRAYFIRE";
#endif

#ifdef SUPPORT_BLAZE
    case EnumETL::BLAZE:
      return os << "BLAZE";
#endif

#ifdef SUPPORT_EIGEN
    case EnumETL::EIGEN:
      return os << "EIGEN";
    case EnumETL::EIGEN_ARRAY:
      return os << "EIGEN_ARRAY";
    case EnumETL::EIGEN_MATRIX:
      return os << "EIGEN_MATRIX";
#endif

#ifdef SUPPORT_ITPP
    case EnumETL::ITPP:
      return os << "ITPP";
    case EnumETL::ITPP_VECTOR:
      return os << "ITPP_VECTOR";
    case EnumETL::ITPP_MATRIX:
      return os << "ITPP_MATRIX";
#endif

#if defined(SUPPORT_MTL4) || defined(SUPPORT_CMTL4)
    case EnumETL::MTL4:
      return os << "MTL4";
    case EnumETL::MTL4_VECTOR:
      return os << "MTL4_VECTOR";
    case EnumETL::MTL4_MATRIX:
      return os << "MTL4_MATRIX";
#endif

#ifdef SUPPORT_UBLAS
    case EnumETL::UBLAS:
      return os << "UBLAS";
#endif

#ifdef SUPPORT_VEXCL
    case EnumETL::VEXCL:
      return os << "VEXCL";
#endif

#ifdef SUPPORT_VIENNACL
    case EnumETL::VIENNACL:
      return os << "VIENNACL";
#endif
  }
  return os << static_cast<std::uint16_t>(etl);
}

/** @namespace fdbb::detail
 *
 *  @brief
 *  The \ref fdbb::detail namespace, containing all details of the
 *  functionality of the FDBB library
 *
 *  The \ref fdbb::detail namespace contains all details of the
 *  functionality of the FDBB library such as implementation details
 *  and is not exposed to the end-user. The functionality is subject
 *  to change between different releases without notice. For this
 *  reason, end-users are advised to not make explicit use of
 *  functionality from the \ref fdbb::detail namespace.
 *
 *  @note This Source Code Form is subject to the terms of the Mozilla
 *  Public License, v. 2.0. If a copy of the MPL was not distributed
 *  with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
namespace detail {
} // namespace detail
} // namespace fdbb

/** @typedef index_t
 *
 *  @brief
 *  The index_t typedef specifying the default type for indices
 */
#ifndef index_t
#ifdef FDBB_INDEX_TYPE
#define index_t FDBB_INDEX_TYPE
#pragma message "Using FDBB_INDEX_TYPE for index_t type"
#else
#define index_t std::size_t
#pragma message "Using std::size_t for index_t type"
#endif
#else
#pragma message "Using externally defined index_t type"
#endif

/** @brief Macro for forcing inlining of code.
 *
 *  The macro defines the attribute of the function such that it is
 *  directly inlined and not just an recomendation for the compiler.
 */
#ifdef FDBB_FORCE_INLINE
#if defined(_MSC_VER) || defined(__INTEL_COMPILER)
#define FDBB_INLINE __forceinline
#elif defined(__GNUC__)
#define FDBB_INLINE inline __attribute__((always_inline))
#else
#warning                                                                       \
  "Could not determine compiler for forced inline definitions. Using inline."
#define FDBB_INLINE inline
#endif
#else
#define FDBB_INLINE inline
#endif

/** @brief Macro for avoiding inlining of code.
 *
 *  The macro defines the attribute of the function such that it is no
 *  longer considered for inlining.
 */
#ifdef FDBB_FORCE_INLINE
#define FDBB_NO_INLINE __attribute__((noinline))
#else
#define FDBB_NO_INLINE /* no avoiding of inline defined */
#endif

/** @brief Macro for forwarding of arguments.
 *
 *  The macro defines the way in which arguments are forwarded.
 */
#ifdef FDBB_PERFECT_FORWARDING

#define __INVALUE__(Type, Arg) Type&& Arg
#define __FORWARD__(Type, Arg) std::forward<Type>(Arg)

#else

#define __INVALUE__(Type, Arg) const Type& Arg
#define __FORWARD__(Type, Arg) Arg

#endif

#endif // FDBB_CONFIG_HPP
